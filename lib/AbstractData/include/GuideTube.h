#ifndef GUIDETUBE_H
#define GUIDETUBE_H

#include "vtkSmartPointer.h"

#include "vtkContourFilter.h"
#include "vtkPolyDataNormals.h"
#include "vtkStripper.h"
#include "vtkPolyDataMapper.h"
#include "vtkActor.h"
#include "vtkRenderer.h"
#include "vtkClipPolyData.h"
#include "vtkPlane.h"
#include "vtkCutter.h"
#include "vtkStripper.h"
#include "vtkPolyData.h"
#include "vtkTriangleFilter.h"
#include "vtkTubeFilter.h"
#include "vtkLineSource.h"

//Debug
#include "vtkCellArray.h"
#include "vtkPoints.h"

// Our code
#include "AbstractPolyDataAnnotator.h"

//End pt = tumor inside
//Start pt = tumor outside 


class GuideTube : public AbstractPolyDataAnnotator
{
public:
	//Constructor, desctrutor
	GuideTube(/*QObject* parent = NULL*/);
	virtual ~GuideTube();


	//Set Parameter
	void SetRadius(double radius);
	void SetPosition(double* startPos, double* endPos);
	void SetPosition(double, double, double, double, double, double);
	void SetStartPosition(double, double, double);
	void SetEndPosition(double, double, double);


	//Get membership variable
	vtkSmartPointer<vtkLineSource>	 GetLineSource();
	
	double*	 GetStartPosition();
	double*	 GetEndPosition();
	double*	 GetNormal();
	double	 GetLength();
	double	 GetRadius();

    //Pipeline
	bool Create(double* startPos, double* endPos);
	bool Create(double s_x, double s_y, double s_z, double e_x, double e_y, double e_z);
	bool Create(double* endPos, double* direction, double length);
	void Update();

protected:
	virtual void InitializeSource();

	vtkSmartPointer<vtkLineSource>		m_lineSource;
	vtkSmartPointer<vtkTubeFilter>		m_tubeFilter;

    //Parameter
	double	m_startPosition[3];
	double	m_endPosition[3];
	double	m_normal[3];
	double  m_length;
	double	m_radius;
};

#endif // GUIDETUBE_H
