/*
	This programe is a class of IodineSeed which is a componenet of the TPS Module

    Copyright (C) 2015	Wong Matthew Lun

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef IODINE_SEED_H
#define IODINE_SEED_H
#include <iostream>
#include <math.h>
#include <vtkMath.h>
#include <itkResampleImageFilter.h>
#include <itkImageDuplicator.h>
#include <itkAffineTransform.h>
#include "define.h"
#include "Landmark.h"

#include <QString>

// Properties of seeds are defined in .cpp file

class IodineSeed : public Landmark
{
public:
	IodineSeed();
	IodineSeed(double* coord, double* orientation, double seedActivity, double seedLength, int seedInsertSequence);

	virtual ~IodineSeed() {
	};

	/* Set Properties */
	//void SetLandmark(Landmark* landmark);
	virtual void SetPosition(double[3]);
	virtual void SetPosition(double, double, double);
	virtual void SetNormal(double[3]);
	virtual void SetNormal(double, double, double);

	int SetSpacing(double[3]);
	int SetSpacing(double, double, double);
	int SetBounds(double[3]);
	int SetBounds(double, double, double);
	int SetSeedActivity(double);
	int SetSeedLength(double);
	int SetSeedInsertSequence(int);
	int SetBaseVolume(ImageType::Pointer);
	int SetDecayTimeStamp(double);
	int SetHalfLife(double);
	
	/// Legacy function
	int SetOrientation(double[3]);
	int SetOrientation(double, double, double);

	/* Get Properties */
	virtual double*		GetPosition();
	virtual double*		GetCoordinate();
	double*				GetOrientation();
	double*				GetSpacing();
	double*				GetBounds();
	double				GetSeedActivity();
	double				GetDecayTimeStamp();
	double				GetHalfLife();
	double				GetUtomCiConversionConstant();
	const double*		GetBaseOrientation();
	double				GetSeedLength();
	ImageType::Pointer	GetDoseVolume();
	ImageType::Pointer	GetBaseVolume();
	int					GetSeedInsertSequence();
	
	/// Utilities
	void Copy(IodineSeed *templateSeed);	
	void Modified();

	/// Key functions
	/* Creat the object and asign memories*/
	virtual void Create();
	/* Update actors and cutter and transform filters */
	virtual void Update();
	/* Update seed dose volume */
	void UpdateSeed();
	ImageType::Pointer GetOutput();

	enum IODIN_SEED_ERROR {
		BASE_VOLUME_MISSING_ERROR = 1,
		OUT_VOLUME_GENERATE_FAILED = 2,
	};
	
protected:
	virtual void InstallPipeline();

private:

	double _GeometricFunctionLine(double radius, double theta);
	int _GenerateOutVolume();


	// Utility functions
	double _GetMagnitude(const double[3]);
	double _DotProduct(const double v1[3], const double v2[3]);
	double _GetSmallerDouble(double v1, double v2);
	double _GetLargerDouble(double v1, double v2);

	// Flags marking if the funcitons are initiated.
	bool _ISINIT_F;	
	bool _ISINIT_g;

	// Flag indicating if a basevolume exist
	bool _BASE_VOLUME ;

	// Flag mark if the function is updated
	bool _IS_UPDATE ;

	ImageType::Pointer m_doseVolume;
	ImageType::Pointer m_baseVolume;
	double _spacing[3];
	double m_bounds[3];
	double m_seedActivity;
	double m_seedLength;
	double m_halfLife;
	double m_decayTimeStamp; // in days
	int m_seedInsertSequence;

	// Some constant variables
	double m_baseOrientation[3];
	double m_uTomCiConvertionConstant;
};

#endif // IODINE_SEED_H

