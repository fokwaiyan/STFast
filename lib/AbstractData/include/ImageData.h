#ifndef IMAGE_H
#define IMAGE_H

#include <QObject>
#include <QDebug>
#include <QStringList>
#include "Define.h"
#include "vtkImageFlip.h"
#include "itkvtkConvert.hxx"
#include "AbstractImage.h"

class Image : public QObject, public AbstractImage 
{
    Q_OBJECT
public:
    explicit Image(QObject *parent = 0);
	~Image();

	virtual void SetImage(QString niiFilePath);
	virtual void SetImage(QStringList* dcmFileList);
    virtual void SetImage(ImageType::Pointer image);
    void SetWindow(double window);
    void SetLevel(double level);
	void SetDefaultWindow(double window);
	void SetDefaultLevel(double level);
	void SetFixedWindowLevel(bool b);
	void SetLUT(int lut);

	ImageType::Pointer		GetItkImage();
	vtkImageData*			GetImageData();
	double					GetWindow();
	double					GetLevel();
	double					GetDefaultWindow();
	double					GetDefaultLevel();
	int						GetLUT();
	vtkLookupTable*			GetLookUpTable();
	bool					GetFixedWindowLevel();
	//double*					GetSpacing();
	//void					GetPosition(double* pos);
	

	ImageType::DirectionType GetDefaultDirection();
	ImageType::PointType	 GetDefaultOrigin();

public slots:

private:
    void InstallPipeline();

	vtkImageData*						m_imageData;
	vtkLookupTable*						m_lut;
	bool								m_fixedWindowLevel;

	int		m_lutType;
	double  m_defaultWindow;
	double  m_defaultLevel;
	ImageType::DirectionType m_defaultDirection;
	ImageType::PointType	 m_defaultOrigin;
};


#endif // IMAGE_H
