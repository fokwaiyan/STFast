/*
Author:		Wong, Matthew Lun
Date:		26th, May 2016
Occupation:	Chinese University of Hong Kong,
Department of Imaging and Inteventional Radiology,
Junior Research Assistant


The abstract data class is designed to hold data structure typically used in a medical viewer.
This class holds annotator polydata, which automatically generate 2D views based on 3 surfaces.


Wong Matthew Lun
Copyright (C) 2016
*/
#ifndef ABSTRACT_POLYDATA_ANNOTATOR_H
#define ABSTRACT_POLYDATA_ANNOTATOR_H

#include <vtkActor.h>
#include <vtkCutter.h>
#include <vtkStripper.h>
#include <vtkTriangleFilter.h>
#include <vtkPolyDataMapper.h>
#include <vtkPlane.h>
#include "AbstractPolyData.h"
//#include "MarkedActor.h"

class AbstractPolyDataAnnotator : 
	public AbstractPolyData
{
public:
	AbstractPolyDataAnnotator();
	~AbstractPolyDataAnnotator();

	/// Set Functions
	/* Set the cutting planes of display */
	virtual void SetCutFunctions(vtkSmartPointer<vtkPlane>*);
	/* Set the nth cutting plane of display */
	virtual void SetCutFunctions(vtkSmartPointer<vtkPlane>, vtkSmartPointer<vtkPlane>, vtkSmartPointer<vtkPlane>);
	virtual void SetCutFunctions(int, vtkSmartPointer<vtkPlane>);
	/* Get 2D actors pointer with 3 components */
	virtual vtkSmartPointer<vtkActor>*	Get2DActors();
	/* Get nth 2D Actor */
	virtual vtkSmartPointer<vtkActor>	Get2DActor(int);
	virtual void SetVisibility(bool vis);
	virtual void SetOpacity(double opacity);
	virtual void SetActorState(int state);
	

	/// Get Functions
	virtual bool	GetVisibility();
	virtual double	GetOpacity();

	/* Legacy functions, DO NOT USE*/
	virtual vtkSmartPointer<vtkPolyData> GetPolyDataOutput(int n);
	virtual vtkSmartPointer<vtkActor>	GetActor(int n);
	virtual vtkSmartPointer<vtkActor>	GetActor();

	virtual void	Update();

protected:
	enum ABSTRACT_POLY_DATA_ANNOTATION_ERROR {
		CUT_PLANES_MISSING_ERROR = 10,
		ACTORS_2D_MISSING_ERROR = 20
		
	};
	/// Critical Functions
	/* This function is totally not effective unless you inherit InitializeSource() in subclass*/
	virtual void InstallPipeline();
	// Note: Must inherit this virtual function in sub classes
	virtual void InitializeSource() = 0;
	virtual void InitializeActors();

	vtkSmartPointer<vtkActor> m_2Dactors[3];
	vtkSmartPointer<vtkPlane> m_cutPlanes[3];
	vtkSmartPointer<vtkCutter> m_cutters[3];
	vtkSmartPointer<vtkStripper> m_boundaryStrips[3];
	vtkSmartPointer<vtkPolyData> m_polyData2D[3];
	vtkSmartPointer<vtkTriangleFilter> m_triangleFilter[3];
};


#endif