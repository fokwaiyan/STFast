#include "GuideTube.h"
#include "vtkPolyData.h"
#include "vtkPointData.h"
#include "vtkLookupTable.h"
#include "vtkRenderer.h"
#include "vtkRenderWindow.h"
#include "vtkProperty.h"
#include "vtkMath.h"

GuideTube::GuideTube()//(QObject* parent) : QObject(parent)
{
	m_tubeFilter			= NULL;
	m_lineSource			= NULL;
	m_radius				= 3;
	m_dataType = DataType::GUIDE_TUBE;
}

GuideTube::~GuideTube()
{
}


bool GuideTube::Create(double* startPos, double* endPos)
{
	m_startPosition[0] = startPos[0];
	m_startPosition[1] = startPos[1];
	m_startPosition[2] = startPos[2];

	m_endPosition[0] = endPos[0];
	m_endPosition[1] = endPos[1];
	m_endPosition[2] = endPos[2];

	double dir[3];
	dir[0] = m_endPosition[0] - m_startPosition[0];
	dir[1] = m_endPosition[1] - m_startPosition[1];
	dir[2] = m_endPosition[2] - m_startPosition[2];
	double norm = vtkMath::Norm(dir);

	m_normal[0] = dir[0] / norm;
	m_normal[1] = dir[1] / norm;
	m_normal[2] = dir[2] / norm;

	m_length = sqrt(pow((m_endPosition[2] - m_startPosition[2]), 2) + pow((m_endPosition[1] - m_startPosition[1]), 2) + pow((m_endPosition[0] - m_startPosition[0]), 2));

	try {
		InitializeSource();
		InitializePorts();
		InitializeActors();
		this->SetColor(0., 0., 0.9);
	}
	catch (int e) {
		switch (e)
		{
		case POLYDATA_MISSING_ERROR:
			cerr << "Missing polydata...";
			return false;
		case CUT_PLANES_MISSING_ERROR:
			cerr << "Missing cutplanes...";
			return false;
		default:
			return false;
			break;
		}
	}
	return true;
}

bool GuideTube::Create(double s_x, double s_y, double s_z, double e_x, double e_y, double e_z)
{
	m_startPosition[0] = s_x;
	m_startPosition[1] = s_y;
	m_startPosition[2] = s_z;

	m_endPosition[0] = e_x;
	m_endPosition[1] = e_y;
	m_endPosition[2] = e_z;

	this->Create(m_startPosition, m_endPosition);
	return true;
}

bool GuideTube::Create(double* endPos, double* direction, double length)
{
	m_endPosition[0] = endPos[0];
	m_endPosition[1] = endPos[1];
	m_endPosition[2] = endPos[2];

	// Normalize direction vector
	double dir[3];
	double norm = sqrt(pow(direction[0], 2) + pow(direction[1], 2) + pow(direction[2], 2));
	dir[0] = direction[0] / norm;
	dir[1] = direction[1] / norm;
	dir[2] = direction[2] / norm;

	m_normal[0] = dir[0];
	m_normal[1] = dir[1];
	m_normal[2] = dir[2];

	// Compute coordinate of start point
	m_startPosition[0] = m_endPosition[0] - dir[0] * length;
	m_startPosition[1] = m_endPosition[1] - dir[1] * length;
	m_startPosition[2] = m_endPosition[2] - dir[2] * length;

	this->Create(m_startPosition, m_endPosition);
		return true;
}

void GuideTube::InitializeSource()
{
	m_lineSource = vtkSmartPointer<vtkLineSource>::New();
	m_lineSource->SetPoint1(m_startPosition);
	m_lineSource->SetPoint2(m_endPosition);
	m_lineSource->Update();

	m_tubeFilter = vtkTubeFilter::New();
#if VTK_MAJOR_VERSION <= 5
	m_tubeFilter->SetInput(m_lineSource->GetOutput());
#else
	m_tubeFilter->SetInputData(m_lineSource->GetOutput());
#endif
	m_tubeFilter->SetNumberOfSides(50);
	m_tubeFilter->SetRadius(m_radius);
	m_tubeFilter->SetCapping(true);
	m_tubeFilter->Update();

	if (m_polyData == NULL) {
		m_polyData = vtkSmartPointer<vtkPolyData>::New();
	}

	m_polyData = m_tubeFilter->GetOutput();
}

//void GuideTube::InitializeActors()
//{
//	this->InitializeSource();
//
//	for (int i=0; i<4; i++)
//	{
//		if(i<3)
//		{	
//			m_cutter[i] = vtkCutter::New();
//            m_cutter[i]->SetCutFunction(m_cutFunction[i]);
//			m_cutter[i]->SetInputData(0, m_tubeFilter[i]->GetOutput());
//			m_cutter[i]->Update();
//
//			m_boundaryStrips[i] =vtkStripper::New();
//			m_boundaryStrips[i]->SetInputConnection(m_cutter[i]->GetOutputPort());
//			m_boundaryStrips[i]->Update();
//
//			m_polyData[i] = vtkPolyData::New();
//			m_polyData[i]->SetPoints(m_boundaryStrips[i]->GetOutput()->GetPoints());
//			m_polyData[i]->SetPolys(m_boundaryStrips[i]->GetOutput()->GetLines());
//
//			m_triangleFilter[i] = vtkTriangleFilter::New();
//			m_triangleFilter[i]->SetInputData(m_polyData[i]);
//			m_triangleFilter[i]->Update();
//
//			m_polyDataMapper[i] = vtkPolyDataMapper::New();
//			m_polyDataMapper[i]->SetInputConnection(m_triangleFilter[i]->GetOutputPort());
//			m_polyDataMapper[i]->SetScalarVisibility(false);
//			m_polyDataMapper[i]->Update();
//		}
//		else
//		{
//			m_polyDataMapper[i] = vtkPolyDataMapper::New();
//			m_polyDataMapper[i]->SetInputData(m_tubeFilter[i]->GetOutput());
//			m_polyDataMapper[i]->SetScalarVisibility(false);
//			m_polyDataMapper[i]->Update();
//		}
//
//		m_actor[i] = vtkActor::New();
//		m_actor[i]->SetMapper(m_polyDataMapper[i]);
//		m_actor[i]->GetProperty()->SetColor(0,0,1);
//	}
//}

double* GuideTube::GetStartPosition()
{
	return m_startPosition;
}

double* GuideTube::GetEndPosition()
{
	return m_endPosition;
}

double GuideTube::GetLength()
{
	return m_length;
}

void GuideTube::SetRadius(double radius)
{
	m_radius = radius;
	if (m_tubeFilter) {
		m_tubeFilter->SetRadius(radius);
		this->Update();
	}
}

void GuideTube::SetPosition(double* startPos, double* endPos)
{
	m_startPosition[0] = startPos[0];
	m_startPosition[1] = startPos[1];
	m_startPosition[2] = startPos[2];

	m_endPosition[0] = endPos[0];
	m_endPosition[1] = endPos[1];
	m_endPosition[2] = endPos[2];

	double dir[3];
	dir[0] = m_endPosition[0] - m_startPosition[0];
	dir[1] = m_endPosition[1] - m_startPosition[1];
	dir[2] = m_endPosition[2] - m_startPosition[2];
	double norm = vtkMath::Norm(dir);

	m_normal[0] = dir[0] / norm;
	m_normal[1] = dir[1] / norm;
	m_normal[2] = dir[2] / norm;

	m_length = sqrt(pow((m_endPosition[2] - m_startPosition[2]), 2) + pow((m_endPosition[1] - m_startPosition[1]), 2) + pow((m_endPosition[0] - m_startPosition[0]), 2));


	if (!m_lineSource) {
		m_lineSource = vtkSmartPointer<vtkLineSource>::New();
	}

	m_lineSource->SetPoint1(m_startPosition);
	m_lineSource->SetPoint2(m_endPosition);

    this->Update();
}

void GuideTube::SetPosition(double x1, double y1, double z1, double x2, double y2, double z2)
{
	m_startPosition[0] = x1;
	m_startPosition[1] = y1;
	m_startPosition[2] = z1;

	m_endPosition[0] = x2;
	m_endPosition[1] = y2;
	m_endPosition[2] = z2;
}

void GuideTube::Update()
{
	m_lineSource->Update();
	m_tubeFilter->Update();

	AbstractPolyDataAnnotator::Update();
	/*for (int i = 0; i < 3; i++)
	{
		m_boundaryStrips[i]->Update();
		m_polyData->SetPoints(m_boundaryStrips[i]->GetOutput()->GetPoints());
		m_polyData->SetPolys(m_boundaryStrips[i]->GetOutput()->GetLines());
		m_triangleFilter[i]->Update();
		m_polyDataMapper[i]->Update();
	}*/
}

double GuideTube::GetRadius()
{
	return m_tubeFilter->GetRadius();
}

vtkSmartPointer<vtkLineSource> GuideTube::GetLineSource()
{
	return m_lineSource;
}



double* GuideTube::GetNormal()
{
	return m_normal;
}
