#include "Landmark.h"
#include <QDebug>
#include <array>
#include "vtkPolyData.h"
#include "vtkPointData.h"
#include "vtkLookupTable.h"
#include "vtkRenderer.h"
#include "vtkRenderWindow.h"
#include "vtkProperty.h"
#include "vtkMath.h"
#include "vtkSphereSource.h"
#include "vtkEllipsoidSource.h"

const double Landmark::m_initialNormal[3] = { 0, 0, 1 };

Landmark::Landmark() : AbstractPolyDataAnnotator()//(QObject* parent) : QObject(parent)
{
	m_source			= NULL;
	
	m_shape				= Sphere;

	m_worldPosition[0] = 0.;
	m_worldPosition[1] = 0.;
	m_worldPosition[2] = 0.;

	m_normal[0] = 0;
	m_normal[1] = 0;
	m_normal[2] = 1;

	// This makes sure the member normal align with the transformed actor
	//memcpy(m_initialNormal, m_normal, sizeof(double) * 3);
}

Landmark::~Landmark()
{
	m_source->Delete();
}

/* Copy the orientation and position from user transform to seed */
void Landmark::AlignTransform()
{
	// Prepare user transform without translation information
	vtkSmartPointer<vtkMatrix4x4> tmpMatrix = vtkSmartPointer<vtkMatrix4x4>::New();
	tmpMatrix->DeepCopy(this->m_actor->GetUserTransform()->GetMatrix());
	double tmpPosition[3];
	tmpPosition[0] = tmpMatrix->GetElement(0, 3);
	tmpPosition[1] = tmpMatrix->GetElement(1, 3);
	tmpPosition[2] = tmpMatrix->GetElement(2, 3);


	tmpMatrix->SetElement(0, 3, 0);
	tmpMatrix->SetElement(1, 3, 0);
	tmpMatrix->SetElement(2, 3, 0);

	double tmpOrientation[4];
	memcpy(tmpOrientation, m_initialNormal, sizeof(double) * 3);
	tmpOrientation[3] = 1;
	double newOrientation[3];
	memcpy(newOrientation, tmpMatrix->MultiplyDoublePoint(tmpOrientation), sizeof(double) * 3);

	memcpy(m_worldPosition, tmpPosition, sizeof(double) * 3);
	memcpy(m_normal, newOrientation, sizeof(double) * 3);

}

bool Landmark::Create()
{
	InstallPipeline();
	return true;
}

void Landmark::InstallPipeline()
{
	// Standard install pipeline function
	try {
		//InitializeSource();
		AbstractPolyDataAnnotator::InstallPipeline();
		this->Update();
	}
	catch (int e) {
		cerr << "Error type: " << e << endl;
	}
}

void Landmark::InitializeSource()
{
	if (m_shape == Sphere)
	{ 
		m_source = vtkSphereSource::New();
		vtkSphereSource* source = vtkSphereSource::SafeDownCast(m_source);
		source->SetCenter(0,0,0);
		source->SetRadius(3);
		source->SetPhiResolution(30);
		source->SetThetaResolution(30);
		source->Update();
	}
	else if (m_shape == Ellipsode)
	{
		m_source = vtkEllipsoidSource::New();
		vtkEllipsoidSource* source = vtkEllipsoidSource::SafeDownCast(m_source);
		// Notet that original shape is defined by normal from constructor
		source->SetXRadius((m_initialNormal[0] == 0) ? 0.4 : 2.25);
		source->SetYRadius((m_initialNormal[1] == 0) ? 0.4 : 2.25);
		source->SetZRadius((m_initialNormal[2] == 0) ? 0.4 : 2.25);
		source->SetCenter(0,0,0);
		source->SetPhiResolution(10);
		source->SetThetaResolution(10);
		source->Update();
	}

	m_polyData = vtkSmartPointer<vtkPolyData>::New();
	m_polyData->DeepCopy(m_source->GetOutput());
}


void Landmark::SetShape(int shape)
{
	if (shape == Sphere)
		m_shape = Sphere;
	else if (shape == Ellipsode)
		m_shape = Ellipsode;
}

void Landmark::SetRadius(double radius)
{
	for (int i=0;i<4;i++)
	{
		if (m_shape == Sphere) {
			vtkSphereSource* source = vtkSphereSource::SafeDownCast(m_source);
			source->SetRadius(3);
		}
		else if (m_shape == Ellipsode)
		{
			vtkEllipsoidSource* source = vtkEllipsoidSource::SafeDownCast(m_source);
			source->SetXRadius(radius);
			source->SetYRadius(radius);
			source->SetZRadius(radius);
		}
	}
}

void Landmark::SetPosition(double* pos)
{
	// Current position
	double oldPos[3];
	memcpy(oldPos, m_worldPosition, sizeof(double) * 3);

	//char testpos[200];
	//sprintf_s(testpos, "Oldpos: [%.3f, %.3f, %.3f] Newpos: [%.3f, %.3f, %.3f]",
	//	oldPos[0], oldPos[1], oldPos[2],
	//	pos[0], pos[1], pos[2]);
	//std::cout << testpos << endl;

	// New position
	double translateVector[3];
	memcpy(m_worldPosition, pos, sizeof(double) * 3);
	vtkMath::Subtract(pos, oldPos, translateVector);
	//cout << "translateVector: " << translateVector[0] << "," << translateVector[1] << "," << translateVector[2] << endl;
	m_userTransform->Translate(translateVector);

	// Update 2D polydata and actors
	AbstractPolyDataAnnotator::Update();
}

void Landmark::SetPosition(double x, double y, double z)
{
	double pos[3] = { x, y, z };
	this->SetPosition(pos);
}


void Landmark::SetNormal(double* normal)
{
	double inNormal[3];
	memcpy(inNormal, normal, sizeof(double) * 3);
	vtkMath::Normalize(inNormal);

	// old normal
	double curNormal[3];
	
	// Remove translation from the transform
	vtkSmartPointer<vtkMatrix4x4> tempTrans = vtkSmartPointer<vtkMatrix4x4>::New();
	tempTrans->DeepCopy(m_userTransform->GetMatrix());
	for (int i = 0; i < 3; i++)
	{
		tempTrans->SetElement(i, 3, 0);
	}
	memcpy(curNormal, tempTrans->MultiplyDoublePoint(m_initialNormal), sizeof(double) * 3);

	/*cout << "Transform:\n";
	char msg[200];
	sprintf_s(msg, "inNormal: [%.3f,%.3f,%.3f] curNormal: [%.3f,%.3f,%.3f] initNormal: [%.3f,nb %.3f,%.3f]", 
		inNormal[0], inNormal[1], inNormal[2], curNormal[0], curNormal[1], curNormal[2], m_initialNormal[0], m_initialNormal[1], m_initialNormal[2]);
	cout << msg << endl;*/

	// new normal
	m_normal[0] = inNormal[0];
	m_normal[1] = inNormal[1];
	m_normal[2] = inNormal[2];

	m_userTransform->Translate( -m_worldPosition[0], -m_worldPosition[1], -m_worldPosition[2]);

	//Rotate
	double cross[3], bisector[3], dot1, dot2, angle, rotation;

	// Rotate the sliceNode to match the planeWidget normal
	// Potential bug here if the inNormal and the curNormal is exactly oppposite
	vtkMath::Cross(curNormal, m_normal, cross);
	if (vtkMath::Dot(cross, cross) < 0.00000001) {
		// return if two vectors are aligned;
		return;
	}

	vtkMath::Add(m_normal, curNormal, bisector);
	vtkMath::Normalize(bisector);

	dot1 = vtkMath::Dot(curNormal, bisector);
	dot2 = vtkMath::Dot(bisector, m_normal);
	angle = acos(dot1) + acos(dot2);
	// Clamp the dot product to less than 1
	rotation = vtkMath::DegreesFromRadians(angle);

	//std::cout << "Old" << oldNormal[0] << oldNormal[1] << oldNormal[2] << endl;
	//std::cout << "New" << m_normal[0] << m_normal[1] << m_normal[2] << endl;
	//std::cout << "Cross" << cross[0] << cross[1] << cross[2] << endl;
	//std::cout << "Rotation" <<rotation<<endl;

	// Apply the rotation
	m_userTransform->RotateWXYZ(rotation, cross);
	m_userTransform->Translate(	m_worldPosition[0],	 m_worldPosition[1],  m_worldPosition[2]);
	m_transformFilter->Update();

	// Update 2D poloydata and actors
	AbstractPolyDataAnnotator::Update();
}

void Landmark::SetNormal(double x, double y, double z)
{
	double normal[3] = { x, y, z };
	this->SetNormal(normal);
}

void Landmark::Update()
{
	if (m_shape == Sphere) {
		vtkSphereSource* source = vtkSphereSource::SafeDownCast(m_source);
		source->Update();
	}
	else if (m_shape == Ellipsode) {
		vtkEllipsoidSource* source = vtkEllipsoidSource::SafeDownCast(m_source);
		source->Update();
	}
	AbstractPolyDataAnnotator::Update();
}


vtkSmartPointer<vtkActor> Landmark::GetActor()
{
	return this->m_actor;
}

vtkSmartPointer<vtkActor> Landmark::GetActor(int index)
{
	if (index < 3) {
		return m_2Dactors[index];
	}
	else {
		return this->m_actor;
	}
}

double * Landmark::GetPosition()
{
	// Update Position
	vtkMatrix4x4* trans = this->m_userTransform->GetMatrix();
	double temp[3];
	temp[0] = this->m_userTransform->GetMatrix()->GetElement(0, 3);
	temp[1] = this->m_userTransform->GetMatrix()->GetElement(1, 3);
	temp[2] = this->m_userTransform->GetMatrix()->GetElement(2, 3);

	//cout << "temp: " << temp[0] << "," << temp[1] << "," << temp[2] << endl;
	
	memcpy(m_worldPosition, temp, sizeof(double) * 3);
	return m_worldPosition;
}

double Landmark::GetRadius()
{
	if (m_shape == Sphere) {
		vtkSphereSource* source = vtkSphereSource::SafeDownCast(m_source);
		return source->GetRadius();
	}
	else if (m_shape == Ellipsode)
	{
		vtkEllipsoidSource* source = vtkEllipsoidSource::SafeDownCast(m_source);
		return source->GetXRadius();
	}
	else {
		throw WRONG_SHAPE_ERROR;
	}
}

double * Landmark::GetNormal()
{
	double initNormal[4];
	memcpy(initNormal, m_initialNormal, sizeof(double) * 3);
	initNormal[3] = 1;
	vtkSmartPointer<vtkMatrix4x4> tempTrans = vtkSmartPointer<vtkMatrix4x4>::New();
	tempTrans->DeepCopy(m_userTransform->GetMatrix());
	for (int i = 0; i < 3; i++)
	{
		tempTrans->SetElement(i, 3, 0);
	}
	memcpy(m_normal, tempTrans->MultiplyDoublePoint(initNormal), sizeof(double) * 3);
	return m_normal;
}

vtkMatrix4x4* Landmark::GetTransformMatrix()
{
	return m_userTransform->GetMatrix();
}
