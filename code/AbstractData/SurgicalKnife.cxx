#include "SurgicalKnife.h"


SurgicalKnife::SurgicalKnife()
{
	m_polyData = vtkPolyData::New();
	m_thickness = 2;
	m_width = 5;
	m_height = 15;
}

SurgicalKnife::~SurgicalKnife()
{

}

void SurgicalKnife::SetThickness(double thickness)
{
	m_thickness = thickness;
}

void SurgicalKnife::SetHeight(double height)
{
	m_height = height;
}

void SurgicalKnife::SetWidth(double width)
{
	m_width = width;
}

void SurgicalKnife::SetCenter(double center)
{
	m_center = center;
}

void SurgicalKnife::InitializePlate()
{
	m_boxSource = vtkSmartPointer<vtkBox>::New();
	m_BoxRep = vtkSmartPointer<MyBoxRepresentation>::New();
	
	m_boxSource->SetXMax(m_thickness, m_width, m_height);
	m_BoxRep = MyBoxRepresentation::SafeDownCast(m_boxSource);
	
}

vtkSmartPointer<vtkPolyData> SurgicalKnife::GetCurrentPositionAsRepPD()
{
	vtkSmartPointer<vtkPolyData> _pD = vtkSmartPointer<vtkPolyData>::New();
	m_BoxRep->GetPolyData(_pD);
	return _pD;
}

vtkSmartPointer<MyBoxRepresentation> SurgicalKnife::GetCurrentBoxRepresentation()
{
	return m_BoxRep;
}

double SurgicalKnife::GetThickness()
{
	return m_thickness;
}

double SurgicalKnife::GetHeight()
{
	return m_height;
}

double SurgicalKnife::GetWidth()
{
	return m_width;
	
}

double SurgicalKnife::GetCenter()
{
	return m_center;
}

//void SurgicalKnife::Update()
//{
//	
//
//}


