#include "ImageData.h"
#include "vtkImageProperty.h"
#include "vtkImageMapper3D.h"
#include "itkShiftScaleImageFilter.h"
#include "itkExtractImageFilter.h"

Image::Image(QObject *parent) :
    QObject(parent), AbstractImage()
{
	m_itkImage			= ImageType::New();
	m_lut				= NULL;
	m_lutType			= 0;
	m_fixedWindowLevel  = false;
	m_imageType			= FLOAT_IMAGE;
}

Image::~Image()
{
	//m_transform->Delete();
	//m_userMatrix->Delete();
	//m_inverseMatrix->Delete();
}

void Image::SetImage(QString niiFilePath)
{
	AbstractImage::SetImage(niiFilePath);
}

void Image::SetImage(QStringList * dcmFileList)
{
	AbstractImage::SetImage(dcmFileList);
}

void Image::SetImage(ImageType::Pointer image)
{
	m_itkImage->Graft(image);

    this->InstallPipeline();
}

void Image::SetWindow( double window )
{
	for(int i=0;i<3;i++)
	{
		m_i2vConverter->GetViewProp(i)->GetProperty()->SetColorWindow(window);
		m_i2vConverter->GetViewProp(i)->Update();
	}
}

void Image::SetLevel( double level )
{
	for(int i=0;i<3;i++)
	{
		m_i2vConverter->GetViewProp(i)->GetProperty()->SetColorLevel(level);
		m_i2vConverter->GetViewProp(i)->Update();
	}
}

void Image::SetDefaultWindow(double window)
{
	m_defaultWindow = window;
	for (int i = 0; i < 3; i++)
	{
		m_i2vConverter->GetViewProp(i)->GetProperty()->SetColorWindow(window);
		m_i2vConverter->GetViewProp(i)->Update();
	}
	this->SetLUT(m_lutType);
}

void Image::SetDefaultLevel(double level)
{
	m_defaultLevel = level;
	for (int i = 0; i < 3; i++)
	{
		m_i2vConverter->GetViewProp(i)->GetProperty()->SetColorLevel(level);
		m_i2vConverter->GetViewProp(i)->Update();
	}
	this->SetLUT(m_lutType);
}

void Image::InstallPipeline()
{
	AbstractImage::InstallPipeline();
	m_imageData = m_i2vConverter->GetVtkImage();

	//Calculate Window Level
	double minMax[2];
	m_imageData->GetScalarRange(minMax);
	m_defaultWindow = minMax[1] - minMax[0];
	m_defaultLevel = (minMax[1] + minMax[0]) / 2.0;

	if (minMax[1] - minMax[0] < 5)
	{
		minMax[0] = 0;
		minMax[1] = 3;

		m_defaultWindow = 4;
		m_defaultLevel = 2;

	}

	for (int i = 0; i < 3; i++)
	{
		m_viewProp[i]->GetProperty()->SetColorWindow(m_defaultWindow);
		m_viewProp[i]->GetProperty()->SetColorLevel(m_defaultLevel);
		m_viewProp[i]->Update();
	}
	//Set LUT
	this->SetLUT(m_lutType);
}

ImageType::Pointer Image::GetItkImage()
{
	return m_itkImage;
}

vtkImageData* Image::GetImageData()
{
	return m_imageData;
}

double Image::GetWindow()
{
	return m_i2vConverter->GetViewProp(0)->GetProperty()->GetColorWindow();
}

double Image::GetLevel()
{
	return m_i2vConverter->GetViewProp(0)->GetProperty()->GetColorLevel();
}

double Image::GetDefaultWindow()
{
	return m_defaultWindow;
}

double Image::GetDefaultLevel()
{
	return m_defaultLevel;
}

void Image::SetLUT( int lut )
{
	if (m_lut)
	{
		m_lut->Delete();
		m_lut = NULL;
	}

	m_lutType = lut;

	double valuesRange[2];
	valuesRange[0] = m_defaultLevel - m_defaultWindow;
	valuesRange[1] = m_defaultLevel + m_defaultWindow;

	switch (lut)
	{
	case 0: //GrayScale
		m_lut = vtkLookupTable::New();
		m_lut->SetTableRange (valuesRange[0], valuesRange[1]);
		m_lut->SetSaturationRange (0.0, 0.0);
		m_lut->SetHueRange (0.0, 0.0);
		m_lut->SetValueRange (0.0, 1.0);
		m_lut->SetRampToLinear(); 
		m_lut->Build(); //effective built
		break;
	case 1: //GE
		m_lut = vtkLookupTable::New();
		m_lut->SetNumberOfColors(256);

		for(unsigned int j = 0; j < 256; j++)
			m_lut->SetTableValue(j,double(image_ge_lut[j][0])/255.0,double(image_ge_lut[j][1])/255.0,double(image_ge_lut[j][2])/255.0,1.0);

		m_lut->SetTableRange(valuesRange[0],valuesRange[1]);
		m_lut->SetSaturationRange(0, 1.0); // color saturation£®optinal£©
		m_lut->SetHueRange (0.0,1.0);
		m_lut->SetValueRange(0.0, 1.0);
		m_lut->SetRampToLinear(); 
		m_lut->Build(); //effective built
		break;
	case 2: //NIH
		m_lut = vtkLookupTable::New();
		m_lut->SetNumberOfColors(256);

		for(unsigned int j = 0; j < 256; j++)
			m_lut->SetTableValue(j,double(image_nih_lut[j][0])/255.0,double(image_nih_lut[j][1])/255.0,double(image_nih_lut[j][2])/255.0,1.0);

		m_lut->SetTableRange(valuesRange[0],valuesRange[1]);
		m_lut->SetSaturationRange(0, 1.0); // color saturation£®optinal£©
		m_lut->SetHueRange (0.0,1.0);
		m_lut->SetValueRange(0.0, 1.0);
		m_lut->SetRampToLinear(); 
		m_lut->Build(); //effective built
		break;
	case 3: //Gold
		m_lut = vtkLookupTable::New();
		m_lut->SetNumberOfColors(256);

		for(unsigned int j = 0; j < 256; j++)
			m_lut->SetTableValue(j,double(image_gold_lut[j][0])/255.0,double(image_gold_lut[j][1])/255.0,double(image_gold_lut[j][2])/255.0,double(image_gold_lut[j][3])/255.0);

		m_lut->SetTableRange(valuesRange[0],valuesRange[1]);
		m_lut->SetSaturationRange(0, 1.0); // color saturation£®optinal£©
		m_lut->SetHueRange (0.0,1.0);
		m_lut->SetValueRange(0.0, 1.0);
		m_lut->SetRampToLinear(); 
		m_lut->Build(); //effective built
		break;
	case 4: //Sapphire
		m_lut = vtkLookupTable::New();
		m_lut->SetNumberOfColors(256);

		for(unsigned int j = 0; j < 256; j++)
			m_lut->SetTableValue(j,double(image_sapphire_lut[j][0])/255.0,double(image_sapphire_lut[j][1])/255.0,double(image_sapphire_lut[j][2])/255.0,double(image_sapphire_lut[j][3])/255.0);

		m_lut->SetTableRange(valuesRange[0],valuesRange[1]);
		m_lut->SetSaturationRange(0, 1.0); // color saturation£®optinal£©
		m_lut->SetHueRange (0.0,1.0);
		m_lut->SetValueRange(0.0, 1.0);
		m_lut->SetRampToLinear(); 
		m_lut->Build(); //effective built
		break;
	case 5: //Label
		m_lut = vtkLookupTable::New();
		m_lut->SetNumberOfColors(4);
		m_lut->SetTableRange(valuesRange[0],valuesRange[1]);
		m_lut->SetTableValue(0,0,0,0,0);
		m_lut->SetTableValue(1,1,0,0,1);
		m_lut->SetTableValue(2,0,1,0,1);
		m_lut->SetTableValue(3,0,0,1,1);
		m_lut->Build(); //effective built
		break;
	case 6: //Rainbow
		m_lut = vtkLookupTable::New();
		m_lut->SetNumberOfColors(256);
		m_lut->SetTableRange(valuesRange[0], valuesRange[1]);
		m_lut->SetSaturationRange(1.0, 1.0); // color saturation£®optinal£©
		m_lut->SetHueRange(0.667, 0.0);
		m_lut->SetValueRange(0.0, 1.0);
		m_lut->SetRampToLinear();
		m_lut->Build(); //effective built
		break;
	}

	vtkImageProperty* imageProperty = m_i2vConverter->GetViewProp(0)->GetProperty();
	imageProperty->SetLookupTable(m_lut);

	for(int i =0;i<3;i++)
	{
		m_i2vConverter->GetViewProp(i)->SetProperty(imageProperty);
		m_i2vConverter->GetViewProp(i)->Update();
	}
}

int Image::GetLUT()
{
	return m_lutType;
}

ImageType::DirectionType Image::GetDefaultDirection()
{
	return m_defaultDirection;
}

ImageType::PointType Image::GetDefaultOrigin()
{
	return m_defaultOrigin;
}


vtkLookupTable* Image::GetLookUpTable()
{
	return m_lut;
}

bool Image::GetFixedWindowLevel()
{
	return m_fixedWindowLevel;
}

void Image::SetFixedWindowLevel(bool b)
{
	m_fixedWindowLevel = b;
}

