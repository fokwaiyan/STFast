#ifndef SURGICALKNIFE_H
#define SURGICALKNIFE_H

#include "Define.h"
#include "AbstractPolyDataAnnotator.h"

#include <vtkBox.h>
#include <vtkPolyData.h>
#include <vtkTransform.h>

#include "../data/MyBoxRepresentation.h"
#include "../data/MyBoxWidget.h"



class SurgicalKnife : public AbstractPolyData
{
public:
	SurgicalKnife();
	virtual ~SurgicalKnife();
	//Set parameter
	void SetThickness(double);
	void SetHeight(double);
	void SetWidth(double);
	void SetCenter(double);

	//Set position back from list
	void SetPosition(double* startPos, double* endPos);
	void SetPosition(double, double, double, double, double, double);
	void SetStartPosition(double*);		//bounds of actor

	//Get membership variable
	//vtkSmartPointer<vtkBox>	 GetBoxSource();

	//save in list
	vtkSmartPointer<vtkPolyData> GetCurrentPositionAsRepPD();
	vtkSmartPointer<MyBoxRepresentation> GetCurrentBoxRepresentation();
	//double*	 GetCurrentPosition();
	double   GetThickness();
	double   GetHeight();
	double   GetWidth();
	double   GetCenter();

	//void Update();

protected:
	virtual void InitializePlate();
	double	m_thickness;
	double  m_height;
	double  m_width;
	double  m_center;

	//vtkSmartPointer<vtkBox>

	//vtkSmartPointer<MyBoxWidget>			m_boxWidget;
	vtkSmartPointer<MyBoxRepresentation>	m_BoxRep;
	vtkSmartPointer<vtkBox>					m_boxSource;

};


#endif //SURGICALKNIFE_H
