#ifndef LANDMARK_H
#define LANDMARK_H

#include "vtkSmartPointer.h"
#include <QString>
#include <QList>
#include <QObject>
#include <QString>

#include "vtkContourFilter.h"
#include "vtkPolyDataNormals.h"
#include "vtkStripper.h"
#include "vtkPolyDataMapper.h"
#include "vtkActor.h"
#include "vtkRenderer.h"
#include "vtkClipPolyData.h"
#include "vtkPlane.h"
#include "vtkCutter.h"
#include "vtkStripper.h"
#include "vtkPolyData.h"
#include "vtkTriangleFilter.h"
#include "vtkPolyDataAlgorithm.h"
#include "../data/vtkEllipsoidSource.h"
#include "vtkTransformPolyDataFilter.h"
#include "vtkTransform.h"

//Debug
#include "vtkCellArray.h"
#include "AbstractPolyDataAnnotator.h"
#include "vtkPoints.h"

class Landmark : public AbstractPolyDataAnnotator // : public QObject
{
	//Q_OBJECT

public:
	//Constructor, Destructor
	Landmark(/*QObject* parent = NULL*/);
    virtual ~Landmark();

	/// Set Parameter
	void		 SetShape(int shape);
	void		 SetRadius(double radius);
	virtual void SetPosition(double* pos);
	virtual void SetPosition(double, double, double);
	virtual void SetNormal(double* normal);
	virtual void SetNormal(double, double, double);

	/// Get membership variable
	virtual	vtkSmartPointer<vtkActor>	GetActor();
	virtual	vtkSmartPointer<vtkActor>	GetActor(int);
	virtual double*		GetPosition();
	virtual double*		GetNormal();
	double				GetRadius();
	vtkMatrix4x4*		GetTransformMatrix();

	enum Shape{ Sphere = 1, Ellipsode = 2} ;
    
	/// Pipeline
	virtual void Update();
	/* Align member variables with user transform */
	virtual void AlignTransform();
    bool Create();

	enum LANDMARK_ERROR {
		WRONG_SHAPE_ERROR = 100
	};

protected:
	virtual void InstallPipeline();
	virtual void InitializeSource();

	// Pipeline ------------------------------------------------------------
	vtkPolyDataAlgorithm*			m_source;

    //Parameter
	static const double	m_initialNormal[3];
    QString			m_strUniqueName;
	double			m_worldPosition[3];
	double			m_normal[3];
	int				m_shape;

};

#endif // LANDMARK_H
