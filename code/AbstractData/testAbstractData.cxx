#include <QString>
#include <vtkActor.h>
#include <vtkPlane.h>
#include <vtkObjectFactory.h>
#include <vtkInteractorStyleSwitch.h>
#include <vtkInteractorStyleTrackballActor.h>
#include "MarkedActor.h"
#include "GuideTube.h"
#include "Mesh.h"
#include "Landmark.h"
#include "IodineSeed.h"
#include "ImageData.h"


using namespace std;

#define USINGSTYLE vtkInteractorStyleTrackballActor

class MyInteractoStyleActor : public USINGSTYLE
{
public:
	static MyInteractoStyleActor* New();
	MyInteractoStyleActor() {};
	~MyInteractoStyleActor() {};

	vtkTypeMacro(MyInteractoStyleActor, USINGSTYLE);

	virtual void OnMouseMove() {
		USINGSTYLE::OnMouseMove();
		if (m_leftPressed || m_midPressed) {
			static_cast<AbstractPolyDataAnnotator*>(m_clientData)->Update();
		}
	}

	virtual void OnLeftButtonDown() {
		m_leftPressed = true;
		USINGSTYLE::OnLeftButtonDown();
	}

	virtual void OnLeftButtonUp() {
		m_leftPressed = false;
		USINGSTYLE::OnLeftButtonUp();
	}

	virtual void OnMiddleButtonDown() {
		m_midPressed = true;
		USINGSTYLE::OnMiddleButtonDown();
	}

	virtual void OnMiddleButtonUp() {
		m_midPressed = false;
		USINGSTYLE::OnMiddleButtonUp();
	}

	void SetAbstractPolyData(void* clientData) {
		m_clientData = clientData;
	}

private:
	void* m_clientData;
	bool m_leftPressed;
	bool m_midPressed;

};

#ifdef _WIN64
vtkStandardNewMacro(MyInteractoStyleActor);
#else
template <typename TInImage>
vtkStandardNewMacro(myStyle<TInImage>);
#endif

/// Test Annotator
//int main() {
//	// Create cut planes
//	vtkPlane* cutPlanes[3];
//	for (int i = 0; i < 3; i++)
//	{
//		cutPlanes[i] = vtkPlane::New();
//		cutPlanes[i]->SetOrigin(0, 0, 0);
//	}
//	cutPlanes[0]->SetNormal(0, 0, 1);
//	cutPlanes[1]->SetNormal(0, 1, 0);
//	cutPlanes[2]->SetNormal(1, 0, 0);
//
//	GuideTube* testTube = new GuideTube();
//	testTube->SetCutFunctions(cutPlanes[0], cutPlanes[1], cutPlanes[2]);
//	testTube->SetRadius(1);
//	testTube->Create(-5, -5, -5, 5, 5, 5);
//	testTube->Update();
//
//	testTube->SetActorState(GuideTube::HOVERED);
//
//
//	vtkActor* testtubeActor = testTube->GetActor();
//	testtubeActor->GetProperty()->SetOpacity(0.4);
//
//
//	vector<vtkActor*> renme;
//	renme.push_back(testTube->Get2DActor(0));
//	renme.push_back(testTube->Get2DActor(1));
//	renme.push_back(testTube->Get2DActor(2));
//	renme.push_back(static_cast<vtkActor*>(testtubeActor));
//
//	MyInteractoStyleActor* interactorStyle = MyInteractoStyleActor::New();
//	interactorStyle->SetAbstractPolyData((void*)testTube);
//
//
//	ActorRenderers* renderer = new ActorRenderers();
//	renderer->SetRenderList(renme);
//	renderer->GetInteractor()->SetInteractorStyle(interactorStyle);
//	renderer->Render();
//
//	free(testTube);
//	return 0;
//}

/// Test Mesh
//int main() {
//	QString dataPath("../../../datacenter/Skull.stl");
//
//	Mesh mesh;
//	mesh.SetPolyData(dataPath);
//	mesh.SetOpacity(1);
//	mesh.SetVisibility(true);
//	mesh.GetPolyData()->Print(cout);
//
//	vtkActor* meshActor = mesh.GetActor();
//	
//	RenderActors(GetActor(mesh.GetPolyData()));
//
//	vector<vtkActor*> renme;
//	renme.push_back(meshActor);
//	
//	MyInteractoStyleActor* interactorStyle = MyInteractoStyleActor::New();
//	interactorStyle->SetAbstractPolyData((void*)&mesh);
//	
//	
//	ActorRenderers* renderer = new ActorRenderers();
//	renderer->SetRenderList(renme);
//	renderer->GetInteractor()->SetInteractorStyle(interactorStyle);
//	renderer->Render();
//	return 0;
//}


/// Test Landmark
int main() {
	vtkPlane* cutPlanes[3];
	for (int i = 0; i < 3; i++)
	{
		cutPlanes[i] = vtkPlane::New();
		cutPlanes[i]->SetOrigin(0, 0, 0);
	}
	cutPlanes[0]->SetNormal(0, 0, 1);
	cutPlanes[1]->SetNormal(0, 1, 0);
	cutPlanes[2]->SetNormal(1, 0, 0);

	Landmark* landmark = new Landmark();
	landmark->SetShape(Landmark::Ellipsode);
	landmark->SetCutFunctions(cutPlanes[0], cutPlanes[1], cutPlanes[2]);
	landmark->Create();
	landmark->SetPosition(0, 0, 0.5);
	landmark->SetNormal(1, 1, 1);
	
	landmark->SetOpacity(0.4);
	landmark->GetActor()->GetProperty()->SetColor(1, 0, 0);
	//landmark->GetActor()->GetProperty()->SetOpacity(0.4);

	vector<vtkActor*> renme;
	renme.push_back(landmark->GetActor());
	renme.push_back(landmark->Get2DActor(0));
	renme.push_back(landmark->Get2DActor(1));
	renme.push_back(landmark->Get2DActor(2));

	MyInteractoStyleActor* interactorStyle = MyInteractoStyleActor::New();
	interactorStyle->SetAbstractPolyData((void*)landmark);
		
	return 0;
}

/// Test Iodine seed
//int main() {
//	vtkPlane* cutPlanes[3];
//	for (int i = 0; i < 3; i++)
//	{
//		cutPlanes[i] = vtkPlane::New();
//		cutPlanes[i]->SetOrigin(0, 0, 0);
//	}
//	cutPlanes[0]->SetNormal(0, 0, 1);
//	cutPlanes[1]->SetNormal(0, 1, 0);
//	cutPlanes[2]->SetNormal(1, 0, 0);
//
//	IodineSeed seeds;
//	seeds.SetCutFunctions(cutPlanes[0], cutPlanes[1], cutPlanes[2]);
//	seeds.Create();
//	seeds.SetOrientation(1, 1, 1);
//	seeds.SetPosition(1, 1, 1);
//	seeds.SetSeedActivity(1);
//	seeds.Update();
//
//	vector<vtkActor*> renme;
//	renme.push_back(seeds.GetActor());
//	renme.push_back(seeds.Get2DActor(0));
//	renme.push_back(seeds.Get2DActor(1));
//	renme.push_back(seeds.Get2DActor(2));
//	
//	MyInteractoStyleActor* interactorStyle = MyInteractoStyleActor::New();
//	interactorStyle->SetAbstractPolyData((void*)&seeds);
//			
//			
//	ActorRenderers* renderer = new ActorRenderers();
//	renderer->SetRenderList(renme);
//	renderer->GetInteractor()->SetInteractorStyle(interactorStyle);
//	renderer->Render();
//	return 0;
//}

/// Test Image
//int main() {
//	Image im;
//	im.SetImage(QString::fromStdString("../../../datacenter/ct.nii.gz"));
//	
//	ActorRenderers Ren;
//	for (int i = 0; i < 3; i++)
//	{
//		Ren.AddViewProp(im.GetImageViewProp(i));
//	}
//	Ren.Render();
//}