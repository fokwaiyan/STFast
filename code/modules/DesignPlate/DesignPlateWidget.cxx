#include "DesignPlateWidget.h"
#include "ui_DesignPlateWidget.h"
#include "MainWindow.h"
#include "../data/MyBoxWidget.h"

DesignPlateWidget::DesignPlateWidget(QWidget* parent ):
	AbstractWidget(parent), ui(new Ui::DesignPlateWidget)
{
		ui->setupUi(this);
		connect(ui->newKnifePushBtn, SIGNAL(clicked()), this, SLOT(slotNewKnife()));
		connect(ui->insertKnifePushBtn, SIGNAL(clicked()), this, SLOT(slotInsertKnife()));
		connect(ui->plateComboBox, SIGNAL(currentIndexChanged()), this, SLOT(slotComboIndexChange()));

		m_boxWidget = MyBoxWidget::New();
		//this->_checkIfExist();


}

DesignPlateWidget::~DesignPlateWidget()
{

}

void DesignPlateWidget::_checkIfExist()
{
	MainWindow* mainWnd = MainWindow::GetMainWindow();
	MyViewer* myViewer = mainWnd->GetViewers(3);
	Planner* planner = mainWnd->GetPlanner();

	QString hints;
	bool isNew = false;

	if (planner->ContainsMesh("SurgicalPlate"))
	{
		int ret = mainWnd->AskYesNoCancelQuestion("Object has already been clipped. \"Yes\" to continue or \"No\" to discard previous clipped.", QMessageBox::Ok);
		if (ret == 2)
			return;

		if (ret == 0)		//Add on for clipping
			hints = hints + "Please continue to clip.\n\n";

		if (ret == 1)		//Redo all steps
		{
			planner->RemoveMesh(planner->GetMeshByUniqueName("SurgicalPlate"));
			//for (int i = 0; i < planner->GetMeshListSize(); i++) 
			//{
			//	qDebug() << "Mesh List" << i << ":" << planner->GetMeshList()->at(i)->GetUniqueName() << endl;
			//}
			//hints = hints + "Previous clipping discarded. \n\n";
			isNew = true;
		}
	}
	else
		isNew = true;

	if (isNew)
	{
		Mesh* toClip = new Mesh;
		myViewer->GetDataRenderer()->AddActor(toClip->GetActor());

		toClip->SetPolyData(planner->GetMeshByUniqueName("Mesh")->GetPolyData());
		toClip->SetUniqueName("SurgicalPlate");
		toClip->SetColor(234.0 / 255.0, 189.0 / 255.0, 157.0 / 255.0);

		planner->AppendMesh(toClip);
		planner->GetLatestEffectiveMesh()->GetPolyData()->Print(cout);
	}

	myViewer->GetDataRenderer()->RemoveActor(planner->GetMeshByUniqueName("Mesh")->GetActor());
	myViewer->GetRenderWindow()->Render();

}

void DesignPlateWidget::slotNewKnife()
{
	SurgicalKnife* knife = new SurgicalKnife;
	MainWindow* mainwnd = MainWindow::GetMainWindow();
	Planner* planner = mainwnd->GetPlanner();

	int n = 1;
	QString name = "Knife ";
	QString combinedName = name + QString::number(n);
	if (planner->GetKnifeList()->size() ==0){
		std:cout << "knife name:" << combinedName.toStdString() << endl;
	}
	while (planner->ContainsKnife(combinedName)){
		n++;
		combinedName = name + QString::number(n);
		qDebug() << "Combined Name: " << combinedName;

	}
	
	knife->SetUniqueName(combinedName);
	ui->plateComboBox->insertItem(n - 1, combinedName);
	planner->AppendKnife(knife);
	
	//create Knife 1 in combobox
	//can set the width, height, thickness

}

void DesignPlateWidget::slotInsertKnife()
{
	//get the name of the knife, 
	//set the parameters of the knife list 
	//set to vtkbox and then set the representation
	//to myBoxWidget
	MainWindow* mainWnd = MainWindow::GetMainWindow();
	MyViewer* myViewer = mainWnd->GetViewers(3);
	Planner* planner = mainWnd->GetPlanner();
	
	if (planner->GetMeshList()->size()<=0){
		return;
	}
	QString name = ui->plateComboBox->itemText(ui->plateComboBox->currentIndex());
	std::cout << "Current text:" << name.toStdString() << endl;
	SurgicalKnife* surgicalPlate = planner->GetKnifeByUniqueName(name);
	Mesh* currentMesh = planner->GetLatestEffectiveMesh();

	SurgicalKnife* knife = planner->GetKnifeByUniqueName(ui->plateComboBox->itemText(ui->plateComboBox->currentIndex()));
	knife->SetHeight(ui->heightSpinBox->value());
	knife->SetWidth(ui->heightSpinBox->value());
	knife->SetThickness(ui->thicknessSpinBox->value());
	
	qDebug()<< "Unique name: "<< planner->GetKnifeByUniqueName(ui->plateComboBox->itemText(ui->plateComboBox->currentIndex()))->GetUniqueName();
	qDebug() << "Height:"<< planner->GetKnifeByUniqueName(ui->plateComboBox->itemText(ui->plateComboBox->currentIndex()))->GetHeight();

	vtkSmartPointer<MyBoxRepresentation> boxRep = vtkSmartPointer<MyBoxRepresentation>::New();
	m_boxWidget->SetRepresentation(knife->GetCurrentBoxRepresentation());
	m_boxWidget->SetCurrentRenderer(myViewer->GetDataRenderer());
	m_boxWidget->SetInteractor(myViewer->GetInteractor());
	m_boxWidget->SetMoveFacesEnabled(false);
	currentMesh->GetActor()->GetCenter()
	//m_boxWidget->GetRepresentation()->SetPlaceFactor(1);				// Default is 0.5
	//m_boxWidget->GetRepresentation()->PlaceWidget(currentMesh->GetActor()->GetBounds());
	m_boxWidget->On();

	mainWnd->ResetAllViewerCameraClippingRange();
	
}

void DesignPlateWidget::slotComboIndexChange()
{
	//if combo index change, check the current string
	//get the current string and display the 


}

