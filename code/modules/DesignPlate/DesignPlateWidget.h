#ifndef DESIGNPLATEWIDGET_H
#define  DESIGNPLATEWIDGET_H

#include "Define.h"
//#include "ui_DesignPlateWidget.h"
#include "../code/AbstractWidget/AbstractWidget.h"
#include "Planner.h"
#include "../data/MyBoxWidget.h"

namespace Ui {
	class DesignPlateWidget;
}


class DesignPlateWidget : public AbstractWidget
{
	Q_OBJECT

public:
	explicit DesignPlateWidget(QWidget* parent = 0);
	~DesignPlateWidget();

	//function
	public slots:
	void slotNewKnife();
	void slotInsertKnife();
	void slotComboIndexChange();

	//void slot


protected:
	virtual void _checkIfExist();

private:
	Ui::DesignPlateWidget* ui;
	MyBoxWidget*	m_boxWidget;
	

};




#endif //DESIGNPLATEWIDGET_H
