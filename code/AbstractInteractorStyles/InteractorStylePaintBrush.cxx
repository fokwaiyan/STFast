/*
Author:		Wong, Matthew Lun
Date:		16th, June 2016
Occupation:	Chinese University of Hong Kong,
			Department of Imaging and Inteventional Radiology,
			Junior Research Assistant

Author:		Lok, Ka Hei Jason
Date:		16th, June 2016
Occupation:	Chinese University of Hong Kong,
			Department of Imaging and Inteventional Radiology,
			M.Phil Student

This class allows interactive segmentation on images.

Wong Matthew Lun, Lok Ka Hei
Copyright (C) 2016
*/

#include "InteractorStylePaintBrush.h"
vtkStandardNewMacro(InteractorStylePaintBrush);

#define SEGMENTATION_CIRCLE 1
#define SEGMENTATION_SQUARE 0

InteractorStylePaintBrush::InteractorStylePaintBrush()
{


	m_borderWidget = NULL;
	m_retangleRep = NULL;
	m_circleRep = NULL;
	m_brush = NULL;
	m_brushActor = NULL;
	//Default color: Red
	m_color_r = 255;
	m_color_g = 0;
	m_color_b = 0;
	m_opacity = 255;
	
	this->PAINT_BRUSH_MODE_ENABLED = false;
}

InteractorStylePaintBrush::~InteractorStylePaintBrush()
{
	this->SetPaintBrushModeEnabled(false);

	// Clean again if the upper dosen't clean
	if (m_brushActor != NULL) {
		this->m_viewers[m_orientation]->GetAnnotationRenderer()->RemoveActor(m_brushActor);
		this->m_brushActor->Delete();
		this->m_brushActor = NULL;
		this->m_brush->Delete();
		this->m_brush = NULL;
	}

	if (m_borderWidget != NULL) {
		this->m_borderWidget->Delete();
		this->m_retangleRep->Delete();
		this->m_circleRep->Delete();
	}
}


void InteractorStylePaintBrush::SetDrawColor(int r, int g, int b)
{
	m_color_r = r;
	m_color_g = g;
	m_color_b = b;
}
void InteractorStylePaintBrush::SetDrawColor(int* rgb)
{
	m_color_r = rgb[0];
	m_color_g = rgb[1];
	m_color_b = rgb[2];
}
void InteractorStylePaintBrush::SetDrawOpacity(int opacity)
{
	m_opacity = opacity;
}

void InteractorStylePaintBrush::OnLeftButtonUp()
{
	MainWindow* mainWnd = MainWindow::GetMainWindow();
	if (!m_isDraw)
	{
		this->Write2ImageData();
		mainWnd->GetPlanner()->GetImageByUniqueName("Label")->GetReslicer()->GetViewProp(this->m_orientation)->SetVisibility(true);
		//Clear Layer
		m_brush->SetDrawColor(0, 0, 0, 0);
		this->FillBox3D();
		this->Render();
		m_isDraw = true;
	}
	m_leftFunctioning = false;

}

void InteractorStylePaintBrush::OnLeftButtonDown()
{
	MainWindow* mainWnd = MainWindow::GetMainWindow();
	Image* labelImage = mainWnd->GetPlanner()->GetImageByUniqueName("Label");
	m_leftFunctioning = true;
	m_isDraw = false;
	this->GetInteractor()->GetPicker()->Pick(
		this->GetInteractor()->GetEventPosition()[0],
		this->GetInteractor()->GetEventPosition()[1],
		0,  // always zero.
		m_viewers[m_orientation]->GetDataRenderer());

	double* picked = this->GetInteractor()->GetPicker()->GetPickPosition();

	//Check if valid pick
	// Check if valid pick
	if (!this->CheckValidPick(picked))
		return;

	// Project picked world point to plane and obtain ijk index
	int index[3];
	labelImage->GetReslicer()->GetITKIndexFromVTKImageActorPoint(picked, index, this->m_orientation);

	memcpy(this->m_draw_index_old, index, sizeof(int) * 3);

	// 
	if (m_rightFunctioning == true)
	{
		m_rightFunctioning = false;
	}
	else
	{
		this->ReadfromImageData();
		this->Render();
	}

	mainWnd->GetPlanner()->GetImageByUniqueName("Label")->GetReslicer()->GetViewProp(this->m_orientation)->SetVisibility(false);
	Draw(true);
	this->Render();
}

void InteractorStylePaintBrush::OnRightButtonUp()
{
	MainWindow* mainWnd = MainWindow::GetMainWindow();
	m_rightFunctioning = false;
	if (!m_isDraw)
	{
		this->Write2ImageData();
		mainWnd->GetPlanner()->GetImageByUniqueName("Label")->GetReslicer()->GetViewProp(this->m_orientation)->SetVisibility(true);
		//Clear Layer
		m_brush->SetDrawColor(0, 0, 0, 0);
		this->FillBox3D();
		this->Render();
		m_isDraw = true;
	}
	AbstractInteractorStyleImage::OnRightButtonUp();
}

void InteractorStylePaintBrush::OnRightButtonDown()
{
	MainWindow* mainWnd = MainWindow::GetMainWindow();
	Image* labelImage = mainWnd->GetPlanner()->GetImageByUniqueName("Label");
	m_isDraw = false;

	// return if label image is not found
	if (!labelImage)
		return;

	this->GetInteractor()->GetPicker()->Pick(
		this->GetInteractor()->GetEventPosition()[0],
		this->GetInteractor()->GetEventPosition()[1],
		0,  // always zero.
		m_viewers[m_orientation]->GetDataRenderer());

	double* picked = this->GetInteractor()->GetPicker()->GetPickPosition();


	// Check if valid pick
	if (!this->CheckValidPick(picked))
		return;

	// Project picked world point to plane and obtain ijk index
	int index[3];
	labelImage->GetReslicer()->GetITKIndexFromVTKImageActorPoint(picked, index, this->m_orientation);

	// copy index to memship varaible
	memcpy(m_draw_index_old, index, sizeof(int) * 3);

	if (m_leftFunctioning == true)
	{
		m_leftFunctioning = false;
	}
	else
	{
		this->ReadfromImageData();
		this->Render();
	}


	mainWnd->GetPlanner()->GetImageByUniqueName("Label")->GetReslicer()->GetViewProp(this->m_orientation)->SetVisibility(false);
	this->Draw(false);
	this->Render();
	m_rightFunctioning = true;
	AbstractInteractorStyleImage::OnRightButtonDown();
}

void InteractorStylePaintBrush::OnLeave()
{
	if (this->PAINT_BRUSH_MODE_ENABLED) {
		this->m_borderWidget->Off();
	}

	AbstractInteractorStyleImage::OnLeave();
}

void InteractorStylePaintBrush::OnMouseMove()
{
	if (!this->PAINT_BRUSH_MODE_ENABLED)
		this->SetPaintBrushModeEnabled(true);

	if (m_leftFunctioning == true)
		Draw(true);
	if (m_rightFunctioning == true)
		Draw(false);

	this->UpdateBorderWidgetPosition();
	this->Render();

	if (!m_rightFunctioning && !m_leftFunctioning)
		AbstractInteractorStyleImage::OnMouseMove();
}



void InteractorStylePaintBrush::SetPaintBrushModeEnabled(bool b)
{
	MainWindow* mainWnd = MainWindow::GetMainWindow();
	Image* labelImage = mainWnd->GetPlanner()->GetImageByUniqueName("Label");

	// GetSpacing
	if (labelImage != NULL) {
		labelImage->GetImageData()->GetSpacing(m_spacing);
		labelImage->GetImageData()->GetOrigin(m_origin);
		labelImage->GetImageData()->GetExtent(m_extent);
	}
	else
		// Return if there are no Label image
		return;

	/// Delete stuff if the mode is disabled to save memory
	if (m_borderWidget != NULL)
	{
		m_circleRep->Delete();
		m_retangleRep->Delete();
		m_borderWidget->Delete();
		m_borderWidget = NULL;
	}


	if (m_brush != NULL)
	{
		this->m_viewers[this->m_orientation]->GetAnnotationRenderer()->RemoveActor(m_brushActor);

		m_brush->Delete();
		m_brush = NULL;

		m_brushActor->Delete();
		m_brushActor = NULL;

	}

	if (b)
	{
		m_retangleRep = vtkBorderRepresentation::New();
		m_circleRep = vtkCircleBorderRepresentation::New();
		m_borderWidget = vtkBorderWidget::New();
		m_borderWidget->SetInteractor(this->GetInteractor());
		m_borderWidget->SetCurrentRenderer(m_viewers[m_orientation]->GetAnnotationRenderer());
		m_borderWidget->SetRepresentation(m_retangleRep);
		m_borderWidget->SetManagesCursor(false);
		m_borderWidget->GetBorderRepresentation()->SetMoving(false);
		m_borderWidget->GetBorderRepresentation()->SetProportionalResize(true);
		m_borderWidget->SelectableOff();
		m_borderWidget->ProcessEventsOff();
		m_borderWidget->Off();

		// Delete m_overlay image


		if (this->Interactor)
			this->Interactor->Render();

		/// Set up brush boarder widget
		m_brush = vtkImageCanvasSource2D::New();
		m_brush->SetScalarTypeToUnsignedChar();
		m_brush->SetNumberOfScalarComponents(4);
		switch (m_orientation)
		{
		case 0:
		{
			m_brush->SetExtent(0, 0, m_extent[2], m_extent[3], m_extent[4], m_extent[5]);
			break;
		}
		case 1:
		{
			m_brush->SetExtent(m_extent[0], m_extent[1], 0, 0, m_extent[4], m_extent[5]);
			break;
		}
		case 2:
		{
			m_brush->SetExtent(m_extent[0], m_extent[1], m_extent[2], m_extent[3], 0, 0);
			break;
		}
		}
		m_brush->SetDrawColor(0, 0, 0, 0);
		this->FillBox3D();
		m_brush->Update();
		m_brush->GetOutput()->SetOrigin(m_origin);
		m_brush->GetOutput()->SetSpacing(m_spacing);

		m_brushActor = vtkImageActor::New();
		m_brushActor->SetInputData(m_brush->GetOutput());
		m_brushActor->GetProperty()->SetInterpolationTypeToNearest();
		m_brushActor->SetPickable(false);
		// Set the image actor

		switch (this->m_orientation)
		{
		case 0:
			m_brushActor->SetDisplayExtent(
				0, 0, m_extent[2], m_extent[3], m_extent[4], m_extent[5]);
			break;

		case 1:
			m_brushActor->SetDisplayExtent(
				m_extent[0], m_extent[1], 0, 0, m_extent[4], m_extent[5]);
			break;

		case 2:
			m_brushActor->SetDisplayExtent(
				m_extent[0], m_extent[1], m_extent[2], m_extent[3], 0, 0);
			break;
		}



		m_brushActor->SetUserTransform(labelImage->GetReslicer()->GetMeshTransform());

		m_viewers[m_orientation]->GetAnnotationRenderer()->AddActor(m_brushActor);
		m_viewers[m_orientation]->GetAnnotationRenderer()->Render();
	}

	this->PAINT_BRUSH_MODE_ENABLED = b;
}



void InteractorStylePaintBrush::Draw(bool b)
{
	MainWindow* mainWnd = MainWindow::GetMainWindow();
	Image* labelImage = mainWnd->GetPlanner()->GetImageByUniqueName("Label");
	// Update Brush properties
	m_brushProperty = mainWnd->GetBrushProperty();

	if (b) {
		double col[3];
		labelImage->GetLookUpTable()->GetColor(m_brushProperty.labelID, col);
		m_brush->SetDrawColor(col[0]*255., col[1]*255., col[2]*255., 155.);
	}
	else
		m_brush->SetDrawColor(0, 0, 0, 0);

	//Pick
	this->GetInteractor()->GetPicker()->Pick(
		this->GetInteractor()->GetEventPosition()[0],
		this->GetInteractor()->GetEventPosition()[1],
		0,  // always zero.
		m_viewers[m_orientation]->GetDataRenderer());

	double* picked = this->GetInteractor()->GetPicker()->GetPickPosition();

	//Check if valid pick
	if (!this->CheckValidPick(picked))
		return;

	int draw_index[3];
	labelImage->GetReslicer()->GetITKIndexFromVTKImageActorPoint(picked, draw_index, this->m_orientation);

	int x2Dindex, y2Dindex;
	switch (m_orientation)
	{
	case 0:
		x2Dindex = 1;
		y2Dindex = 2;
		break;
	case 1:
		x2Dindex = 0;
		y2Dindex = 2;
		break;
	case 2:
		x2Dindex = 0;
		y2Dindex = 1;
		break;
	}

	//Determine Brush Size and Position

	//Circle
	if (m_brushProperty.shape == SEGMENTATION_CIRCLE) {
		// if size is odd
		DrawCircle(m_draw_index_old[x2Dindex], m_draw_index_old[y2Dindex], draw_index[x2Dindex], draw_index[y2Dindex], m_brushProperty.size / 2.);

	}
	//Default:Square
	else
	{
		int loopLowerBoundX = ceil(-m_brushProperty.size / m_spacing[x2Dindex] / 2.);
		int loopUpperBoundX = ceil(m_brushProperty.size / m_spacing[x2Dindex] / 2.);
		int loopLowerBoundY = ceil(-m_brushProperty.size / m_spacing[y2Dindex] / 2.);
		int loopUpperBoundY = ceil(m_brushProperty.size / m_spacing[y2Dindex] / 2.);

		for (int x = loopLowerBoundX; x < loopUpperBoundX; x++)
		{
			for (int y = loopLowerBoundY; y < loopUpperBoundY; y++)
			{
				this->DrawLine3D(m_draw_index_old[x2Dindex] + x, m_draw_index_old[y2Dindex] + y, draw_index[x2Dindex]+x, draw_index[y2Dindex]+y);
			}
		}

	}
	
	// Reset the old draw index
	for (int i = 0; i < 3; i++)
	{
		m_draw_index_old[i] = draw_index[i];
	}

}

void InteractorStylePaintBrush::FillBox3D()
{
	switch (this->m_orientation)
	{
	case 2:
		m_brush->FillBox(m_extent[0], m_extent[1], m_extent[2], m_extent[3]);
		break;
	case 1:
		for (int x = 0; x < m_extent[1]; x++)
		{
			m_brush->DrawSegment3D(x, 0, m_extent[4], x, 0, m_extent[5]);
		}
		break;
	case 0:
		for (int y = 0; y < m_extent[3]; y++)
		{
			m_brush->DrawSegment3D(0,y, m_extent[4], 0, y, m_extent[5]);
		}
		break;	
	}
}

void InteractorStylePaintBrush::DrawLine3D(int x0, int y0, int x1, int y1)
{
	int index[6];
	switch (m_orientation)
	{
	case 0:
		if (x0 < m_extent[2] || x0 > m_extent[3] || y0 < m_extent[4] || y0 > m_extent[5] || x1 < m_extent[2] || x1 > m_extent[3] || y1 < m_extent[4] || y1 > m_extent[5])
			return;
		index[0] = 0;
		index[1] = x0;
		index[2] = y0;
		index[3] = 0;
		index[4] = x1;
		index[5] = y1;
		break;
	case 1:
		if (x0 < m_extent[0] || x0 > m_extent[1] || y0 < m_extent[4] || y0 > m_extent[5] || x1 < m_extent[0] || x1 > m_extent[1] || y1 < m_extent[4] || y1 > m_extent[5])
			return;
		index[0] = x0;
		index[1] = 0;
		index[2] = y0;
		index[3] = x1;
		index[4] = 0;
		index[5] = y1;
		break;
	case 2:
		if (x0 < m_extent[0] || x0 > m_extent[1] || y0 < m_extent[2] || y0 > m_extent[3] || x1 < m_extent[0] || x1 > m_extent[1] || y1 < m_extent[2] || y1 > m_extent[3])
			return;
		index[0] = x0;
		index[1] = y0;
		index[2] = 0;
		index[3] = x1;
		index[4] = y1;
		index[5] = 0;
		break;
	}
	m_brush->DrawSegment3D(index[0], index[1], index[2], index[3], index[4], index[5]);
}

// Draw lines from (x0, y0) -> (y0, y1) with base as circle
void InteractorStylePaintBrush::DrawCircle(int x0, int y0, int x1, int y1, double radius)
{
	int x2Dindex, y2Dindex;
	switch (m_orientation)
	{
	case 0:
		x2Dindex = 1;
		y2Dindex = 2;
		break;
	case 1:
		x2Dindex = 0;
		y2Dindex = 2;
		break;
	case 2:
		x2Dindex = 0;
		y2Dindex = 1;
		break;
	}

	int loopLowerBoundX = floor(-radius / (this->m_spacing[x2Dindex]));
	int loopUpperBoundX = ceil(radius / (this->m_spacing[x2Dindex]));
	int loopLowerBoundY = floor(-radius / (this->m_spacing[y2Dindex]));
	int loopUpperBoundY = ceil(radius / (this->m_spacing[y2Dindex]));


	// check bound
	if (loopLowerBoundX >= loopUpperBoundX || loopLowerBoundY >= loopUpperBoundY) {
		std::cerr << "Bound calculation failed.";
		return;
	}

	for (int i = loopLowerBoundX; i < loopUpperBoundX; i++)
	{
		for (int j = loopLowerBoundY; j < loopUpperBoundY; j++)
		{
			double d = pow(i * m_spacing[x2Dindex], 2) + pow(j * m_spacing[y2Dindex], 2.);
			if (sqrt(d) < radius)
				this->DrawLine3D(x0 + i, y0 + j, x1 + i, y1 + j);
		}
	}



	
	//int x = 0, y = radius;
	//int d = 1 - radius;   
	//while (x <= y) {
	//	for (int i = -x; i <= x; i++)
	//	{
	//		this->DrawLine3D(x0 + i, y0 + y, x1 + i, y1 + y);
	//		this->DrawLine3D(x0 + i, y0 - y, x1 + i, y1 - y);
	//	}

	//	for (int j = -y; j <= y; j++)
	//	{
	//		this->DrawLine3D(x0 + j, y0 + x, x1 + j, y1 + x);
	//		this->DrawLine3D(x0 + j, y0 - x, x1 + j, y1 - x);
	//	}

	//	if (d<0)
	//		d += (2 * x) + 3;
	//	else {
	//		d += (2 * (x - y)) + 5;
	//		y -= 1;
	//	}
	//	x++;
	//}

	

	/*			//MidPoint Circle algorithm //Th shape of the circle is a little bit wierd
	int x = radius;
	int y = 0;
	int err = 0;

	while (x >= y)
	{
		for (int i = -x; i <= x; i++)
		{
			this->DrawLine3D(x0 + i, y0 + y, x1 + i, y1 + y);
			this->DrawLine3D(x0 + i, y0 - y, x1 + i, y1 - y);
		}

		for (int j = -y; j <= y; j++)
		{
			this->DrawLine3D(x0 + j, y0 + x, x1 + j, y1 + x);
			this->DrawLine3D(x0 + j, y0 - x, x1 + j, y1 - x);
		}

		y += 1;
		err += 1 + 2 * y;
		if (2 * (err - x) + 1 > 0)
		{
			x -= 1;
			err += 1 - 2 * x;
		}
	}
	*/ 
}

void InteractorStylePaintBrush::UpdateBorderWidgetPosition()
{
	if (m_borderWidget == NULL)
		return;

	// Update Brush propertyies
	MainWindow* mainWnd = MainWindow::GetMainWindow();
	Image* labelImage = mainWnd->GetPlanner()->GetImageByUniqueName("Label");
	//m_brushProperty = mainWnd->GetImageSegmentationWidget()->GetBrushProperty();
	m_brushProperty = mainWnd->GetBrushProperty();

	double color[3] = { (double)m_color_r / 255.0,(double)m_color_g / 255.0,(double)m_color_b / 255.0 };
	
	//Restrict the box applying on whole pixel 
	this->GetInteractor()->GetPicker()->Pick(
		this->GetInteractor()->GetEventPosition()[0],
		this->GetInteractor()->GetEventPosition()[1],
		0,  // always zero.
		m_viewers[m_orientation]->GetDataRenderer());

	double* picked = this->GetInteractor()->GetPicker()->GetPickPosition();


	//Check if valid pick
	if (!this->CheckValidPick(picked)) {
		this->m_borderWidget->Off();
		return;
	}

	int ijk[3];
	double xyz[3], XY[3], transedSpacing[4] = { m_spacing[0],m_spacing[1],m_spacing[2],1 };
	labelImage->GetReslicer()->GetITKIndexFromVTKImageActorPoint(picked, ijk, this->m_orientation);
	labelImage->GetReslicer()->GetVTKPointFromITKIndex(ijk, xyz);
	labelImage->GetReslicer()->GetResliceBaseTransform()->MultiplyPoint(transedSpacing, transedSpacing);
	//// Snap the boarder to the right upper corner of the selected pixel;
	//switch (m_orientation)
	//{
	//case 0:
	//	xyz[1] += transedSpacing[1]/2.;
	//	xyz[2] += transedSpacing[2]/2.;
	//	break;
	//case 1:
	//	xyz[0] += transedSpacing[0]/2.;
	//	xyz[2] += transedSpacing[2]/2.;
	//	break;
	//case 2:
	//	xyz[0] += transedSpacing[0]/2.;
	//	xyz[1] += transedSpacing[1]/2.;
	//	break;
	//}

	// Convert the point into display space
	m_viewers[m_orientation]->GetDataRenderer()->SetWorldPoint(xyz[0], xyz[1], xyz[2], 1);
	m_viewers[m_orientation]->GetDataRenderer()->WorldToDisplay();
	m_viewers[m_orientation]->GetDataRenderer()->GetDisplayPoint(XY);
	

	double X1 = XY[0];
	double Y1 = XY[1];
	double display_origin[3];
	double display_size[3];

	/// Obtain a vector correspond to the image size in Display Space
	// Project (size, size, size) to image space
	double sss[4] = { m_brushProperty.size, m_brushProperty.size, m_brushProperty.size, 1 };
	int x2Dindex, y2Dindex;
	switch (m_orientation)
	{
	case 0:
		sss[0] = 0;
		break;
	case 1:
		sss[1] = 0;
		break;
	case 2:
		sss[2] = 0;
		break;
	}
	labelImage->GetReslicer()->GetResliceBaseTransform()->MultiplyPoint(sss, sss);

	// Convert (0,0,0) to display space 
	m_viewers[m_orientation]->GetDataRenderer()->SetWorldPoint(0, 0, 0, 1);
	m_viewers[m_orientation]->GetDataRenderer()->WorldToDisplay();
	m_viewers[m_orientation]->GetDataRenderer()->GetDisplayPoint(display_origin);
	// Convert(size, size, size) to displayspace);
	m_viewers[m_orientation]->GetDataRenderer()->SetWorldPoint(sss);
	m_viewers[m_orientation]->GetDataRenderer()->WorldToDisplay();
	m_viewers[m_orientation]->GetDataRenderer()->GetDisplayPoint(display_size);
	// Obtain vector in display space	switch (m_orientation)
	double X3 = display_size[0] - display_origin[0];
	double Y3 = display_size[1] - display_origin[1];

	m_viewers[m_orientation]->GetDataRenderer()->DisplayToNormalizedDisplay(X1, Y1);
	m_viewers[m_orientation]->GetDataRenderer()->NormalizedDisplayToViewport(X1, Y1);
	m_viewers[m_orientation]->GetDataRenderer()->ViewportToNormalizedViewport(X1, Y1);

	m_viewers[m_orientation]->GetDataRenderer()->DisplayToNormalizedDisplay(X3, Y3);
	m_viewers[m_orientation]->GetDataRenderer()->NormalizedDisplayToViewport(X3, Y3);
	m_viewers[m_orientation]->GetDataRenderer()->ViewportToNormalizedViewport(X3, Y3);

	if (m_brushProperty.shape == SEGMENTATION_SQUARE) {
		X1 = X1 - X3 / 2.0;
		m_borderWidget->SetRepresentation(m_retangleRep);
		Y1 = Y1 - Y3 / 2.0;
		m_retangleRep->SetPosition(X1, Y1);
		m_retangleRep->SetPosition2(X3, Y3);

	}
	else if (m_brushProperty.shape == SEGMENTATION_CIRCLE) //Circle
	{
		m_borderWidget->SetRepresentation(m_circleRep);
		m_circleRep->SetPosition(X1, Y1);
		m_circleRep->SetPosition2(X3, Y3);
	}

	//vtkCoordinate* pos1 = m_borderWidget->GetBorderRepresentation()->GetPositionCoordinate();
	//pos1->SetValue(X1, Y1);

	//vtkCoordinate* pos2 = m_borderWidget->GetBorderRepresentation()->GetPosition2Coordinate();
	//pos2->SetValue(X3, Y3);

	mainWnd->GetPlanner()->GetImageByUniqueName("Label")->GetLookUpTable()->GetColor(m_brushProperty.labelID, color);
	m_borderWidget->GetBorderRepresentation()->GetBorderProperty()->SetColor(color);
	m_borderWidget->GetBorderRepresentation()->GetBorderProperty()->SetLineStipplePattern(0xAAAA);
	m_borderWidget->GetBorderRepresentation()->Modified();
	m_borderWidget->Modified();
	m_borderWidget->On();

	//double rsize[2];
	//m_borderWidget->GetBorderRepresentation()->GetSize(rsize);
	//qDebug() << "Size: " << size;
	//qDebug() << "rSize: " << rsize[0] <<", "<<rsize[1];
	//qDebug() << "Event: " << x << y;
	//qDebug() << "Pos3:" << X3 << Y3;
	//qDebug() << "rPos: " << QString::number(m_borderWidget->GetBorderRepresentation()->GetPosition()[0]) << QString::number(m_borderWidget->GetBorderRepresentation()->GetPosition()[1]);
	//qDebug() << "rPos2: " << QString::number(m_borderWidget->GetBorderRepresentation()->GetPosition2()[0]) << QString::number(m_borderWidget->GetBorderRepresentation()->GetPosition2()[1]);

}

void InteractorStylePaintBrush::ReadfromImageData()
{

	MainWindow* mainWnd = MainWindow::GetMainWindow();
	Image* labelMap = mainWnd->GetPlanner()->GetImageByUniqueName("Label");

	//Clear Layer
	m_brush->SetDrawColor(0, 0, 0, 0);
	this->FillBox3D();
	//m_brush->Update();
	m_brush->SetDrawColor(m_color_r, m_color_g, m_color_b, m_opacity);

	// obtain current position ijk
	itk::Index<3> pos;
	int ijk[3];
	labelMap->GetReslicer()->GetITKIndexFromVTKImageActorPoint(this->m_currentPos, ijk, this->m_orientation);

	switch (this->m_orientation)
	{
	case 0:
		for (int y = m_extent[2]; y < m_extent[3]; y++)
		{
			for (int z = m_extent[4]; z < m_extent[5]; z++)
			{
				pos[0] = ijk[0];
				pos[1] = y;
				pos[2] = z;
				PixelType val = labelMap->GetItkImage()->GetPixel(pos);
				if (val > 0) {
					double col[3];
					labelMap->GetLookUpTable()->GetColor(val, col);
					m_brush->SetDrawColor(col[0]*255, col[1]*255, col[2]*255, 255.);
					m_brush->DrawSegment3D(0, y, z, 0, y, z);
				}
			}
		}
		break;
	case 1:
		for (int x = m_extent[0]; x < m_extent[1]; x++)
		{
			for (int z = m_extent[4]; z < m_extent[5]; z++)
			{
				pos[0] = x;
				pos[1] = ijk[1];
				pos[2] = z;
				PixelType val = labelMap->GetItkImage()->GetPixel(pos);
				if (val > 0) {
					double col[3];
					labelMap->GetLookUpTable()->GetColor(val, col);
					m_brush->SetDrawColor(col[0] * 255, col[1] * 255, col[2] * 255, 255.);
					m_brush->DrawSegment3D(x, 0, z, x, 0, z);
				}
			}
		}
		break;
	case 2:
		for (int x = m_extent[0]; x < m_extent[1]; x++)
		{
			for (int y = m_extent[2]; y < m_extent[3]; y++)
			{
				pos[0] = x;
				pos[1] = y;
				pos[2] = ijk[2];

				PixelType val = labelMap->GetItkImage()->GetPixel(pos);
				if (val > 0) {
					double col[3];
					labelMap->GetLookUpTable()->GetColor(val, col);
					m_brush->SetDrawColor(col[0] * 255, col[1] * 255, col[2] * 255, 255.);
					m_brush->DrawPoint(x, y);
				}
			}
		}
		break;
	}

}

void InteractorStylePaintBrush::Write2ImageData()
{
	//Update Layer
	MainWindow* mainWnd = MainWindow::GetMainWindow();
	Image* labelMap = mainWnd->GetPlanner()->GetImageByUniqueName("Label");
	m_brushProperty = mainWnd->GetBrushProperty();

	/// Obtain current cursor position and project xyz to ijk on that plane
	itk::Index<3> pos;
	int ijk[3];
	labelMap->GetReslicer()->GetITKIndexFromVTKImageActorPoint(this->m_currentPos, ijk, this->m_orientation);


	/// Draw
	double pixelval;
	switch (this->m_orientation)
	{
	case 0:
		for (int y = m_extent[2]; y < m_extent[3]; y++)
		{
			for (int z = m_extent[4]; z < m_extent[5]; z++)
			{
				pos[0] = ijk[0];
				pos[1] = y;
				pos[2] = z;
				uchar* val = static_cast<uchar*>(m_brush->GetOutput()->GetScalarPointer(0, y, z));
				if (val[0] > 0 || val[1] > 0 || val[2] > 0 || val[3] > 0) {
					// Check if color of the lookup table matches the color drawn on canvas
					for (int i = 0; i < labelMap->GetLookUpTable()->GetNumberOfColors(); i++)
					{
						double rgba[4];
						uchar rgbaUCHAR[4];
						labelMap->GetLookUpTable()->GetIndexedColor(i, rgba);
						labelMap->GetLookUpTable()->GetColorAsUnsignedChars(rgba, rgbaUCHAR); // convert double to uchar

						if (val[0] == rgbaUCHAR[0] && val[1] == rgbaUCHAR[1] && val[2] == rgbaUCHAR[2]) {
							pixelval = i;
							break;
						}
					}
				}
				else {
					pixelval = 0;
				}
				labelMap->GetItkImage()->SetPixel(pos, pixelval);
			}
		}
		break;
	case 1:
		for (int x = m_extent[0]; x < m_extent[1]; x++)
		{
			for (int z = m_extent[4]; z < m_extent[5]; z++)
			{
				pos[0] = x;
				pos[1] = ijk[1];
				pos[2] = z;
				uchar* val = static_cast<uchar *>(m_brush->GetOutput()->GetScalarPointer(x, 0, z));
				if (val[0] > 0 || val[1] > 0 || val[2] > 0 || val[3] > 0)
					// Check if color of the lookup table matches the color drawn on canvas
					for (int i = 0; i < labelMap->GetLookUpTable()->GetNumberOfColors(); i++)
					{
						double rgba[4];
						uchar rgbaUCHAR[4];
						labelMap->GetLookUpTable()->GetIndexedColor(i, rgba);
						labelMap->GetLookUpTable()->GetColorAsUnsignedChars(rgba, rgbaUCHAR); // convert double to uchar

						if (val[0] == rgbaUCHAR[0] && val[1] == rgbaUCHAR[1] && val[2] == rgbaUCHAR[2]) {
							pixelval = i;
							break;
						}
					}
				else
					pixelval = 0;
				labelMap->GetItkImage()->SetPixel(pos, pixelval);
			}
		}
		break;
	case 2:
		for (int x = m_extent[0]; x < m_extent[1]; x++)
		{
			for (int y = m_extent[2]; y < m_extent[3]; y++)
			{
				pos[0] = x;
				pos[1] = y;
				pos[2] = ijk[2];
				uchar * val = static_cast<uchar *>(m_brush->GetOutput()->GetScalarPointer(x, y, 0));
				if (val[0] > 0 || val[1] > 0 || val[2] > 0 || val[3] > 0)
					// Check if color of the lookup table matches the color drawn on canvas
					for (int i = 0; i < labelMap->GetLookUpTable()->GetNumberOfColors(); i++)
					{
						double rgba[4];
						uchar rgbaUCHAR[4];
						labelMap->GetLookUpTable()->GetIndexedColor(i, rgba);
						labelMap->GetLookUpTable()->GetColorAsUnsignedChars(rgba, rgbaUCHAR); // convert double to uchar

						if (val[0] == rgbaUCHAR[0] && val[1] == rgbaUCHAR[1] && val[2] == rgbaUCHAR[2]) {
							pixelval = i;
							break;
						}
					}
				else
					pixelval = 0;
				labelMap->GetItkImage()->SetPixel(pos, pixelval);
			}
		}
		break;
	}
	labelMap->GetImageData()->Modified();
}


void InteractorStylePaintBrush::Render()
{
	if (m_brush == NULL)
		return;

	m_brush->Update();
	m_brush->GetOutput()->SetOrigin(m_origin);
	m_brush->GetOutput()->SetSpacing(m_spacing);
	for (int i = 0; i<3; i++)
	{
		m_viewers[i]->Render();
	}
}


/* Return true if pick is valid, false ortherwise */
bool InteractorStylePaintBrush::CheckValidPick(double *picked)
{
	MainWindow* mainWnd = MainWindow::GetMainWindow();
	Image* labelMap = mainWnd->GetPlanner()->GetImageByUniqueName("Label");

	if (!labelMap)
		return false;


	/// Check if valid pick on display
	if (picked[0] == 0.0&&picked[1] == 0.0)
		return false;

	/// Check if pick is inside label map
	int* extent = labelMap->GetImageData()->GetExtent();
	
	// obtain ijk index of the clicked point
	int ijk[3];
	labelMap->GetReslicer()->GetITKIndexFromVTKImageActorPoint(picked, ijk, this->m_orientation);

	// index has to be within extent
	for (int i = 0; i < 3; i++)
	{
		int l_index = ijk[i];
		int l_lowerRange = extent[i * 2], l_upperRange = extent[i * 2 + 1];
	
		if (l_index < l_lowerRange || l_index > l_upperRange) {
			return false;
		}
	}

	return true;
}


