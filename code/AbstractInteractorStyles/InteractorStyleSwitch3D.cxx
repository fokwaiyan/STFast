/*
Author:		Wong, Matthew Lun
Date:		16th, June 2016
Occupation:	Chinese University of Hong Kong,
Department of Imaging and Inteventional Radiology,
Junior Research Assistant


This class is based on vtkInteractorStyleSwitch, written to allow easy
switching between 3D interactors.

Wong Matthew Lun
Copyright (C) 2016
*/


#include <vtkCallbackCommand.h>
#include "InteractorStyleSwitch3D.h"
#include "MainWindow.h"
#include "ui_MainWindow.h"

class MyViewer;

vtkStandardNewMacro(InteractorStyleSwitch3D);

InteractorStyleSwitch3D::InteractorStyleSwitch3D()
{
	MainWindow* mainwnd = MainWindow::GetMainWindow();
	// Setup text actor properties
	//m_textActor = vtkTextActor::New
	//m_textActor->SetDisplayPosition(5, 5);
	//m_textActor->GetTextProperty()->SetColor(0.9, 0.9, 0.9);
	//m_textActor->GetTextProperty()->SetBold(1);
	//m_textActor->GetTextProperty()->SetShadow(1);
	//m_textActor->GetTextProperty()->SetShadowOffset(1, 1);

	Navigation = InteractorStyle3DNavigation::New();
	TrackBallCamera = InteractorStyle3DTrackBallCamera::New();
	//PickSeed = InteractorStyle3DPickSeed::New();
	this->CurrentStyle = 0;
}

InteractorStyleSwitch3D::~InteractorStyleSwitch3D()
{
	Navigation->Delete();
	Navigation = 0;

	TrackBallCamera->Delete();
	TrackBallCamera = 0;

	//PickSeed->Delete();
	//PickSeed = 0;
}

void InteractorStyleSwitch3D::InternalUpdate()
{
	// Don't render text before any text is loaded
	if (this->m_viewers[3] && this->m_viewers[3]->GetDataRenderer()->GetNumberOfPropsRendered() != 0) {
		//this->m_viewers[3]->AddAnnotationActor(m_textActor);
		m_textActor = m_viewers[3]->GetCornerTextActor();
	}

	//if (this->CurrentStyleIs3DNavigation()) {
		//this->m_textActor->SetText(0, "Navigation Mode");
	//}
	//else if (this->CurrentStyleIs3DPickSeed()) {
	//	this->m_textActor->SetText(0, "Pick Seed Mode");
	//}
	//else if (this->CurrentStyleIs3DTrackBallCamera()) {
	//	this->m_textActor->SetText(0, "Trackball Camera mode");
	//}

	if (this->Interactor != NULL)
		this->Interactor->Render();
}

void InteractorStyleSwitch3D::SetAutoAdjustCameraClippingRange(int value)
{
	if (value == this->AutoAdjustCameraClippingRange)
	{
		return;
	}

	if (value < 0 || value > 1)
	{
		vtkErrorMacro("Value must be between 0 and 1 for" <<
			" SetAutoAdjustCameraClippingRange");
		return;
	}

	this->AutoAdjustCameraClippingRange = value;
	this->Navigation->SetAutoAdjustCameraClippingRange(value);
	//this->PickSeed->SetAutoAdjustCameraClippingRange(value);
	this->Modified();
}
//
//void InteractorStyleSwitch3D::Set3DMode(int m)
//{
//	switch (m)
//	{
//	case 1:
//		this->SetInteractorStyleTo3DTrackBallCamera();
//		break;
//	case 2:
//		this->SetInteractorStyleTo3DNavigation();
//		break;
//	case 3:
//		this->SetInteractorStyleTo3DPickSeed();
//		break;
//	default:
//		this->SetInteractorStyleTo3DTrackBallCamera();
//		break;
//	}
//}

void InteractorStyleSwitch3D::SetDefaultRenderer(vtkRenderer* renderer)
{
	this->vtkInteractorStyle::SetDefaultRenderer(renderer);
	this->Navigation->SetDefaultRenderer(renderer);
	this->TrackBallCamera->SetDefaultRenderer(renderer);
	//this->PickSeed->SetDefaultRenderer(renderer);

}

void InteractorStyleSwitch3D::SetCurrentRenderer(vtkRenderer* renderer)
{
	this->vtkInteractorStyle::SetCurrentRenderer(renderer);
	this->Navigation->SetCurrentRenderer(renderer);
	this->TrackBallCamera->SetCurrentRenderer(renderer);
	//this->PickSeed->SetCurrentRenderer(renderer);

}

void InteractorStyleSwitch3D::SetViewers(MyViewer **viewers)
{
	this->Navigation->SetViewers(viewers);
	this->TrackBallCamera->SetViewers(viewers);
	//this->PickSeed->SetViewers(viewers);
	AbstractInteractorStyle::SetViewers(viewers);
}

void InteractorStyleSwitch3D::SetOrientation(int orientation)
{
	this->Navigation->SetOrientation(orientation);
	this->TrackBallCamera->SetOrientation(orientation);
	//this->PickSeed->SetOrientation(orientation);
	AbstractInteractorStyle::SetOrientation(orientation);
}

void InteractorStyleSwitch3D::OnChar()
{
	switch (this->Interactor->GetKeyCode())
	{
	case 'q':
	case 'Q':
		this->SetInteractorStyleTo3DNavigation();
		break;
	//case 'w':
	//case 'W':
		//this->SetInteractorStyleTo3DPickSeed();
		//break;
	default:
		break;
	}
}
//
//void InteractorStyleSwitch3D::Change3DInteractionMode(vtkObject *, long unsigned int, void *)
//{
//	MainWindow* mainWnd = MainWindow::GetMainWindow();
//	MyViewer* viewer = mainWnd->GetViewers(3);
//	int state = viewer->GetButtonRep()->GetState() + 1;
//
//	this->Set3DMode(state);
//}

void InteractorStyleSwitch3D::SetInteractor(vtkRenderWindowInteractor *iren)
{
	if (iren == this->Interactor)
	{
		return;
	}
	// if we already have an Interactor then stop observing it
	if (this->Interactor)
	{
		this->Interactor->RemoveObserver(this->EventCallbackCommand);
	}
	this->Interactor = iren;
	// add observers for each of the events handled in ProcessEvents
	if (iren)
	{
		iren->AddObserver(vtkCommand::CharEvent,
			this->EventCallbackCommand,
			this->Priority);

		iren->AddObserver(vtkCommand::DeleteEvent,
			this->EventCallbackCommand,
			this->Priority);
	}
	//this->SetInteractorStyleTo3DTrackBallCamera();
}