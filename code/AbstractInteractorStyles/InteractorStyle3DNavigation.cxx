/*
Author:		Wong, Matthew Lun
Date:		16th, June 2016
Occupation:	Chinese University of Hong Kong,
Department of Imaging and Inteventional Radiology,
Junior Research Assistant


This class is an interactor modified from TrackBallCamera, providing an extra function
of switching slice planes position to the world position clicked on.


Wong Matthew Lun
Copyright (C) 2016
*/

#include <vtkSmartPointer.h>
#include <vtkCenterOfMass.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRendererCollection.h>
#include <vtkRenderer.h>
#include "InteractorStyle3DNavigation.h"
#include "MainWindow.h"

vtkStandardNewMacro(InteractorStyle3DNavigation);

InteractorStyle3DNavigation::InteractorStyle3DNavigation()
{
}

InteractorStyle3DNavigation::~InteractorStyle3DNavigation()
{
}

void InteractorStyle3DNavigation::OnLeftButtonDown()
{
	this->m_clickTimer.restart();
	AbstractInteractorStyle3D::OnLeftButtonDown();
}

void InteractorStyle3DNavigation::OnLeftButtonUp()
{
	if (this->m_clickTimer.elapsed() < 150) {
		this->CalculateIndex();
	}
	AbstractInteractorStyle3D::OnLeftButtonUp();
}

void InteractorStyle3DNavigation::OnChar()
{
	MainWindow* mainwnd = MainWindow::GetMainWindow();

	// Get the keypress
	vtkRenderWindowInteractor *rwi = this->Interactor;
	char key = rwi->GetKeyCode();

	if (key == 'r') {
		// if label is found
		if (mainwnd->GetPlanner()->GetMeshByUniqueName("Label")) {
			// obtain center of label
			vtkSmartPointer<vtkCenterOfMass> comfilter = vtkSmartPointer<vtkCenterOfMass>::New();
			comfilter->SetInputData(mainwnd->GetPlanner()->GetMeshByUniqueName("Label")->GetPolyData());
			comfilter->Update();

			double com[3];
			memcpy(com, comfilter->GetCenter(), sizeof(double) * 3);
			this->Interactor->GetRenderWindow()->GetRenderers()->GetFirstRenderer()->GetActiveCamera()->SetFocalPoint(com);
			this->Interactor->Render();
		}
		else {
			AbstractInteractorStyle3D::OnChar();
		}
	}
	else {
		AbstractInteractorStyle3D::OnChar();
	}
}

bool InteractorStyle3DNavigation::CalculateIndex()
{
	MainWindow* mainWnd = MainWindow::GetMainWindow();

	//Get most updated current value
	mainWnd->GetCursorPosition(m_currentPos);

	//Pick
	this->GetInteractor()->GetPicker()->Pick(
		this->GetInteractor()->GetEventPosition()[0],
		this->GetInteractor()->GetEventPosition()[1],
		0,  // always zero.
		mainWnd->GetViewers(3)->GetRenderWindow()->GetRenderers()->GetFirstRenderer());

	double* picked = this->GetInteractor()->GetPicker()->GetPickPosition();

	//Check if valid pick
	if (picked[0] == 0.0&&picked[1] == 0.0 && picked[2] == 0.0)
		return false;

	//Update current pos
	m_currentPos[0] = picked[0];
	m_currentPos[1] = picked[1];
	m_currentPos[2] = picked[2];

	//Set Spin Box value
	mainWnd->slotChangeCursorPosition(m_currentPos[0], m_currentPos[1], m_currentPos[2]);
	return true;
}

