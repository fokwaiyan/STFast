#ifndef INTERACTOR_STYLE_3D_TRACKBALL_CAMERA_H
#define INTERACTOR_STYLE_3D_TRACKBALL_CAMERA_H

#include <vtkInteractorStyleTrackball.h>
#include "AbstractInteractorStyle3D.h"

class InteractorStyle3DTrackBallCamera : public AbstractInteractorStyle3D
{
public:
	vtkTypeMacro(InteractorStyle3DTrackBallCamera, AbstractInteractorStyle3D);
	static InteractorStyle3DTrackBallCamera* New();

protected:
	InteractorStyle3DTrackBallCamera();
	~InteractorStyle3DTrackBallCamera();


};



#endif // INTERACTOR_STYLE_3D_TRACKBALL_CAMERA_H