/*
Author:		Wong, Matthew Lun
Date:		16th, June 2016
Occupation:	Chinese University of Hong Kong,
			Department of Imaging and Inteventional Radiology,
			Junior Research Assistant

Author:		Lok, Ka Hei Jason
Date:		16th, June 2016
Occupation:	Chinese University of Hong Kong,
			Department of Imaging and Inteventional Radiology,
			M.Phil Student

This class is an interactor modified from TrackBallActor. It allows interactions with the data type
Seed and controls the orientation/position of it.


Wong Matthew Lun, Lok Ka Hei
Copyright (C) 2016
*/

#ifndef INTERACTOR_STYLE_3D_PICKSEED_H
#define INTERACTOR_STYLE_3D_PICKSEED_H

#include <QString>
#include <vtkInteractorStyleTrackballActor.h>
#include "IodineSeed.h"
#include "AbstractPolyData.h"
#include "AbstractInteractorStyle3D.h"

class InteractorStyle3DPickSeed : public vtkInteractorStyleTrackballActor, public AbstractInteractorStyle
{
public:
	vtkTypeMacro(InteractorStyle3DPickSeed, vtkInteractorStyleTrackballActor);
	static InteractorStyle3DPickSeed* New();

	vtkActor * PickActor(int x, int y);

protected:
	InteractorStyle3DPickSeed();
	~InteractorStyle3DPickSeed();

	virtual void OnKeyPress();
	virtual void OnMouseMove();
	virtual void OnLeftButtonDown();
	virtual void OnLeftButtonUp();
	virtual void OnRightButtonDown();
	virtual void OnRightButtonUp();
	virtual void OnMiddleButtonDown();
	virtual void OnMiddleButtonUp();

private:
	void UpdateTransform();

	IodineSeed*			m_selectedSeed;
	AbstractPolyData*	m_clientData;


};


#endif // INTERACTOR_STYLE_3D_PICKSEED_H