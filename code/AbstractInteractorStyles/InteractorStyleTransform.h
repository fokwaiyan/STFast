/*
Author:		Wong, Matthew Lun
Date:		16th, June 2016
Occupation:	Chinese University of Hong Kong,
			Department of Imaging and Inteventional Radiology,
			Junior Research Assistant

Author:		Lok, Ka Hei Jason
Date:		16th, June 2016
Occupation:	Chinese University of Hong Kong,
			Department of Imaging and Inteventional Radiology,
			M.Phil Student

This class allows interactive transforms of images including translation
and rotation.


Wong Matthew Lun, Lok Ka Hei
Copyright (C) 2016
*/

#ifndef INTERACTOR_STYLE_TRANSFORM_H
#define INTERACTOR_STYLE_TRANSFORM_H

#include "AbstractInteractorStyleImage.h"

class InteractorStyleTransform : public AbstractInteractorStyleImage
{
public:
	vtkTypeMacro(InteractorStyleTransform, AbstractInteractorStyleImage);
	static InteractorStyleTransform* New();


protected:
	InteractorStyleTransform();
	~InteractorStyleTransform();

	virtual void OnMouseMove();

private:
	void Prop3DTransform(vtkProp3D*, double*, int, double **, double*);
	void PanImage();
	void SpinImage();

};



#endif