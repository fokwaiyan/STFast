/*
Author:		Wong, Matthew Lun
Date:		16th, June 2016
Occupation:	Chinese University of Hong Kong,
			Department of Imaging and Inteventional Radiology,
			Junior Research Assistant

Author:		Lok, Ka Hei Jason
Date:		16th, June 2016
Occupation:	Chinese University of Hong Kong,
			Department of Imaging and Inteventional Radiology,
			M.Phil Student

This class allows interactive transforms of images including translation
and rotation.


Wong Matthew Lun, Lok Ka Hei
Copyright (C) 2016
*/

#include "InteractorStyleTransform.h"
#include "MainWindow.h"
#include <vtkMath.h>
#include <vtRenderWindowInteractor.h>

vtkStandardNewMacro(InteractorStyleTransform);

InteractorStyleTransform::InteractorStyleTransform()
{
}
 
InteractorStyleTransform::~InteractorStyleTransform()
{
}

void InteractorStyleTransform::OnMouseMove()
{
	if (m_middleFunctioning) {
		this->PanImage();
	}

	if (m_leftFunctioning) {
		this->SpinImage();
	}

	//AbstractInteractorStyleImage::OnMouseMove();
}

void InteractorStyleTransform::Prop3DTransform(vtkProp3D *, double *, int, double **, double *)
{
}

void InteractorStyleTransform::PanImage()
{
	if (!this->CurrentRenderer)
		return;

	MainWindow* mainWnd = MainWindow::GetMainWindow();

	Image* image = mainWnd->GetSelectedImage();

	if (image == NULL)
		return;

	vtkRenderWindowInteractor *rwi = this->Interactor;
	//vtkMatrix4x4* matrix = image->GetMatrix();

	// Use initial center as the m_origin from which to pan

	double *obj_center = image->GetReslicer()->GetReslicer(m_orientation)->GetCenter();

	double disp_obj_center[3], new_pick_point[4];
	double old_pick_point[4], motion_vector[3];

	this->ComputeWorldToDisplay(obj_center[0], obj_center[1], obj_center[2],
		disp_obj_center);

	this->ComputeDisplayToWorld(rwi->GetEventPosition()[0],
		rwi->GetEventPosition()[1],
		disp_obj_center[2],
		new_pick_point);

	this->ComputeDisplayToWorld(rwi->GetLastEventPosition()[0],
		rwi->GetLastEventPosition()[1],
		disp_obj_center[2],
		old_pick_point);

	motion_vector[0] = new_pick_point[0] - old_pick_point[0];
	motion_vector[1] = new_pick_point[1] - old_pick_point[1];
	motion_vector[2] = new_pick_point[2] - old_pick_point[2];

	vtkTransform *t = image->GetTransform();
	t->PostMultiply();
	t->Translate(motion_vector[0], motion_vector[1], motion_vector[2]);

	mainWnd->ResetAllViewerCameraClippingRange();
}

void InteractorStyleTransform::SpinImage()
{
	if (!this->CurrentRenderer)
		return;

	MainWindow* mainWnd = MainWindow::GetMainWindow();
	double curPos[3];
	mainWnd->GetCursorPosition(curPos);
	Image* image = mainWnd->GetSelectedImage();

	if (image == NULL)
		return;

	vtkRenderWindowInteractor *rwi = this->Interactor;
	vtkCamera *cam = this->CurrentRenderer->GetActiveCamera();
	vtkMatrix4x4* matrix = image->GetMatrix();


	// Get the axis to rotate around = vector from eye to m_origin
	double *imageCenter = (double*)malloc(sizeof(double) * 4);
	imageCenter[3] = 1;
	memcpy(imageCenter, image->GetImageData()->GetCenter(), sizeof(double*) * 3);

	//double *obj_center = image->GetReslicer()->GetReslicer(m_orientation)->GetCenter();
	double *obj_center = curPos;


	double motion_vector[3];
	double view_point[3];
	if (cam->GetParallelProjection())
	{
		// If parallel projection, want to get the view plane normal...
		cam->ComputeViewPlaneNormal();
		cam->GetViewPlaneNormal(motion_vector);
	}
	else
	{
		// Perspective projection, get vector from eye to center of actor
		cam->GetPosition(view_point);
		motion_vector[0] = view_point[0] - obj_center[0];
		motion_vector[1] = view_point[1] - obj_center[1];
		motion_vector[2] = view_point[2] - obj_center[2];
		vtkMath::Normalize(motion_vector);
	}

	double disp_obj_center[3];

	this->ComputeWorldToDisplay(obj_center[0], obj_center[1], obj_center[2],
		disp_obj_center);

	double newAngle =
		vtkMath::DegreesFromRadians(atan2(rwi->GetEventPosition()[1] - disp_obj_center[1],
			rwi->GetEventPosition()[0] - disp_obj_center[0]));

	double oldAngle =
		vtkMath::DegreesFromRadians(atan2(rwi->GetEventPosition()[1] - disp_obj_center[1],
			rwi->GetLastEventPosition()[0] - disp_obj_center[0]));

	double scale[3];
	scale[0] = scale[1] = scale[2] = 1.0;

	double **rotate = new double*[1];
	rotate[0] = new double[4];

	rotate[0][0] = newAngle - oldAngle;
	rotate[0][1] = motion_vector[0];
	rotate[0][2] = motion_vector[1];
	rotate[0][3] = motion_vector[2];

	//qDebug() << rotate[0][0];
	//qDebug() << rotate[0][1];
	//qDebug() << rotate[0][2];
	//qDebug() << rotate[0][3];

	vtkTransform *t = image->GetTransform();
	t->PostMultiply();
	t->Translate(-curPos[0], -curPos[1], -curPos[2]);
	t->RotateWXYZ(-rotate[0][0], -rotate[0][1], -rotate[0][2], -rotate[0][3]);
	t->Translate(curPos);

	mainWnd->ResetAllViewerCameraClippingRange();
}
