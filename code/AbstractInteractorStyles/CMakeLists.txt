cmake_minimum_required(VERSION 2.8)
PROJECT(QVTKTest)
 
if(POLICY CMP0020)
  cmake_policy(SET CMP0020 NEW)
endif()

find_package(VTK REQUIRED)
include(${VTK_USE_FILE})

#find_package(vtkUtilities REQUIRED)
#include_directories(${VTK_UTILITIES_INCLUDE_DIR})
 
if(${VTK_VERSION} VERSION_GREATER "6" AND VTK_QT_VERSION VERSION_GREATER "4")
  # Instruct CMake to run moc automatically when needed.
  set(CMAKE_AUTOMOC ON)
  find_package(Qt5Widgets REQUIRED QUIET)
else()
  find_package(Qt4 REQUIRED)
  include(${QT_USE_FILE})
endif()
 
# ./..		codes
# ./../..	Projects
include_directories(
	${CMAKE_CURRENT_SOURCE_DIR} 
	${CMAKE_CURRENT_SOURCE_DIR}/../core/							# includes for MyViewers.cxx or other data providers
	${CMAKE_CURRENT_SOURCE_DIR}/../../include/          # Basic includes
	${CMAKE_BINARY_DIR}	# include ui headers
	${CMAKE_SOURCE_DIR}/code/Core
	${CMAKE_SOURCE_DIR}/code/data
	${CMAKE_SOURCE_DIR}/code/filters
	${CMAKE_SOURCE_DIR}/code/AbstractInteractorStyles
	${CMAKE_SOURCE_DIR}/code/AbstractData
  )
 
link_directories(
  ${CMAKE_CURRENT_SOURCE_DIR}/lib
  
  )

file(GLOB ABSTRACT_INTERACTORS_SRC Abstract*.cxx)
file(GLOB ABSTRACT_INTERACTORS_H Abstract*.h)
file(GLOB INTERACTORS_SRC  InteractorStyle*.cxx PARENT_SCOPE)
file(GLOB INTERACTORS_INC  InteractorStyle*.h StyleSwitchMacro.h PARENT_SCOPE)

# Set variables for parent directory
SET(INTERACTORS_INC ${INTERACTORS_INC} PARENT_SCOPE)
SET(INTERACTORS_SRC ${INTERACTORS_SRC} PARENT_SCOPE)

SET(QT_WRAP ${ABSTRACT_INTERACTORS_H})

if(${VTK_VERSION} VERSION_GREATER "6" AND VTK_QT_VERSION VERSION_GREATER "4")
  # CMAKE_AUTOMOC in ON so the MOC headers will be automatically wrapped.
  add_library(AbstractInteractorStyles ${ABSTRACT_INTERACTORS_SRC} ${QT_WRAP})
  qt5_use_modules(AbstractInteractorStyles Core)
  target_link_libraries(AbstractInteractorStyles ${VTK_LIBRARIES} ${QT_LIBRARIES})
else()
  QT4_WRAP_UI(UISrcs ${UI_FILES})
  QT4_WRAP_CPP(MOCSrcs ${QT_WRAP})
  add_executable(QVTKTest ${CXX_FILES} ${UISrcs} ${MOCSrcs})
 
  if(VTK_LIBRARIES)
    if(${VTK_VERSION} VERSION_LESS "6")
      target_link_libraries(QVTKTest ${VTK_LIBRARIES} ${VTK_UTILITIES_DEBUG_LIB_DIR} QVTK)
    else()
      target_link_libraries(QVTKTest 
        ${VTK_LIBRARIES} 
        debug ${VTK_UTILITIES_DEBUG_LIB_DIR}
        debug AbstractData_msvc2015_win10_64_debug
        )
      target_link_libraries(QVTKTest 
        ${VTK_LIBRARIES} 
        optimized ${VTK_UTILITIES_DEBUG_LIB_DIR}
        optimized AbstractData_msvc2015_win10_64_release
        )
    endif()
  else()
    target_link_libraries(QVTKTest vtkHybrid QVTK vtkViews ${QT_LIBRARIES})
  endif()
endif()