/*
Author:		Wong, Matthew Lun
Date:		16th, June 2016
Occupation:	Chinese University of Hong Kong,
Department of Imaging and Inteventional Radiology,
Junior Research Assistant


This class is based on vtkInteractorStyleSwitch, written to allow easy
switching between 2D interactors.

Wong Matthew Lun
Copyright (C) 2016
*/


#ifndef INTERACTOR_STYLE_SWITCH_H
#define INTERACTOR_STYLE_SWITCH_H

#include <vtkObjectFactory.h>
#include <vtkInteractorStyle.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkInteractorStyleSwitchBase.h>
#include "AbstractInteractorStyle.h"
#include "InteractorStyleNavigation.h"
#include "InteractorStyleWindowLevel.h"
#include "InteractorStyleTransform.h"
#include "InteractorStylePolygonDraw.h"
#include "InteractorStylePaintBrush.h"
#include "StyleSwitchMacro.h"


class InteractorStyleSwitch : public vtkInteractorStyleSwitchBase, public AbstractInteractorStyle
{
public:
	vtkTypeMacro(InteractorStyleSwitch, vtkInteractorStyleSwitchBase);
	static InteractorStyleSwitch* New();

	vtkGetMacro(WindowLevel, InteractorStyleWindowLevel*);
	vtkGetMacro(Navigation, InteractorStyleNavigation*);
	//vtkGetMacro(Transform, InteractorStyleTransform*);
	vtkGetMacro(PolygonDraw, InteractorStylePolygonDraw*);
	vtkGetMacro(PaintBrush, InteractorStylePaintBrush*);

	void SetInteractor(vtkRenderWindowInteractor *iren);

	SetInteractorStyleMacro(Navigation);
	SetInteractorStyleMacro(WindowLevel);
	//SetInteractorStyleMacro(Transform);
	SetInteractorStyleMacro(PolygonDraw);
	SetInteractorStyleMacro(PaintBrush);

	CurrentStyleMacro(Navigation);
	CurrentStyleMacro(WindowLevel);
	//CurrentStyleMacro(Transform);
	CurrentStyleMacro(PolygonDraw);
	CurrentStyleMacro(PaintBrush);

	//bool CurrentStyleIsNavigation();
	//bool CurrentStyleIsWindowLevel();
	//bool CurrentStyleIsTransform();

	virtual void SetDefaultRenderer(vtkRenderer* renderer);
	virtual void SetCurrentRenderer(vtkRenderer* renderer);
	virtual void SetViewers(MyViewer**);
	virtual void SetOrientation(int);

protected:
	InteractorStyleSwitch();
	~InteractorStyleSwitch();

	void ImageDirectionOn();
	void ImageDirectionOff();
	void InternalUpdate();
	void SetAutoAdjustCameraClippingRange(int value);


	InteractorStyleNavigation*	Navigation;
	InteractorStyleWindowLevel* WindowLevel;
	//InteractorStyleTransform*	Transform;
	InteractorStylePolygonDraw* PolygonDraw;
	InteractorStylePaintBrush*	PaintBrush;
	vtkInteractorStyle*			CurrentStyle;
private:
	bool CheckIdentityMatrix(vtkMatrix4x4*);

	vtkSmartPointer<vtkTransform> m_additionalTransformHolder;
};



#endif