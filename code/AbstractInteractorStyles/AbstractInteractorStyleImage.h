/*
Author:		Wong, Matthew Lun
Date:		16th, June 2016
Occupation:	Chinese University of Hong Kong,
Department of Imaging and Inteventional Radiology,
Junior Research Assistant


The abstract interactor class used in medical viewers.
This class contains methods of image related processes, including changing slice position,
zoomming, dragging...etc.


Wong Matthew Lun
Copyright (C) 2016
*/

#ifndef ABSTRACT_INTERACTOR_STYLE_IMAGE_H
#define ABSTRACT_INTERACTOR_STYLE_IMAGE_H

/// VTK
#include <vtkInteractorStyleImage.h>
#include <vtkObjectFactory.h>

#include "AbstractInteractorStyle.h"


class AbstractInteractorStyleImage : public vtkInteractorStyleImage, public AbstractInteractorStyle
{
public:
	vtkTypeMacro(AbstractInteractorStyleImage, vtkInteractorStyleImage);
	static AbstractInteractorStyleImage *New();


	double* GetCurrentPos();
	int*	GetPreviousPosition();



protected:
	AbstractInteractorStyleImage();
	~AbstractInteractorStyleImage();
	
	virtual void OnMouseWheelForward();
	virtual void OnMouseWheelBackward();
	virtual void OnLeftButtonDown();
	virtual void OnLeftButtonUp();
	virtual void OnRightButtonDown();
	virtual void OnRightButtonUp();
	virtual void OnMiddleButtonDown();
	virtual void OnMiddleButtonUp();
	virtual void OnMouseMove();
	virtual void OnChar();
	virtual void OnKeyPressed();

	void MoveSliceForward();
	void MoveSliceBackward();

	double	m_currentPos[3];
	int 	m_previousPosition[2];
};

#endif //ABSTRACT_INTERACTOR_STYLE_IMAGE_H