#ifndef ABSTRACTWIDGET_H
#define ABSTRACTWIDGET_H

#include <QWidget>
#include <QThread>
#include <QMessageBox>

class AbstractWidget: public QWidget
{
    Q_OBJECT
  
public:
    explicit AbstractWidget(QWidget *parent = 0);
    ~AbstractWidget();


	int DisplayMsgBox(QString msg, QMessageBox::Icon icon, QList<QMessageBox::StandardButton> buttonList);

	//bool GetBusying();

public slots:
	virtual void SetBusying(bool b);

protected:
	bool		m_busying;
	QThread*	m_thread;
};

#endif // ABSTRACTWIDGET_H
