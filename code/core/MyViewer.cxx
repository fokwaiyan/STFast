#include "MyViewer.h"
#include "MainWindow.h"
#include "vtkAngleRepresentation2D.h"
#include "vtkCamera.h"
#include "vtkDistanceRepresentation2D.h"
#include "vtkJPEGWriter.h"
#include "vtkPNGWriter.h"
#include "vtkPointHandleRepresentation2D.h"
#include "vtkTIFFWriter.h"
#include "vtkWindowToImageFilter.h"
#include "vtkPNGReader.h"
#include "vtkProperty2D.h"
#include "vtkBalloonRepresentation.h"
#include "vtkCommand.h"


#include "InteractorStyleSwitch.h"
#include "InteractorStyleSwitch3D.h"


MyViewer::MyViewer(QObject *parent) :
    QObject(parent)
{
    m_distanceWidget	= NULL;
    m_angleWidget		= NULL;
	m_oriReader			= vtkSTLReader::New();
	m_oriMapper			= vtkPolyDataMapper::New();
	m_oriWidget			= vtkOrientationMarkerWidget::New();
	m_oriActor			= vtkActor::New();
	m_btnRep			= vtkTexturedButtonRepresentation2D::New();
	m_btnWidget			= vtkButtonWidget::New();

	m_plane				= vtkPlane::New();

	m_iren				= vtkRenderWindowInteractor::New();
    m_renWindow			= vtkRenderWindow::New();
	m_camera			= vtkCamera::New();
    m_dataRenderer		= vtkRenderer::New();
	m_annotationRenderer= vtkRenderer::New();
	m_pointPicker		= vtkPointPicker::New();
	m_propPicker		= vtkPropPicker::New();

	m_style				= InteractorStyleSwitch::New();
	m_threeDStyle		= InteractorStyleSwitch3D::New();

	for (int i = 0; i < 4; i++)
		m_textactor[i] = vtkTextActor::New();

	m_dataRenderer->SetGlobalWarningDisplay(false);
	m_dataRenderer->SetActiveCamera(m_camera);
	m_dataRenderer->GetActiveCamera()->ParallelProjectionOn();
	m_dataRenderer->SetLayer(0);

	m_annotationRenderer->SetGlobalWarningDisplay(false);
	m_annotationRenderer->SetActiveCamera(m_camera);
	m_annotationRenderer->GetActiveCamera()->ParallelProjectionOn();
	m_annotationRenderer->SetLayer(1);
	
	m_cursor3D = vtkCursor3D::New();
	m_cursor3D->SetModelBounds(-15,15,-15,15,-15,15);
	m_cursor3D->SetFocalPoint(0,0,0);
	m_cursor3D->AllOff();
	m_cursor3D->AxesOn();
	m_cursor3D->TranslationModeOn();
	m_cursor3D->Update();

	m_cursorMapper = vtkPolyDataMapper::New();
	m_cursorMapper->SetInputData(m_cursor3D->GetOutput());
	
	m_cursorMatrix = vtkMatrix4x4::New();
	m_cursorMatrix->Identity();

	m_cursorActor = vtkActor::New();
	m_cursorActor->GetProperty()->SetColor(1,1,0);
	m_cursorActor->SetMapper(m_cursorMapper);
	m_cursorActor->SetUserMatrix(m_cursorMatrix);
	m_cursorActor->SetVisibility(false);
	m_cursorActor->SetPickable(false);

	m_annotationRenderer->AddActor(m_cursorActor);
	
    m_renWindow->SetNumberOfLayers(2);
	m_renWindow->AddRenderer(m_dataRenderer);
	m_renWindow->AddRenderer(m_annotationRenderer);
	m_renWindow->SetInteractor(m_iren);

}

MyViewer::~MyViewer()
{
	if (m_distanceWidget)
		m_distanceWidget->Delete();

	if (m_angleWidget)
		m_angleWidget->Delete();

	m_cursorMatrix->Delete();
	m_cursorActor->Delete();
	m_cursorMapper->Delete();
	m_cursor3D->Delete();
	
	m_propPicker->Delete();
	m_pointPicker->Delete();
	m_oriReader->Delete();
	m_oriMapper->Delete();
	m_oriActor->Delete();
	m_oriWidget->Delete();
	m_plane->Delete();
	m_camera->Delete();
	m_dataRenderer->Delete();
	m_annotationRenderer->Delete();
	m_renWindow->Delete();
	m_iren->Delete();
	//m_style->Delete();
}

void MyViewer::Initialize()
{
	m_iren->Initialize();
	if (m_orientation == 3) {
		// Set up text info for 3D viewer
		m_cornerText = vtkCornerAnnotation::New();
		m_cornerText->SetMaximumFontSize(12);
		m_cornerText->GetTextProperty()->SetColor(0.5, 0.5, 0.9);
		m_cornerText->GetTextProperty()->SetBold(1);
		m_cornerText->GetTextProperty()->SetShadow(1);
		m_cornerText->GetTextProperty()->SetShadowOffset(1, 1);
		m_dataRenderer->AddViewProp(m_cornerText);
	}
}

vtkRenderWindow* MyViewer::GetRenderWindow()
{
	return m_renWindow;
}

vtkRenderWindowInteractor* MyViewer::GetInteractor()
{
	return m_iren;
}

vtkRenderer* MyViewer::GetDataRenderer()
{
	return m_dataRenderer;
}

vtkRenderer* MyViewer::GetAnnotationRenderer()
{
	return m_annotationRenderer;
}

vtkPlane* MyViewer::GetPlane()
{
	return m_plane;
}

void MyViewer::SetOrientation(int ori)
{
	m_orientation = ori;
	m_style->SetOrientation(ori);
	m_threeDStyle->SetOrientation(ori);

	switch(m_orientation)
	{
	case 0:
		m_plane->SetNormal(1,0,0);
		m_plane->SetOrigin(0,0,0);
		m_camera->Azimuth(90);
		m_camera->Roll(-90);
		m_iren->SetInteractorStyle(m_style);
		m_iren->SetPicker(m_pointPicker);
		m_iren->Initialize();
		break;  
	case 1:
		m_plane->SetNormal(0,1,0);
		m_plane->SetOrigin(0,0,0);
		m_camera->Elevation(-90);
		m_iren->SetInteractorStyle(m_style);
		m_iren->SetPicker(m_pointPicker);
		m_iren->Initialize();
		break;  
	case 2:
		m_plane->SetNormal(0,0,1);
		m_plane->SetOrigin(0,0,0);
		m_camera->Roll(180);
		m_camera->Elevation(180);
		m_iren->SetInteractorStyle(m_style);
		m_iren->SetPicker(m_pointPicker);
		m_iren->Initialize();
		break;
	case 3:
		m_iren->SetInteractorStyle(m_threeDStyle);
		m_camera->Elevation(-90);		
		m_camera->SetParallelProjection(false);
		m_renWindow->RemoveRenderer(m_annotationRenderer);
		m_annotationRenderer->RemoveActor(m_cursorActor);
		m_iren->SetPicker(m_propPicker);
		m_renWindow->Render();
		m_dataRenderer->AddActor(m_cursorActor);

		m_iren->Initialize();	
		break;
	}
}

void MyViewer::Render()
{
	//DataRenderer and Annotation Renderer share same camera
	if (m_dataRenderer != NULL)
		m_dataRenderer->ResetCameraClippingRange();
	if (m_renWindow)
		m_renWindow->Render();
}

void MyViewer::ResetCamera()
{
	//DataRenderer and Annotation Renderer share same camera
	m_dataRenderer->ResetCameraClippingRange();
	m_dataRenderer->ResetCamera();
}

void MyViewer::AddActor(vtkActor* actor, bool renderAfterward)
{
	m_dataRenderer->AddActor(actor);

	if (renderAfterward)
		m_renWindow->Render();
}

void MyViewer::RemoveActor(vtkActor* actor, bool renderAfterward)
{
	m_dataRenderer->RemoveActor(actor);
	
	if (renderAfterward)
		m_renWindow->Render();
}

void MyViewer::AddViewProp(vtkProp* prop, bool renderAfterward)
{
	// Check if viewProp of label exist
	MainWindow* mainwnd = MainWindow::GetMainWindow();
	Image* labelImage = mainwnd->GetPlanner()->GetImageByUniqueName("Label");

	if (labelImage) {
		m_dataRenderer->RemoveViewProp(labelImage->GetImageViewProp(m_orientation));
		m_dataRenderer->AddViewProp(prop);
		m_dataRenderer->AddViewProp(labelImage->GetImageViewProp(m_orientation));
	}
	else 
		m_dataRenderer->AddViewProp(prop);
	
	if (renderAfterward)
		m_renWindow->Render();
}

void MyViewer::RemoveViewProp(vtkProp* prop, bool renderAfterward)
{
	m_dataRenderer->RemoveViewProp(prop);
	
	if (renderAfterward)
		m_renWindow->Render();
}

void MyViewer::AddAnnotationActor(vtkProp* prop, bool renderAfterward)
{
	//3D view use dataRenderer to add actor to present problem of Z-buffer
	if (m_orientation<3)
		m_annotationRenderer->AddActor(prop);
	else
		m_dataRenderer->AddActor(prop);

	if (renderAfterward)
		m_renWindow->Render();
}

void MyViewer::RemoveAnnotationActor(vtkProp* prop, bool renderAfterward)
{
	//3D view use dataRenderer to add actor to present problem of Z-buffer
	if (m_orientation<3)
		m_annotationRenderer->RemoveActor(prop);
	else
		m_dataRenderer->RemoveActor(prop);

	if (renderAfterward)
		m_renWindow->Render();
}

void MyViewer::createOrientationMarker(QString sex)
{
	MainWindow* mainwnd = MainWindow::GetMainWindow();

	//Orientation marker path
	QString markerFilePath;
	markerFilePath = mainwnd->GetUsefulDir();
	markerFilePath = markerFilePath+ "/resources/orientationMarker/body-";
	markerFilePath = markerFilePath+ sex;	
	markerFilePath = markerFilePath+ "-georndr.stl";

	// Read the poly data for the icon
	m_oriReader->SetFileName(markerFilePath.toStdString().c_str());

	m_oriMapper->SetInputConnection(m_oriReader->GetOutputPort());

	m_oriActor->SetMapper(m_oriMapper);
	m_oriActor->GetProperty()->SetColor(234.0/255.0,189.0/255.0,157.0/255.0);

	m_oriWidget->SetOutlineColor(0, 0, 0);
	m_oriWidget->SetOrientationMarker(m_oriActor);
	m_oriWidget->SetInteractor(m_iren);
	m_oriWidget->SetViewport(0.8, 0.0, 1.2, 0.30);
	m_oriWidget->SetEnabled(1);
	m_oriWidget->InteractiveOff();

	m_renWindow->Render();
}

void MyViewer::SetNavigationInteractionStyle()
{
	this->SetRulerWidgetEnabled(false);
	this->SetProtractorWidgetEnabled(false);

	//m_style->SetMode(1);
}

void MyViewer::SetWindowLevelInteractionStyle()
{
	this->SetRulerWidgetEnabled(false);
	this->SetProtractorWidgetEnabled(false);

	
	//m_style->SetMode(2);
}

void MyViewer::SetPaintBrushInteractionStyle()
{
	this->SetRulerWidgetEnabled(false);
	this->SetProtractorWidgetEnabled(false);

	//m_style->SetMode(3);
}

void MyViewer::SetPolygonInteractionStyle()
{
	this->SetRulerWidgetEnabled(false);
	this->SetProtractorWidgetEnabled(false);

	//m_style->SetMode(4);
}

void MyViewer::SetTransformInteractionStyle()
{
	this->SetRulerWidgetEnabled(false);
	this->SetProtractorWidgetEnabled(false);

	//m_style->SetMode(5);
}

void MyViewer::SetRulerWidgetEnabled( bool b )
{
	if (b==true)
	{	
		this->SetRulerWidgetEnabled(false);

		m_distanceWidget = vtkDistanceWidget::New();
		m_distanceWidget->SetInteractor(this->GetRenderWindow()->GetInteractor());

		// Set a priority higher than our reslice cursor widget
		m_distanceWidget->SetPriority(this->GetRenderWindow()->GetInteractor()->GetInteractorStyle()->GetPriority()+0.1);

		vtkSmartPointer< vtkPointHandleRepresentation2D > pRulerHandleRep	= vtkSmartPointer< vtkPointHandleRepresentation2D >::New();
		vtkSmartPointer< vtkDistanceRepresentation2D >	  pDistanceRep		= vtkSmartPointer< vtkDistanceRepresentation2D >::New();
		pDistanceRep->SetHandleRepresentation(pRulerHandleRep);
		m_distanceWidget->SetRepresentation(pDistanceRep);
		pDistanceRep->InstantiateHandleRepresentation();
		pDistanceRep->SetLabelFormat("%-#6.2f mm");

		m_distanceWidget->CreateDefaultRepresentation();
		m_distanceWidget->EnabledOn();
	}
	else
	{
		if(m_distanceWidget)
		{
			m_distanceWidget->Delete();
			m_distanceWidget = NULL;
		}
		m_renWindow->Render();
	}

}

void MyViewer::SetProtractorWidgetEnabled( bool b )
{
	if (b==true)
	{	
		this->SetProtractorWidgetEnabled(false);

		m_angleWidget = vtkAngleWidget::New();
		m_angleWidget->SetInteractor(this->GetRenderWindow()->GetInteractor());

		m_angleWidget->SetPriority(this->GetRenderWindow()->GetInteractor()->GetInteractorStyle()->GetPriority() + 0.01);

		vtkSmartPointer< vtkPointHandleRepresentation2D > pAngleHandleRep =
			vtkSmartPointer< vtkPointHandleRepresentation2D >::New();
		vtkSmartPointer< vtkAngleRepresentation2D > pAngleRep =
			vtkSmartPointer< vtkAngleRepresentation2D >::New();
		pAngleRep->SetHandleRepresentation(pAngleHandleRep);
		m_angleWidget->SetRepresentation(pAngleRep);
		pAngleRep->InstantiateHandleRepresentation();

		m_angleWidget->CreateDefaultRepresentation();
		m_angleWidget->EnabledOn();
	}
	else
	{
		if(m_angleWidget)
		{
			m_angleWidget->Delete();
			m_angleWidget = NULL;
		}

		m_renWindow->Render();
	}
}

void MyViewer::SetOrientationMarkerEnabled( bool b )
{
	m_oriWidget->SetEnabled(b);
	m_renWindow->Render();
}

int MyViewer::GetOrientation()
{
	return m_orientation;
}


InteractorStyleSwitch* MyViewer::GetMyStyle()
{
	return m_style;
}

InteractorStyleSwitch3D* MyViewer::GetMyStyle3D()
{
	return m_threeDStyle;
}

void MyViewer::SetCursorPosition(double* pos)
{
	m_plane->SetOrigin(pos);
	
	m_cursorActor->SetVisibility(true);
	m_cursorMatrix->SetElement(0, 3, pos[0]);
	m_cursorMatrix->SetElement(1, 3, pos[1]);
	m_cursorMatrix->SetElement(2, 3, pos[2]);
}

void MyViewer::InitializeOrientationText()
{
	int* size = m_annotationRenderer->GetSize();

	double margin = 15;

	int down[2] = { size[0] / 2, 5 };
	int up[2] = { size[0] / 2, size[1] - margin };
	int left[2] = { 5, size[1] / 2 };
	int right[2] = { size[0] - margin, size[1] / 2 };

	for (int i = 0; i < 4; i++)
	{
		m_textactor[i]->GetTextProperty()->SetFontSize(15);
		//m_textactor[i]->GetTextProperty()->SetColor(1.0, 0.7490, 0.0);
		m_textactor[i]->GetTextProperty()->SetColor(1.0, 1.0, 0.0);
		m_annotationRenderer->AddActor2D(m_textactor[i]);
	}

	m_textactor[0]->SetDisplayPosition(up[0], up[1]);
	m_textactor[1]->SetDisplayPosition(down[0], down[1]);
	m_textactor[2]->SetDisplayPosition(left[0], left[1]);
	m_textactor[3]->SetDisplayPosition(right[0], right[1]);

	switch (m_orientation)
	{
	case 0:
		m_textactor[0]->SetInput("S");
		m_textactor[1]->SetInput("I");
		m_textactor[2]->SetInput("A");
		m_textactor[3]->SetInput("P");
		break;
	case 1:
		m_textactor[0]->SetInput("S");
		m_textactor[1]->SetInput("I");
		m_textactor[2]->SetInput("R");
		m_textactor[3]->SetInput("L");
		break;
	case 2:
		m_textactor[0]->SetInput("A");
		m_textactor[1]->SetInput("P");
		m_textactor[2]->SetInput("R");
		m_textactor[3]->SetInput("L");
		break;
	}
}

void MyViewer::ResizeViewer()
{
	int* size = m_annotationRenderer->GetSize();

	double margin = 15;

	int down[2] = { size[0] / 2, 5 };
	int up[2] = { size[0] / 2, size[1] - margin };
	int left[2] = { 5, size[1] / 2 };
	int right[2] = { size[0] - margin, size[1] / 2 };

	m_textactor[0]->SetDisplayPosition(up[0], up[1]);
	m_textactor[1]->SetDisplayPosition(down[0], down[1]);
	m_textactor[2]->SetDisplayPosition(left[0], left[1]);
	m_textactor[3]->SetDisplayPosition(right[0], right[1]);

	if (m_orientation == 3)
	{
		vtkSmartPointer<vtkCoordinate> upperRight = vtkSmartPointer<vtkCoordinate>::New();
		upperRight->SetCoordinateSystemToNormalizedDisplay();
		upperRight->SetValue(1.0, 1.0);

		double bds[6];
		double sz = 25.0;
		double padding = 2.0;
		bds[0] = upperRight->GetComputedDisplayValue(m_dataRenderer)[0] - sz -padding;
		bds[1] = bds[0] + sz -padding;
		bds[2] = upperRight->GetComputedDisplayValue(m_dataRenderer)[1] - sz - padding;
		bds[3] = bds[2] + sz -padding;
		bds[4] = bds[5] = 0.0;

		// Scale to 1, default is .5
		m_btnRep->SetPlaceFactor(1);
		m_btnRep->PlaceWidget(bds);
	}
}
//
//void MyViewer::create3DInteractionButton()
//{
//	MainWindow* mainwnd = MainWindow::GetMainWindow();
//
//	//Trackball Camera path
//	QString cameraFilePath;
//	cameraFilePath = mainwnd->GetUsefulDir();
//	cameraFilePath = cameraFilePath + "/resources/icons/TrackballCamera-frame.png";
//
//	// Read the camera image
//	vtkSmartPointer<vtkPNGReader> cameraReader = vtkSmartPointer<vtkPNGReader>::New();
//	cameraReader->SetFileName(cameraFilePath.toStdString().c_str());
//	cameraReader->Update();
//
//	//Navigation path
//	QString navigateFilPath;
//	navigateFilPath = mainwnd->GetUsefulDir();
//	navigateFilPath = navigateFilPath + "/resources/icons/Navigation-frame.png";
//
//	// Read the navigation image
//	vtkSmartPointer<vtkPNGReader> navigateReader = vtkSmartPointer<vtkPNGReader>::New();
//	navigateReader->SetFileName(navigateFilPath.toStdString().c_str());
//	navigateReader->Update();
//
//	//Seed path
//	QString seedFilPath;
//	seedFilPath = mainwnd->GetUsefulDir();
//	seedFilPath = seedFilPath + "/resources/icons/drug-frame.png";
//
//	// Read the seed image
//	vtkSmartPointer<vtkPNGReader> seedReader = vtkSmartPointer<vtkPNGReader>::New();
//	seedReader->SetFileName(seedFilPath.toStdString().c_str());
//	seedReader->Update();
//	
//	m_btnRep->SetNumberOfStates(3); 
//	m_btnRep->SetButtonTexture(0, cameraReader->GetOutput());
//	m_btnRep->SetButtonTexture(1, navigateReader->GetOutput());
//	m_btnRep->SetButtonTexture(2, seedReader->GetOutput());
//	m_btnRep->SetState(0);
//
//	m_btnWidget->SetInteractor(m_iren);
//	m_btnWidget->SetRepresentation(m_btnRep);
//
//	// Place the widget. Must be done after a render so that the
//	// viewport is defined.
//	// Here the widget placement is in normalized display coordinates
//	vtkSmartPointer<vtkCoordinate> upperRight = vtkSmartPointer<vtkCoordinate>::New();
//	upperRight->SetCoordinateSystemToNormalizedDisplay();
//	upperRight->SetValue(1.0, 1.0);
//
//	double bds[6];
//	double sz = 25.0;
//	double padding = 2.0;
//	bds[0] = upperRight->GetComputedDisplayValue(m_dataRenderer)[0] - sz - padding;
//	bds[1] = bds[0] + sz - padding;
//	bds[2] = upperRight->GetComputedDisplayValue(m_dataRenderer)[1] - sz - padding;
//	bds[3] = bds[2] + sz - padding;
//	bds[4] = bds[5] = 0.0;
//
//	// Scale to 1, default is .5
//	m_btnRep->SetPlaceFactor(1);
//	m_btnRep->PlaceWidget(bds);
//
//	m_btnWidget->On();
//	m_btnWidget->SetEnabled(true);
//	//m_btnWidget->AddObserver(vtkCommand::StateChangedEvent, m_threeDStyle, &InteractorStyleSwitch3D::Change3DInteractionMode, 1);
//
//	m_renWindow->Render();
//}

void MyViewer::SetCursorMatrix(vtkMatrix4x4* matrix)
{
	//Get current position
	double curPos[3];
	curPos[0] = m_cursorMatrix->GetElement(0, 3);
	curPos[1] = m_cursorMatrix->GetElement(1, 3);
	curPos[2] = m_cursorMatrix->GetElement(2, 3);

	m_cursorMatrix->DeepCopy(matrix);

	//Return to current position
	m_cursorMatrix->SetElement(0, 3, curPos[0]);
	m_cursorMatrix->SetElement(1, 3, curPos[1]);
	m_cursorMatrix->SetElement(2, 3, curPos[2]);
}

void MyViewer::SetOrientationTextEnabled(bool b)
{
	for (int i = 0; i < 4; i++)
	{
		m_textactor[i]->SetVisibility(b);
	}
	
	m_renWindow->Render();
}

vtkButtonWidget* MyViewer::GetButtonWidget()
{
	return m_btnWidget;
}

vtkTexturedButtonRepresentation2D* MyViewer::GetButtonRep()
{
	return m_btnRep;
}

vtkCornerAnnotation * MyViewer::GetCornerTextActor()
{
	if (m_cornerText) {
		return m_cornerText;
	}
	else {
		return NULL;
	}
}

void MyViewer::CaptureView(QString filePath)
{
	vtkSmartPointer<vtkWindowToImageFilter> windowToImageFilter = vtkSmartPointer<vtkWindowToImageFilter>::New();
	windowToImageFilter->SetInput(m_renWindow);
	windowToImageFilter->Update();

	if (filePath.contains(".png"))
	{
		vtkSmartPointer<vtkPNGWriter> writer = vtkSmartPointer<vtkPNGWriter>::New();
		writer->SetFileName(filePath.toStdString().c_str());
		writer->SetInputConnection(windowToImageFilter->GetOutputPort());
		writer->Write();
	}
	else if (filePath.contains(".jpg") || filePath.contains(".jpeg"))
	{
		vtkSmartPointer<vtkJPEGWriter> writer = vtkSmartPointer<vtkJPEGWriter>::New();
		writer->SetFileName(filePath.toStdString().c_str());
		writer->SetInputConnection(windowToImageFilter->GetOutputPort());
		writer->Write();
	}
	else if (filePath.contains(".tiff") || filePath.contains(".tif"))
	{
		vtkSmartPointer<vtkTIFFWriter> writer = vtkSmartPointer<vtkTIFFWriter>::New();
		writer->SetFileName(filePath.toStdString().c_str());
		writer->SetInputConnection(windowToImageFilter->GetOutputPort());
		writer->Write();
	}
}
