#ifndef MYVIEWER_H
#define MYVIEWER_H

#include <QObject>
#include "vtkSTLReader.h"
#include "vtkPolyDataMapper.h"
#include "vtkActor.h"
#include "vtkOrientationMarkerWidget.h"
#include "vtkRenderWindow.h"
#include "vtkInteractorStyleTrackballCamera.h"
#include "vtkAngleWidget.h"
#include "vtkDistanceWidget.h"
#include "vtkSmartPointer.h"
#include "vtkCamera.h"
#include "vtkCursor3D.h"
#include "vtkMatrix4x4.h"
#include "vtkPointPicker.h"
#include "vtkPropPicker.h"
#include "vtkTextActor.h"
#include "vtkTextProperty.h"
#include "vtkCornerAnnotation.h"

#include <vtkButtonWidget.h>
#include <vtkTexturedButtonRepresentation2D.h>


class InteractorStyleSwitch;
class InteractorStyleSwitch3D;

class MyViewer : public QObject
{
    Q_OBJECT
public:
    explicit MyViewer(QObject *parent = 0);
    ~MyViewer();

    vtkRenderWindow*				GetRenderWindow();
	vtkRenderWindowInteractor*		GetInteractor();
	vtkRenderer*					GetDataRenderer();
	vtkRenderer*					GetAnnotationRenderer();
	vtkPlane*						GetPlane();
	InteractorStyleSwitch*			GetMyStyle();
	InteractorStyleSwitch3D*		GetMyStyle3D();
	vtkButtonWidget*					GetButtonWidget();
	vtkTexturedButtonRepresentation2D*	GetButtonRep();
	vtkCornerAnnotation*				GetCornerTextActor();

    void SetOrientation(int ori);
	int  GetOrientation();
    //void SetTransformation(vtkMatrix4x4* );
	void Initialize();
    void Render();
	void ResetCamera();

	void SetCursorPosition(double* pos);
	void SetCursorMatrix(vtkMatrix4x4* matrix);
    
    //void InitializeOrientationText();
    //void ResizeRenderWindow();

    void AddActor(vtkActor* actor, bool renderAfterward = false);
	void RemoveActor(vtkActor* actor, bool renderAfterward = false);

	void AddViewProp(vtkProp* prop, bool renderAfterward = false);
	void RemoveViewProp(vtkProp* prop, bool renderAfterward = false);

	void AddAnnotationActor(vtkProp* prop, bool renderAfterward = false);
	void RemoveAnnotationActor(vtkProp* prop, bool renderAfterward = false);

    //Interaction style
    void SetNavigationInteractionStyle();
	void SetWindowLevelInteractionStyle();
    void SetPaintBrushInteractionStyle();
	void SetPolygonInteractionStyle();
	void SetTransformInteractionStyle();


	//3DOrientation Marker
	void createOrientationMarker(QString sex = "m");
	void SetOrientationMarkerEnabled(bool b);

	//3D Interaction Button
	//void create3DInteractionButton();

	///Annotation Text
	void InitializeOrientationText();
	void SetOrientationTextEnabled(bool b);

	void CaptureView(QString filePath);
	
	//Ruler,Protractor
	void SetRulerWidgetEnabled(bool b);
	void SetProtractorWidgetEnabled(bool b);
	
	void ResizeViewer();
signals:

public slots:

private:
    vtkRenderer*				m_dataRenderer;
	vtkRenderer*				m_annotationRenderer;
	vtkCamera*					m_camera;
	vtkRenderWindow*			m_renWindow;
	vtkRenderWindowInteractor*	m_iren;
	vtkPlane*					m_plane;

	vtkCursor3D*				m_cursor3D;
	vtkPolyDataMapper*			m_cursorMapper;
	vtkActor*					m_cursorActor;
	vtkMatrix4x4*				m_cursorMatrix;

    //InteractorStyle
    vtkDistanceWidget*			m_distanceWidget;
    vtkAngleWidget*				m_angleWidget;

	//3D button
	vtkTexturedButtonRepresentation2D*	m_btnRep;
	vtkButtonWidget*					m_btnWidget;

	InteractorStyleSwitch*			m_style;
	InteractorStyleSwitch3D*		m_threeDStyle;
	vtkPointPicker*					m_pointPicker;
	vtkPropPicker*					m_propPicker;

	//Annotation text
	vtkTextActor*				m_textactor[4];
	vtkCornerAnnotation*		m_cornerText;

	int m_orientation;

	vtkSTLReader*				m_oriReader;
	vtkPolyDataMapper*			m_oriMapper;
	vtkActor*					m_oriActor;
	vtkOrientationMarkerWidget* m_oriWidget;
};

#endif // MYVIEWER_H
