#include <QApplication>
#include "MainWindow.h"

//#pragma comment(linker, "/SUBSYSTEM:windows /Entry:mainCRTStartup")	//hide the debugbox
 
int main( int argc, char** argv )
{
  // QT Stuff
  QApplication app( argc, argv );
 
  //set qss style sheet
  QFile styleFile(":/TMSGuide.qss");
  styleFile.open(QFile::ReadOnly);
  QString style(styleFile.readAll());
  app.setStyleSheet(style);
  //
  
  MainWindow mainWindow;
  mainWindow.show();
 
  return app.exec();
}
