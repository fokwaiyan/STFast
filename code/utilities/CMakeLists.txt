#ITK
find_package(ITK REQUIRED)
include(${ITK_USE_FILE})

#VTK
find_package(VTK REQUIRED)
include(${VTK_USE_FILE})

# Instruct CMake to run moc automatically when needed.
set(CMAKE_AUTOMOC ON)
find_package(Qt5Widgets REQUIRED QUIET)


#link other libraries
link_directories(${PROJECT_SOURCE_DIR}/lib)

include_directories(
	${MAIN_INCLUDE_DIRECTORIES}
	${CMAKE_CURRENT_BINARY_DIR}
	${PROJECT_SOURCE_DIR}/include/
	)

#SET(LIBRARY_OUTPUT_PATH ${CMAKE_CURRENT_SOURCE_DIR}/lib)

SET(MODULE_INC_LIST ${PROJECT_SOURCE_DIR}/include/Define.h)
SET(UTILITIES_INC
	${CMAKE_CURRENT_SOURCE_DIR}/vtkROIWidget.h
	)

SET(UTILITIES_SRC
	${CMAKE_CURRENT_SOURCE_DIR}/vtkROIWidget.cxx
	)
	
SET(UTILITIES_INC UTILITIES_INC PARENT_SCOPE)
SET(UTILITIES_SRC UTILITIES_SRC PARENT_SCOPE)

SET(QT_WRAP ${UTILITIES_INC})
add_library(Utitlities ${QT_WRAP} ${UTILITIES_SRC})
qt5_use_modules(Utitlities Core)
target_link_libraries(Utitlities ${ITK_LIBRARIES}  ${VTK_LIBRARIES})

