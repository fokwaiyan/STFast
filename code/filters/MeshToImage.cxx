#include "MeshToImage.h"

#include <QDebug>
#include "vtkPolyData.h"
#include "itkMesh.h"
#include "itkMeshFileReader.h"
#include "itkTriangleMeshToBinaryImageFilter.h"
#include <itkVTKPolyDataWriter.h>

#if VTK_MAJOR_VERSION <= 5
#define SetInputData SetInput
#define AddInputData AddInput
#endif

//constructor:
MeshToImage::MeshToImage(QObject* parent) : AbstractFilter(parent)
{
	m_polyDataToitkMesh = NULL;
	m_triangleFilter	= NULL;
	m_input				= NULL;
	m_output			= NULL;
	m_refImage			= NULL;
	m_imageInsideValue	= 1; 
}

MeshToImage::~MeshToImage(void)
{
	if (m_triangleFilter)		m_triangleFilter->Delete();
	if (m_polyDataToitkMesh)	delete m_polyDataToitkMesh;
}

void MeshToImage::SetInputData(vtkPolyData* mesh)
{
	m_input = mesh;
}

int MeshToImage::Update()
{
	emit Process(0);

	if (m_input == NULL) { emit Process(100); emit Finished("No Input Image"); return 1; }

	//Polydata to image
	emit Process(50);
	m_triangleFilter = vtkTriangleFilter::New();
	m_triangleFilter->SetInputData(m_input);
	m_triangleFilter->Update();

	typedef itk::Mesh< float, 3 > MeshType;
	if (m_polyDataToitkMesh) delete m_polyDataToitkMesh;
	m_polyDataToitkMesh = new vtkPolyDataToitkMesh;
	m_polyDataToitkMesh->SetInput(m_triangleFilter->GetOutput());
	m_polyDataToitkMesh->Update();

	//output format
	typedef itk::TriangleMeshToBinaryImageFilter< MeshType, ImageType > FilterType;
	FilterType::Pointer filter = FilterType::New();
	filter->SetInput(m_polyDataToitkMesh->GetOutput());
	filter->SetInfoImage(m_refImage);
	filter->SetInsideValue(m_imageInsideValue);
	try
	{
		filter->Update();
	}
	catch (itk::ExceptionObject & error)
	{
		std::cerr << "Error: " << error << std::endl;
		return 2;
	}

	m_output = filter->GetOutput();

	if (!m_output) { emit Process(100); emit Finished("No Output Mesh"); return 3; }

	emit Process(100);
	emit Finished();
	return 0;
}

void MeshToImage::SetImageInsideValue(int value)
{
	m_imageInsideValue = value;
}

void MeshToImage::SetReferenceImage(ImageType::Pointer refImage)
{
	m_refImage = refImage;
}

ImageType::Pointer MeshToImage::GetOutput()
{
	return m_output;
}


