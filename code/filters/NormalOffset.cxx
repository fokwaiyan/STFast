#include "NormalOffset.h"
#include <vtkPolyData.h>
#include <vtkSmartPointer.h>



void NormalOffset::SetInputData(vtkPolyData* Input) {
	this->Inputpd = Input;
}
void NormalOffset::SetOffsetFactor(double a) {
	this->Extrusionfactor = a;
}
void NormalOffset::SetNumberOfMeanFilter(int a) {
	if (a <= 0)
		a = 0;
	this->Num_mean_filter = a;
}
void NormalOffset::SetNaiveMode(bool a) {
	this->Naivemode = a;
}
void NormalOffset::SetAggressiveMode(bool a) {
	this->Aggressivemode = a;
}
void NormalOffset::SetSilentMode(bool a) {
	this->Silentmode = a;
}
void NormalOffset::SetMeshQualityControl(bool a) {
	this->Meshqualitycontrol = a;
}
void NormalOffset::SetMaxNumberOfIteration(int a) {
	if (a <= 0)
		a = 0;
	this->Max_num_fix = a;
}
void NormalOffset::SetAngleTolerance(double a) {
	this->Angular_change_tolerance = a;
}
void NormalOffset::SetProgressReportMode(bool a) {
	this->Progressreport = a;
}
vtkPolyData* NormalOffset::GetOutPut() {
	return this->Outputpd;
}

void NormalOffset::Update() {
	double extrusionfactor = this->Extrusionfactor;	
	int num_mean_filter = this->Num_mean_filter;					
	bool naivemode = this->Naivemode;							
	bool aggressivemode = this->Aggressivemode;					

	bool silentmode = this->Silentmode;						
	bool meshqualitycontrol = this->Meshqualitycontrol;				
	int max_num_fix = this->Max_num_fix;						
	double angular_change_tolerance = this->Angular_change_tolerance;		
	bool progressreport = this->Progressreport;					



	vtkSmartPointer<vtkPolyData> originalpd = this->Inputpd;

	vtkSmartPointer<vtkPolyDataNormals> normalinformation = vtkSmartPointer<vtkPolyDataNormals>::New();
	normalinformation->SetInputData(originalpd);
	normalinformation->ComputeCellNormalsOn();
	normalinformation->ComputePointNormalsOn();
	normalinformation->AutoOrientNormalsOn();
	normalinformation->Update();

	vtkSmartPointer<vtkPolyData> pdwithnormal = normalinformation->GetOutput();

	vtkSmartPointer<vtkPoints> originalpointlist = vtkSmartPointer<vtkPoints>::New();
	for (int i = 0; i < pdwithnormal->GetNumberOfPoints(); i++) {
		originalpointlist->InsertNextPoint(pdwithnormal->GetPoint(i));
	}
	vtkSmartPointer<vtkCellArray> originalcelllist = vtkSmartPointer<vtkCellArray>::New();
	for (int i = 0; i < pdwithnormal->GetNumberOfCells(); i++) {
		originalcelllist->InsertNextCell(3);
		for (int j = 0; j < 3; j++)
			originalcelllist->InsertCellPoint(pdwithnormal->GetCell(i)->GetPointId(j));
	}

	vtkSmartPointer<vtkDoubleArray> smoothedcellnormal = vtkSmartPointer<vtkDoubleArray>::New();
	smoothedcellnormal->SetNumberOfComponents(3);
	smoothedcellnormal->SetNumberOfTuples(pdwithnormal->GetNumberOfCells());
	for (int i = 0; i < pdwithnormal->GetNumberOfCells(); i++) {
		smoothedcellnormal->SetTuple3(i, pdwithnormal->GetCellData()->GetNormals()->GetComponent(i, 0), pdwithnormal->GetCellData()->GetNormals()->GetComponent(i, 1), pdwithnormal->GetCellData()->GetNormals()->GetComponent(i, 2));
	}

	vtkSmartPointer<vtkDoubleArray> smoothedpointnormal = vtkSmartPointer<vtkDoubleArray>::New();
	smoothedpointnormal->SetNumberOfComponents(3);
	smoothedpointnormal->SetNumberOfTuples(pdwithnormal->GetNumberOfPoints());
	for (int i = 0; i < pdwithnormal->GetNumberOfPoints(); i++) {
		smoothedpointnormal->SetTuple3(i, pdwithnormal->GetPointData()->GetNormals()->GetComponent(i, 0), pdwithnormal->GetPointData()->GetNormals()->GetComponent(i, 1), pdwithnormal->GetPointData()->GetNormals()->GetComponent(i, 2));
	}


	//Perform mean filter on normal;
	pdwithnormal->BuildLinks();
	for (int n = 0; n<num_mean_filter; n++) {
		vtkSmartPointer<vtkDoubleArray>cachecellnormallist = vtkSmartPointer<vtkDoubleArray>::New();
		cachecellnormallist->SetNumberOfComponents(3);
		cachecellnormallist->SetNumberOfTuples(smoothedcellnormal->GetNumberOfTuples());

		vtkSmartPointer<vtkDoubleArray>cachepointnormallist = vtkSmartPointer<vtkDoubleArray>::New();
		cachepointnormallist->SetNumberOfComponents(3);
		cachepointnormallist->SetNumberOfTuples(smoothedpointnormal->GetNumberOfTuples());

		vtkSmartPointer<vtkIdList> cellinegiborpointids = vtkSmartPointer<vtkIdList>::New();
		vtkSmartPointer<vtkIdList> cellipointinegiborpointids = vtkSmartPointer<vtkIdList>::New();
		vtkSmartPointer<vtkIdList> templist = vtkSmartPointer<vtkIdList>::New();
		for (unsigned long int i = 0; i < pdwithnormal->GetNumberOfCells(); i++) {
			cellinegiborpointids->Reset();
			cellipointinegiborpointids->Reset();

			for (int j = 0; j < 3; j++) {
				templist->Reset();
				templist->InsertNextId(pdwithnormal->GetCell(i)->GetPointId(j));
				pdwithnormal->GetCellNeighbors(i, templist, cellipointinegiborpointids);
				for (int k = 0; k < cellipointinegiborpointids->GetNumberOfIds(); k++) {
					cellinegiborpointids->InsertUniqueId(cellipointinegiborpointids->GetId(k));
				}
			}
			if (cellinegiborpointids->GetNumberOfIds() == 0) {
				cachecellnormallist->SetTuple3(i, 0, 0, 1);
				continue;
			}
			double sum2x = 0, sum2y = 0, sum2z = 0;
			for (int m = 0; m < cellinegiborpointids->GetNumberOfIds(); m++) {
				sum2x += smoothedcellnormal->GetComponent(cellinegiborpointids->GetId(m), 0);
				sum2y += smoothedcellnormal->GetComponent(cellinegiborpointids->GetId(m), 1);
				sum2z += smoothedcellnormal->GetComponent(cellinegiborpointids->GetId(m), 2);
			}
			double norm2 = sqrt(sum2x*sum2x + sum2y*sum2y + sum2z*sum2z);
			if (norm2 == 0) {
				cachecellnormallist->SetTuple3(i, 0, 0, 1);
				continue;
			}
			sum2x = sum2x / norm2;
			sum2y = sum2y / norm2;
			sum2z = sum2z / norm2;
			cachecellnormallist->SetTuple3(i, sum2x, sum2y, sum2z);
		}

		vtkSmartPointer<vtkIdList> pointinegiborcellids = vtkSmartPointer<vtkIdList>::New();
		for (int i = 0; i < pdwithnormal->GetNumberOfPoints(); i++) {
			pointinegiborcellids->Reset();
			pdwithnormal->GetPointCells(i, pointinegiborcellids);
			if (pointinegiborcellids->GetNumberOfIds() == 0) {
				cachepointnormallist->SetTuple3(i, 0, 0, 1);
				continue;
			}
			double sum2x = 0, sum2y = 0, sum2z = 0;
			for (int j = 0; j < pointinegiborcellids->GetNumberOfIds(); j++) {
				sum2x += smoothedcellnormal->GetComponent(pointinegiborcellids->GetId(j), 0);
				sum2y += smoothedcellnormal->GetComponent(pointinegiborcellids->GetId(j), 1);
				sum2z += smoothedcellnormal->GetComponent(pointinegiborcellids->GetId(j), 2);
			}
			double norm2 = sqrt(sum2x*sum2x + sum2y*sum2y + sum2z*sum2z);
			if (norm2 == 0) {
				cachepointnormallist->SetTuple3(i, 0, 0, 1);
				continue;
			}
			sum2x = sum2x / norm2;
			sum2y = sum2y / norm2;
			sum2z = sum2z / norm2;
			cachepointnormallist->SetTuple3(i, sum2x, sum2y, sum2z);
		}

		for (int i = 0; i < smoothedcellnormal->GetNumberOfTuples(); i++) {
			smoothedcellnormal->SetTuple3(i, cachecellnormallist->GetComponent(i, 0), cachecellnormallist->GetComponent(i, 1), cachecellnormallist->GetComponent(i, 2));
		}

		for (int i = 0; i < smoothedpointnormal->GetNumberOfTuples(); i++) {
			smoothedpointnormal->SetTuple3(i, cachepointnormallist->GetComponent(i, 0), cachepointnormallist->GetComponent(i, 1), cachepointnormallist->GetComponent(i, 2));
		}

	}

	vtkSmartPointer<vtkPolyData> inputpd = vtkSmartPointer<vtkPolyData>::New();
	inputpd->SetPoints(originalpointlist);
	inputpd->SetStrips(originalcelllist);
	inputpd->GetCellData()->SetNormals(smoothedcellnormal);
	inputpd->GetPointData()->SetNormals(smoothedpointnormal);

	vtkSmartPointer<vtkPoints> pointlist = vtkSmartPointer<vtkPoints>::New();
	for (int i = 0; i < inputpd->GetNumberOfPoints(); i++) {
		pointlist->InsertNextPoint(inputpd->GetPoint(i)[0], inputpd->GetPoint(i)[1], inputpd->GetPoint(i)[2]);
	}

	vtkSmartPointer<vtkCellArray> celllist = vtkSmartPointer<vtkCellArray>::New();
	for (int i = 0; i < inputpd->GetNumberOfCells(); i++) {
		celllist->InsertNextCell(3);
		for (int j = 0; j < 3; j++) {
			celllist->InsertCellPoint(inputpd->GetCell(i)->GetPointId(j));
		}
	}

	//Calculate mean area of triangle of mesh;
	double maxarea;
	if (meshqualitycontrol) {
		double sumarea = 0, sum2area = 0;
		for (int i = 0; i < inputpd->GetNumberOfCells(); i++) {
			double point1[3], point2[3], point3[3];
			int id1 = inputpd->GetCell(i)->GetPointId(0), id2 = inputpd->GetCell(i)->GetPointId(1), id3 = inputpd->GetCell(i)->GetPointId(2);
			inputpd->GetPoint(id1, point1);
			inputpd->GetPoint(id2, point2);
			inputpd->GetPoint(id3, point3);
			double area = vtkTriangle::TriangleArea(point1, point2, point3);
			sumarea += area;
			sum2area += area*area;
		}
		const double averagearea = sumarea / inputpd->GetNumberOfCells();
		const double sdarea = sqrt(1.0 / inputpd->GetNumberOfCells()*sum2area - averagearea*averagearea);
		double maxarea1 = 1.0*(averagearea + 5.0*sdarea), maxarea2 = 5.0*averagearea; //these parameters are subject to change
		maxarea = maxarea1;
		if (maxarea2 < maxarea1)
			maxarea = maxarea2;
		// Take minimum over these 2 parameter.
	}

	vtkSmartPointer<vtkPolyData> cachepd = vtkSmartPointer<vtkPolyData>::New(); //This cachepd will be changed multiple times in the following WHILE loop

	bool extrusiongreenlight = 0;
	if (naivemode)
		extrusiongreenlight = 1;
	int count = -1;
	int progressreporter = 0;
	const long double PI = 3.14159265358979323846;
	double cos_of_tolerance = cos(angular_change_tolerance * PI / 180);

	while (extrusiongreenlight == 0) {
		count++;
		if (silentmode == 0)
			std::cout << count << " ";

		int marker = progressreporter*0.1*cachepd->GetNumberOfCells();

		cachepd = vtkSmartPointer<vtkPolyData>::New();
		cachepd->SetPoints(pointlist);
		cachepd->SetStrips(celllist);
		cachepd->GetCellData()->SetNormals(smoothedcellnormal);
		cachepd->GetPointData()->SetNormals(smoothedpointnormal);

		//Correct the orientation of normal.
		vtkSmartPointer<vtkPolyDataNormals> fixnormal = vtkSmartPointer<vtkPolyDataNormals>::New();
		fixnormal->SetInputData(cachepd);
		fixnormal->ComputeCellNormalsOff();
		fixnormal->ComputePointNormalsOff();
		fixnormal->AutoOrientNormalsOn();
		fixnormal->Update();

		cachepd = vtkSmartPointer<vtkPolyData>::New();
		cachepd = fixnormal->GetOutput();

		if (count == max_num_fix) {
			if (silentmode == 0)
				std::cout << "Program timeout!\n";
			break;
		}

		cachepd->BuildLinks();

		vtkSmartPointer<vtkDoubleArray> tempcellnormallist = vtkSmartPointer<vtkDoubleArray>::New();
		tempcellnormallist->SetNumberOfComponents(3);
		tempcellnormallist->SetNumberOfTuples(cachepd->GetNumberOfCells());
		vtkSmartPointer<vtkDoubleArray> temppointnormallist = vtkSmartPointer<vtkDoubleArray>::New();
		temppointnormallist->SetNumberOfComponents(3);
		temppointnormallist->SetNumberOfTuples(cachepd->GetNumberOfPoints());

		//Smooth 1 time(Mean filter);
		for (unsigned long int i = 0; i < cachepd->GetNumberOfCells(); i++) {
			vtkSmartPointer<vtkIdList> cellinegiborpointids = vtkSmartPointer<vtkIdList>::New();
			vtkSmartPointer<vtkIdList> cellipointinegiborpointids = vtkSmartPointer<vtkIdList>::New();

			for (int j = 0; j < 3; j++) {
				vtkSmartPointer<vtkIdList> templist = vtkSmartPointer<vtkIdList>::New();
				templist->InsertNextId(cachepd->GetCell(i)->GetPointId(j));
				cachepd->GetCellNeighbors(i, templist, cellipointinegiborpointids);
				for (int k = 0; k < cellipointinegiborpointids->GetNumberOfIds(); k++) {
					cellinegiborpointids->InsertUniqueId(cellipointinegiborpointids->GetId(k));
				}
			}
			if (cellinegiborpointids->GetNumberOfIds() == 0)
				continue;
			double sum2x = 0, sum2y = 0, sum2z = 0;
			for (int m = 0; m < cellinegiborpointids->GetNumberOfIds(); m++) {
				sum2x += cachepd->GetCellData()->GetNormals()->GetComponent(cellinegiborpointids->GetId(m), 0);
				sum2y += cachepd->GetCellData()->GetNormals()->GetComponent(cellinegiborpointids->GetId(m), 1);
				sum2z += cachepd->GetCellData()->GetNormals()->GetComponent(cellinegiborpointids->GetId(m), 2);
			}
			double norm2 = sqrt(sum2x*sum2x + sum2y*sum2y + sum2z*sum2z);
			if (norm2 == 0)
				continue;
			sum2x = sum2x / norm2;
			sum2y = sum2y / norm2;
			sum2z = sum2z / norm2;
			tempcellnormallist->SetTuple3(i, sum2x, sum2y, sum2z);
		}

		for (int i = 0; i < cachepd->GetNumberOfPoints(); i++) {
			vtkSmartPointer<vtkIdList> pointinegiborcellids = vtkSmartPointer<vtkIdList>::New();
			cachepd->GetPointCells(i, pointinegiborcellids);
			if (pointinegiborcellids->GetNumberOfIds() == 0)
				continue;

			double sum2x = 0, sum2y = 0, sum2z = 0;
			for (int j = 0; j < pointinegiborcellids->GetNumberOfIds(); j++) {
				sum2x += cachepd->GetCellData()->GetNormals()->GetComponent(pointinegiborcellids->GetId(j), 0);
				sum2y += cachepd->GetCellData()->GetNormals()->GetComponent(pointinegiborcellids->GetId(j), 1);
				sum2z += cachepd->GetCellData()->GetNormals()->GetComponent(pointinegiborcellids->GetId(j), 2);
			}
			double norm2 = sqrt(sum2x*sum2x + sum2y*sum2y + sum2z*sum2z);
			if (norm2 == 0)
				continue;
			sum2x = sum2x / norm2;
			sum2y = sum2y / norm2;
			sum2z = sum2z / norm2;
			temppointnormallist->SetTuple3(i, sum2x, sum2y, sum2z);
		}

		//Update smoothednormallist for cell and point;
		for (int i = 0; i < smoothedcellnormal->GetNumberOfTuples(); i++)
			smoothedcellnormal->SetTuple3(i, tempcellnormallist->GetComponent(i, 0), tempcellnormallist->GetComponent(i, 1), tempcellnormallist->GetComponent(i, 2));
		for (int i = 0; i < smoothedpointnormal->GetNumberOfTuples(); i++)
			smoothedpointnormal->SetTuple3(i, temppointnormallist->GetComponent(i, 0), temppointnormallist->GetComponent(i, 1), temppointnormallist->GetComponent(i, 2));

		//Performing extrusion test on each cell;
		for (int i = 0; i < cachepd->GetNumberOfCells(); i++) {
			double point1[3], point2[3], point3[3], normal1[3], normal2[3], normal3[3];
			for (int j = 0; j < 3; j++) {
				point1[j] = cachepd->GetPoint(cachepd->GetCell(i)->GetPointId(0))[j];
				point2[j] = cachepd->GetPoint(cachepd->GetCell(i)->GetPointId(1))[j];
				point3[j] = cachepd->GetPoint(cachepd->GetCell(i)->GetPointId(2))[j];
				normal1[j] = smoothedpointnormal->GetComponent(cachepd->GetCell(i)->GetPointId(0), j);
				normal2[j] = smoothedpointnormal->GetComponent(cachepd->GetCell(i)->GetPointId(1), j);
				normal3[j] = smoothedpointnormal->GetComponent(cachepd->GetCell(i)->GetPointId(2), j);
			}
			double v1[3], v2[3], n1[3], n2[3];
			for (int j = 0; j < 3; j++) {
				v1[j] = point2[j] - point1[j];
				v2[j] = point3[j] - point1[j];
				n1[j] = normal2[j] - normal1[j];
				n2[j] = normal3[j] - normal1[j];
			}
			double offsetted1[3], offsetted2[3];
			for (int j = 0; j < 3; j++) {
				offsetted1[j] = v1[j] + extrusionfactor*n1[j];
				offsetted2[j] = v2[j] + extrusionfactor*n2[j];
			}
			double cross1[3], cross2[3];
			vtkMath::Cross(v1, v2, cross1);
			vtkMath::Cross(offsetted1, offsetted2, cross2);
			double value = vtkMath::Dot(cross1, cross2);
			double norm1 = vtkMath::Norm(cross1);
			double norm2 = vtkMath::Norm(cross2);
			double thershold = cos_of_tolerance*norm1*norm2;

			//Check if the given extrusionfactor will cause trouble to cell i;
			if (value >= thershold) {
				if (i + 1 == cachepd->GetNumberOfCells())
					extrusiongreenlight = 1;
				continue;
			}

			else //Cell i must have problem then.
			{
				if (silentmode == 0)
					std::cout << " problem at cell " << i << "\n";

				if (i > marker && progressreport) {
					std::cout << progressreporter * 10 << " % completed.\n";
					progressreporter++;
				}

				//Calculate centroid;
				double centroid[3] = { 0,0,0 };
				for (int j = 0; j < 3; j++) {
					centroid[j] = point1[j] / 3 + point2[j] / 3 + point3[j] / 3;
				}

				//Detect if the centroid point has been created or not;
				for (int j = 0; j < cachepd->GetNumberOfPoints(); j++) {
					double temppoint[3];
					cachepd->GetPoint(j, temppoint);
					if (temppoint[0] == centroid[0] && temppoint[1] == centroid[1] && temppoint[2] == centroid[2]) {
						std::cout << "Non-manifold edge will be produced.....\n";
						system("pause");
					}
				}



				//Detect if other cell (share an edge) or (share a point only) with cell i;
				vtkSmartPointer<vtkIntArray> numsharedpointwithcellilist = vtkSmartPointer<vtkIntArray>::New();
				numsharedpointwithcellilist->SetNumberOfComponents(1);
				numsharedpointwithcellilist->SetNumberOfTuples(cachepd->GetNumberOfCells());

				//initialize;
				for (int j = 0; j < numsharedpointwithcellilist->GetNumberOfTuples(); j++)
					numsharedpointwithcellilist->SetTuple1(j, 0);

				int id1 = cachepd->GetCell(i)->GetPointId(0);
				int id2 = cachepd->GetCell(i)->GetPointId(1);
				int id3 = cachepd->GetCell(i)->GetPointId(2);

				//Count number of share point of each cell j and the problematic cell i
				for (int j = 0; j < numsharedpointwithcellilist->GetNumberOfTuples(); j++) {
					for (int k = 0; k < 3; k++) {
						if (cachepd->GetCell(j)->GetPointId(k) == id1 || cachepd->GetCell(j)->GetPointId(k) == id2 || cachepd->GetCell(j)->GetPointId(k) == id3) {
							int originalvalue = numsharedpointwithcellilist->GetComponent(j, 0);
							numsharedpointwithcellilist->SetTuple1(j, originalvalue + 1);
						}
					}
				}


				//For agressivemode, we make the centroid(Newly constructed point) to be the centerpoint of all related point;
				if (aggressivemode) {
					int counter = 0;
					double sumx = 0, sumy = 0, sumz = 0;
					for (int j = 0; j < numsharedpointwithcellilist->GetNumberOfTuples(); j++) {
						if (numsharedpointwithcellilist->GetComponent(j, 0) != 1)
							continue;
						else {
							for (int k = 0; k < 3; k++) {
								int tempidk = cachepd->GetCell(j)->GetPointId(k);
								if (tempidk == id1 || tempidk == id2 || tempidk == id3)
									continue;
								else {
									double point[3];
									cachepd->GetPoint(tempidk, point);
									sumx += point[0];
									sumy += point[1];
									sumz += point[2];
									counter++;
								}
							}
						}
					}
					if (counter != 0) {
						centroid[0] = sumx / counter;
						centroid[1] = sumy / counter;
						centroid[2] = sumz / counter;
					}
					else {
						if (silentmode == 0)
							std::cout << "No neighboring cell for cell " << i << " ?\n";
					}

				}


				//Sort id1, id2, id3 so that id1<id2<id3;
				if (id1 > id2) {
					int tmp = id2;
					id2 = id1;
					id1 = tmp;
				}
				if (id2 > id3) {
					int tmp = id3;
					id3 = id2;
					id2 = tmp;
				}
				if (id1 > id2) {
					int tmp = id2;
					id2 = id1;
					id1 = tmp;
				}


				//To collapse(delete) cell i , we collapse the entire triangle into a point (say x0). So cell i and any triangle with a common edge will be deleted.
				//A new point (x0) will be sotred as the last point of the mesh. All triangle shared a single point with cell i will be directed to this new point x0.

				//Update temp point list
				vtkSmartPointer<vtkDoubleArray>temppointlist = vtkSmartPointer<vtkDoubleArray>::New();
				temppointlist->SetNumberOfComponents(3);
				for (int j = 0; j < cachepd->GetNumberOfPoints(); j++) {
					if (j != id1 && j != id2 && j != id3)
						temppointlist->InsertNextTuple3(cachepd->GetPoint(j)[0], cachepd->GetPoint(j)[1], cachepd->GetPoint(j)[2]);
				}
				temppointlist->InsertNextTuple3(centroid[0], centroid[1], centroid[2]);

				//Update temp cell list
				vtkSmartPointer<vtkIntArray>tempcellidlist = vtkSmartPointer<vtkIntArray>::New();
				tempcellidlist->SetNumberOfComponents(3);
				for (int j = 0; j < cachepd->GetNumberOfCells(); j++) {
					if (numsharedpointwithcellilist->GetComponent(j, 0) == 0) {
						//Note that pointid of each cell will be changed if we delete a cell and its three point.
						int firstid = cachepd->GetCell(j)->GetPointId(0), secondid = cachepd->GetCell(j)->GetPointId(1), thirdid = cachepd->GetCell(j)->GetPointId(2);
						if (firstid > id3)
							firstid--;
						if (firstid > id2)
							firstid--;
						if (firstid > id1)
							firstid--;
						if (secondid > id3)
							secondid--;
						if (secondid > id2)
							secondid--;
						if (secondid > id1)
							secondid--;
						if (thirdid > id3)
							thirdid--;
						if (thirdid > id2)
							thirdid--;
						if (thirdid > id1)
							thirdid--;
						tempcellidlist->InsertNextTuple3(firstid, secondid, thirdid);
					}

				}

				int centroidid = temppointlist->GetNumberOfTuples() - 1; //New point is the last point.

																		 //Redirect all affected triangle to the new point.
				for (int j = 0; j < numsharedpointwithcellilist->GetNumberOfTuples(); j++) {
					if (numsharedpointwithcellilist->GetComponent(j, 0) == 1) {
						int testsum = 0;
						if (cachepd->GetCell(j)->GetPointId(0) == id1 || cachepd->GetCell(j)->GetPointId(0) == id2 || cachepd->GetCell(j)->GetPointId(0) == id3) {
							int secondid = cachepd->GetCell(j)->GetPointId(1), thirdid = cachepd->GetCell(j)->GetPointId(2);
							if (secondid > id3)
								secondid--;
							if (secondid > id2)
								secondid--;
							if (secondid > id1)
								secondid--;
							if (thirdid > id3)
								thirdid--;
							if (thirdid > id2)
								thirdid--;
							if (thirdid > id1)
								thirdid--;
							tempcellidlist->InsertNextTuple3(centroidid, secondid, thirdid);
						}

						if (cachepd->GetCell(j)->GetPointId(1) == id1 || cachepd->GetCell(j)->GetPointId(1) == id2 || cachepd->GetCell(j)->GetPointId(1) == id3) {
							int secondid = cachepd->GetCell(j)->GetPointId(0), thirdid = cachepd->GetCell(j)->GetPointId(2);
							if (secondid > id3)
								secondid--;
							if (secondid > id2)
								secondid--;
							if (secondid > id1)
								secondid--;
							if (thirdid > id3)
								thirdid--;
							if (thirdid > id2)
								thirdid--;
							if (thirdid > id1)
								thirdid--;
							tempcellidlist->InsertNextTuple3(secondid, centroidid, thirdid);
						}

						if (cachepd->GetCell(j)->GetPointId(2) == id1 || cachepd->GetCell(j)->GetPointId(2) == id2 || cachepd->GetCell(j)->GetPointId(2) == id3) {
							int secondid = cachepd->GetCell(j)->GetPointId(0), thirdid = cachepd->GetCell(j)->GetPointId(1);
							if (secondid > id3)
								secondid--;
							if (secondid > id2)
								secondid--;
							if (secondid > id1)
								secondid--;
							if (thirdid > id3)
								thirdid--;
							if (thirdid > id2)
								thirdid--;
							if (thirdid > id1)
								thirdid--;
							tempcellidlist->InsertNextTuple3(secondid, thirdid, centroidid);
						}

					}

				}

				//Update pointlist;
				pointlist->Reset();
				pointlist->Initialize();
				for (int j = 0; j < temppointlist->GetNumberOfTuples(); j++) {
					double pointcache[3];
					pointcache[0] = temppointlist->GetComponent(j, 0);
					pointcache[1] = temppointlist->GetComponent(j, 1);
					pointcache[2] = temppointlist->GetComponent(j, 2);
					pointlist->InsertNextPoint(pointcache[0], pointcache[1], pointcache[2]);
				}

				//Update celllist;
				celllist->Reset();
				celllist->Initialize();
				for (int j = 0; j < tempcellidlist->GetNumberOfTuples(); j++) {
					celllist->InsertNextCell(3);
					for (int k = 0; k < 3; k++) {
						int pointcache = tempcellidlist->GetComponent(j, k);
						celllist->InsertCellPoint(pointcache);
					}
				}

				//Store new cell normal in cache;
				vtkSmartPointer<vtkDoubleArray> tempcellnormal = vtkSmartPointer<vtkDoubleArray>::New();
				tempcellnormal->SetNumberOfComponents(3);

				for (int j = 0; j < numsharedpointwithcellilist->GetNumberOfTuples(); j++) {
					if (numsharedpointwithcellilist->GetComponent(j, 0) == 0)//Copy unaltered cell normal first;
						tempcellnormal->InsertNextTuple3(smoothedcellnormal->GetComponent(j, 0), smoothedcellnormal->GetComponent(j, 1), smoothedcellnormal->GetComponent(j, 2));
				}

				for (int j = 0; j < numsharedpointwithcellilist->GetNumberOfTuples(); j++) {
					if (numsharedpointwithcellilist->GetComponent(j, 0) == 1) {
						tempcellnormal->InsertNextTuple3(smoothedcellnormal->GetComponent(j, 0), smoothedcellnormal->GetComponent(j, 1), smoothedcellnormal->GetComponent(j, 2));
					}
				}

				//Store new point normal in cache;
				vtkSmartPointer<vtkDoubleArray> temppointnormal = vtkSmartPointer<vtkDoubleArray>::New();
				temppointnormal->SetNumberOfComponents(3);

				for (int j = 0; j < pointlist->GetNumberOfPoints() - 1; j++) { //Copy unaltered point normal first;
					if (j < id1) {
						temppointnormal->InsertNextTuple3(smoothedpointnormal->GetComponent(j, 0), smoothedpointnormal->GetComponent(j, 1), smoothedpointnormal->GetComponent(j, 2));
						continue;
					}
					if (j >= id1 && j < id2) {
						temppointnormal->InsertNextTuple3(smoothedpointnormal->GetComponent(j + 1, 0), smoothedpointnormal->GetComponent(j + 1, 1), smoothedpointnormal->GetComponent(j + 1, 2));
						continue;
					}
					if (j >= id2 && j < id3) {
						temppointnormal->InsertNextTuple3(smoothedpointnormal->GetComponent(j + 2, 0), smoothedpointnormal->GetComponent(j + 2, 1), smoothedpointnormal->GetComponent(j + 2, 2));
						continue;
					}
					if (j >= id3) {
						temppointnormal->InsertNextTuple3(smoothedpointnormal->GetComponent(j + 3, 0), smoothedpointnormal->GetComponent(j + 3, 1), smoothedpointnormal->GetComponent(j + 3, 2));
						continue;
					}
				}
				//only the final added centroid left, its normal is defined to be original cell normal.
				temppointnormal->InsertNextTuple3(smoothedcellnormal->GetComponent(i, 0), smoothedcellnormal->GetComponent(i, 1), smoothedcellnormal->GetComponent(i, 2));


				//Update cell normal
				smoothedcellnormal->Reset();
				smoothedcellnormal->Initialize();
				smoothedcellnormal->SetNumberOfComponents(3);
				for (int j = 0; j < tempcellnormal->GetNumberOfTuples(); j++)
					smoothedcellnormal->InsertNextTuple3(tempcellnormal->GetComponent(j, 0), tempcellnormal->GetComponent(j, 1), tempcellnormal->GetComponent(j, 2));

				//Update point normal
				smoothedpointnormal->Reset();
				smoothedpointnormal->Initialize();
				smoothedpointnormal->SetNumberOfComponents(3);
				for (int j = 0; j < temppointnormal->GetNumberOfTuples(); j++)
					smoothedpointnormal->InsertNextTuple3(temppointnormal->GetComponent(j, 0), temppointnormal->GetComponent(j, 1), temppointnormal->GetComponent(j, 2));


				//Check for area;
				if (meshqualitycontrol) {
					// A triangle with "large" area wil be broken to 4 smaller triangle, formed center point of the three side of original triangle.
					bool areacheck = 1;
					while (areacheck) {
						int areatrouble = 0; int troublecellid = 0;

						for (int j = 0; j < celllist->GetNumberOfCells(); j++) {
							double t1[3], t2[3], t3[3], temptotalnumcell = celllist->GetNumberOfCells();
							int checkptid1 = celllist->GetData()->GetComponent(4 * j + 1, 0);
							int checkptid2 = celllist->GetData()->GetComponent(4 * j + 2, 0);
							int checkptid3 = celllist->GetData()->GetComponent(4 * j + 3, 0);
							pointlist->GetPoint(checkptid1, t1);
							pointlist->GetPoint(checkptid2, t2);
							pointlist->GetPoint(checkptid3, t3);
							double temparea = vtkTriangle::TriangleArea(t1, t2, t3);
							if (temparea > maxarea) {
								areatrouble = 1;
								troublecellid = j;
							}
						}

						if (areatrouble == 0) {
							areacheck = 0;
							break;
						}

						//Create new pd by storing in cache first;
						numsharedpointwithcellilist->Reset();
						numsharedpointwithcellilist->Initialize();
						numsharedpointwithcellilist->SetNumberOfComponents(1);
						for (int j = 0; j < celllist->GetNumberOfCells(); j++)
							numsharedpointwithcellilist->InsertNextTuple1(0);
						for (int k = 0; k < 3; k++) {
							int targetid = celllist->GetData()->GetComponent(4 * troublecellid + 1 + k, 0);
							for (int j = 0; j < numsharedpointwithcellilist->GetNumberOfTuples(); j++) {
								for (int m = 0; m < 3; m++) {
									if (celllist->GetData()->GetComponent(4 * j + m + 1, 0) == targetid) {
										int cachevalue = numsharedpointwithcellilist->GetComponent(j, 0);
										numsharedpointwithcellilist->SetTuple1(j, cachevalue + 1);
									}
								}
							}
						}

						temppointlist->Reset();
						temppointlist->Initialize();
						temppointlist->SetNumberOfComponents(3);
						tempcellidlist->Reset();
						tempcellidlist->Initialize();
						tempcellidlist->SetNumberOfComponents(3);
						tempcellnormal->Reset();
						tempcellnormal->Initialize();
						tempcellnormal->SetNumberOfComponents(3);
						temppointnormal->Reset();
						temppointnormal->Initialize();
						temppointnormal->SetNumberOfComponents(3);
						double t1[3], t2[3], t3[3];
						pointlist->GetPoint(celllist->GetData()->GetComponent(4 * troublecellid + 1, 0), t1);
						pointlist->GetPoint(celllist->GetData()->GetComponent(4 * troublecellid + 2, 0), t2);
						pointlist->GetPoint(celllist->GetData()->GetComponent(4 * troublecellid + 3, 0), t3);
						double tempcenter1[3], tempcenter2[3], tempcenter3[3];
						for (int m = 0; m < 3; m++) {
							tempcenter1[m] = t1[m] / 2 + t2[m] / 2;
							tempcenter2[m] = t2[m] / 2 + t3[m] / 2;
							tempcenter3[m] = t3[m] / 2 + t1[m] / 2;

						}
						for (int j = 0; j < pointlist->GetNumberOfPoints(); j++)
							temppointlist->InsertNextTuple3(pointlist->GetData()->GetComponent(j, 0), pointlist->GetData()->GetComponent(j, 1), pointlist->GetData()->GetComponent(j, 2));
						temppointlist->InsertNextTuple3(tempcenter1[0], tempcenter1[1], tempcenter1[2]);
						temppointlist->InsertNextTuple3(tempcenter2[0], tempcenter2[1], tempcenter2[2]);
						temppointlist->InsertNextTuple3(tempcenter3[0], tempcenter3[1], tempcenter3[2]);


						for (int j = 0; j < celllist->GetNumberOfCells(); j++) {
							if (numsharedpointwithcellilist->GetComponent(j, 0) <= 1) {
								int cache[3];
								for (int k = 0; k < 3; k++)
									cache[k] = celllist->GetData()->GetComponent(4 * j + k + 1, 0);
								tempcellidlist->InsertNextTuple3(cache[0], cache[1], cache[2]);
							}
						}
						int troublecellpt1id = celllist->GetData()->GetComponent(4 * troublecellid + 1, 0), troublecellpt2id = celllist->GetData()->GetComponent(4 * troublecellid + 2, 0), troublecellpt3id = celllist->GetData()->GetComponent(4 * troublecellid + 3, 0);
						int oldnumofpt = pointlist->GetNumberOfPoints();
						int count12 = 0, count23 = 0, count31 = 0, cell12id, cell23id, cell31id;
						for (int j = 0; j < celllist->GetNumberOfCells(); j++) {
							if (numsharedpointwithcellilist->GetComponent(j, 0) == 2) {
								int leftoverid;
								for (int k = 0; k < 3; k++) {
									int cacheidkofj = celllist->GetData()->GetComponent(4 * j + k + 1, 0);
									if ((cacheidkofj - troublecellpt1id)*(cacheidkofj - troublecellpt2id)*(cacheidkofj - troublecellpt3id) != 0) {
										leftoverid = cacheidkofj;
									}
								}
								int neighbortype; //>0 =>1-2, =0 =>2-3, <0 => 3-1 type;
								if (troublecellpt1id == celllist->GetData()->GetComponent(4 * j + 1, 0) || troublecellpt1id == celllist->GetData()->GetComponent(4 * j + 2, 0) || troublecellpt1id == celllist->GetData()->GetComponent(4 * j + 3, 0)) {
									if (troublecellpt2id == celllist->GetData()->GetComponent(4 * j + 1, 0) || troublecellpt2id == celllist->GetData()->GetComponent(4 * j + 2, 0) || troublecellpt2id == celllist->GetData()->GetComponent(4 * j + 3, 0)) {
										neighbortype = 1;
									}
									else neighbortype = -1;
								}
								else neighbortype = 0;

								if (neighbortype > 0) {
									tempcellidlist->InsertNextTuple3(troublecellpt1id, leftoverid, oldnumofpt);
									tempcellidlist->InsertNextTuple3(troublecellpt2id, oldnumofpt, leftoverid);
									count12 = count12 + 2;
									cell12id = j;
								}
								else if (neighbortype == 0) {
									tempcellidlist->InsertNextTuple3(troublecellpt2id, leftoverid, oldnumofpt + 1);
									tempcellidlist->InsertNextTuple3(troublecellpt3id, oldnumofpt + 1, leftoverid);
									count23 = count23 + 2;
									cell23id = j;
								}
								else {
									tempcellidlist->InsertNextTuple3(troublecellpt3id, leftoverid, oldnumofpt + 2);
									tempcellidlist->InsertNextTuple3(troublecellpt1id, oldnumofpt + 2, leftoverid);
									count31 = count31 + 2;
									cell31id = j;
								}
							}
						}
						if (count12 > 2 || count23 > 2 || count31 > 2) {
							if (silentmode == 0) {
								std::cout << "Problem arises in cell reconstruction due to the existence of non-manifold edge!!\n";
							}
						}

						tempcellidlist->InsertNextTuple3(troublecellpt1id, oldnumofpt, oldnumofpt + 2);
						tempcellidlist->InsertNextTuple3(troublecellpt2id, oldnumofpt + 1, oldnumofpt);
						tempcellidlist->InsertNextTuple3(troublecellpt3id, oldnumofpt + 2, oldnumofpt + 1);
						tempcellidlist->InsertNextTuple3(oldnumofpt, oldnumofpt + 1, oldnumofpt + 2);


						for (int j = 0; j < smoothedcellnormal->GetNumberOfTuples(); j++) {
							if (numsharedpointwithcellilist->GetComponent(j, 0) <= 1)
								tempcellnormal->InsertNextTuple3(smoothedcellnormal->GetComponent(j, 0), smoothedcellnormal->GetComponent(j, 1), smoothedcellnormal->GetComponent(j, 2));
						}
						for (int j = 0; j < count12; j++)
							tempcellnormal->InsertNextTuple3(smoothedcellnormal->GetComponent(cell12id, 0), smoothedcellnormal->GetComponent(cell12id, 1), smoothedcellnormal->GetComponent(cell12id, 2));
						for (int j = 0; j < count23; j++)
							tempcellnormal->InsertNextTuple3(smoothedcellnormal->GetComponent(cell23id, 0), smoothedcellnormal->GetComponent(cell23id, 1), smoothedcellnormal->GetComponent(cell23id, 2));
						for (int j = 0; j < count31; j++)
							tempcellnormal->InsertNextTuple3(smoothedcellnormal->GetComponent(cell31id, 0), smoothedcellnormal->GetComponent(cell31id, 1), smoothedcellnormal->GetComponent(cell31id, 2));
						for (int j = 0; j < 4; j++)
							tempcellnormal->InsertNextTuple3(smoothedcellnormal->GetComponent(troublecellid, 0), smoothedcellnormal->GetComponent(troublecellid, 1), smoothedcellnormal->GetComponent(troublecellid, 2));

						for (int j = 0; j < smoothedpointnormal->GetNumberOfTuples(); j++)
							temppointnormal->InsertNextTuple3(smoothedpointnormal->GetComponent(j, 0), smoothedpointnormal->GetComponent(j, 1), smoothedpointnormal->GetComponent(j, 2));
						double normal1[3], normal2[3], normal3[3];;
						for (int j = 0; j < 3; j++) {
							normal1[j] = smoothedpointnormal->GetComponent(troublecellpt1id, j) / 2 + smoothedpointnormal->GetComponent(troublecellpt2id, j) / 2;
							normal2[j] = smoothedpointnormal->GetComponent(troublecellpt3id, j) / 2 + smoothedpointnormal->GetComponent(troublecellpt2id, j) / 2;
							normal3[j] = smoothedpointnormal->GetComponent(troublecellpt1id, j) / 2 + smoothedpointnormal->GetComponent(troublecellpt3id, j) / 2;
						}
						double tempnorm;
						tempnorm = sqrt(normal1[0] * normal1[0] + normal1[1] * normal1[1] + normal1[2] * normal1[2]);
						if (tempnorm == 0 || silentmode == 0)
							std::cerr << "ERROR for normal";
						for (int j = 0; j < 3; j++)
							normal1[j] = normal1[j] / tempnorm;
						tempnorm = sqrt(normal2[0] * normal2[0] + normal2[1] * normal2[1] + normal2[2] * normal2[2]);
						if (tempnorm == 0 || silentmode == 0)
							std::cerr << "ERROR for normal";
						for (int j = 0; j < 3; j++)
							normal2[j] = normal2[j] / tempnorm;
						tempnorm = sqrt(normal3[0] * normal3[0] + normal3[1] * normal3[1] + normal3[2] * normal3[2]);
						if (tempnorm == 0 || silentmode == 0)
							std::cerr << "ERROR for normal";
						for (int j = 0; j < 3; j++)
							normal3[j] = normal3[j] / tempnorm;
						temppointnormal->InsertNextTuple3(normal1[0], normal1[1], normal1[2]);
						temppointnormal->InsertNextTuple3(normal2[0], normal2[1], normal2[2]);
						temppointnormal->InsertNextTuple3(normal3[0], normal3[1], normal3[2]);


						//Update the data of cachepd;
						pointlist->Reset();
						pointlist->Initialize();
						for (int j = 0; j < temppointlist->GetNumberOfTuples(); j++)
							pointlist->InsertNextPoint(temppointlist->GetComponent(j, 0), temppointlist->GetComponent(j, 1), temppointlist->GetComponent(j, 2));
						celllist->Reset();
						celllist->Initialize();
						for (int j = 0; j < tempcellidlist->GetNumberOfTuples(); j++) {
							celllist->InsertNextCell(3);
							for (int k = 0; k < 3; k++)
								celllist->InsertCellPoint(tempcellidlist->GetComponent(j, k));
						}
						smoothedcellnormal->Reset();
						smoothedcellnormal->Initialize();
						for (int j = 0; j < tempcellnormal->GetNumberOfTuples(); j++)
							smoothedcellnormal->InsertNextTuple3(tempcellnormal->GetComponent(j, 0), tempcellnormal->GetComponent(j, 1), tempcellnormal->GetComponent(j, 2));
						smoothedpointnormal->Reset();
						smoothedpointnormal->Initialize();
						for (int j = 0; j < temppointnormal->GetNumberOfTuples(); j++)
							smoothedpointnormal->InsertNextTuple3(temppointnormal->GetComponent(j, 0), temppointnormal->GetComponent(j, 1), temppointnormal->GetComponent(j, 2));

						//To check if any degenerated triangle in the mesh.
						for (int j = 0; j < celllist->GetNumberOfCells(); j++) {
							int pid1 = celllist->GetData()->GetComponent(4 * j + 1, 0);
							int pid2 = celllist->GetData()->GetComponent(4 * j + 2, 0);
							int pid3 = celllist->GetData()->GetComponent(4 * j + 3, 0);
							if ((pid1 - pid2)*(pid2 - pid3)*(pid3 - pid1) == 0 || silentmode == 0) {
								std::cout << "Degenerated triangle detected. ";
								tempcellidlist->Reset();
								tempcellidlist->Initialize();
								tempcellidlist->SetNumberOfComponents(3);
								tempcellnormal->Reset();
								tempcellnormal->Initialize();
								tempcellnormal->SetNumberOfComponents(3);
								for (int k = 0; k < celllist->GetNumberOfCells(); k++) {
									if (k != j)
										tempcellidlist->InsertNextTuple3(celllist->GetData()->GetComponent(4 * k + 1, 0), celllist->GetData()->GetComponent(4 * k + 2, 0), celllist->GetData()->GetComponent(4 * k + 3, 0));
								}
								for (int k = 0; k < smoothedcellnormal->GetNumberOfTuples(); k++) {
									if (k != j)
										tempcellnormal->InsertNextTuple3(smoothedcellnormal->GetComponent(k, 0), smoothedcellnormal->GetComponent(k, 1), smoothedcellnormal->GetComponent(k, 2));
								}
								celllist->Reset();
								celllist->Initialize();
								for (int j = 0; j < tempcellidlist->GetNumberOfTuples(); j++) {
									celllist->InsertNextCell(3);
									for (int k = 0; k < 3; k++)
										celllist->InsertCellPoint(tempcellidlist->GetComponent(j, k));
								}
								smoothedcellnormal->Reset();
								smoothedcellnormal->Initialize();
								for (int j = 0; j < tempcellnormal->GetNumberOfTuples(); j++)
									smoothedcellnormal->InsertNextTuple3(tempcellnormal->GetComponent(j, 0), tempcellnormal->GetComponent(j, 1), tempcellnormal->GetComponent(j, 2));
								if (silentmode == 0)
									std::cout << "  Tango down!\n";
							}
						}
						if (silentmode == 0)
							std::cout << ".";
					}//while areacheck end;
				}//meshqualitycontrol mode end;

				if (silentmode == 0)
					std::cout << "\n";
				break;  //To go through another complete check of the mesh.
			}//Else end

		} //for each cell i

	}//While end;


	 //Normal offset start;
	for (int i = 0; i < pointlist->GetNumberOfPoints(); i++) {
		double point[3];
		pointlist->GetPoint(i, point);
		pointlist->GetData()->SetTuple3(i, point[0] + extrusionfactor*smoothedpointnormal->GetComponent(i, 0), point[1] + extrusionfactor*smoothedpointnormal->GetComponent(i, 1), point[2] + extrusionfactor*smoothedpointnormal->GetComponent(i, 2));
	}

	this->Outputpd = vtkSmartPointer<vtkPolyData>::New();
	this->Outputpd->SetPoints(pointlist);
	this->Outputpd->SetStrips(celllist);
	this->Outputpd->GetCellData()->SetNormals(smoothedcellnormal);
	this->Outputpd->GetPointData()->SetNormals(smoothedpointnormal);


}


