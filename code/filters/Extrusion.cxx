#include "Extrusion.h"
#include <string>
#include <vtkSmartPointer.h>
#include <vtkSTLReader.h>
#include <vtkOBJReader.h>
#include <vtkPolyDataNormals.h>			
#include <vtkPolyData.h>	

#include <vtkXMLPolyDataReader.h>					
#include <vtkPoints.h>
#include <vtkPointData.h>
#include <vtkCellArray.h>
#include <vtkCellData.h>
#include <vtkDoubleArray.h>
#include <vtkIntArray.h>

#include <vtkMath.h>
#include <vtkTriangle.h>

#include <vtkLinearExtrusionFilter.h>

#include <vtkSTLWriter.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>				
#include <vtkOBJExporter.h>

void Extrusion::SetInputData(vtkPolyData* Input) {
	this->Inputpd = Input;
}
vtkPolyData* Extrusion::GetOutput() {
	return this->Outputpd;
}
void Extrusion::SetInputCenterLine(vtkPolyData* Input) {
	this->Linepd = Input;
}
void Extrusion::SetExtrusionFactor(double a) {
	this->Extrusionfactor = a;
}
void Extrusion::SetNaiveMode(bool a) {
	this->Naivemode = a;
}
void Extrusion::SetNumberOfSmooth(int a) {
	if (a < 0)
		a = 0;
	this->Numberofsmoothing = a;
}
void Extrusion::SetFixMode(bool a) {
	this->Fixmode = a;
}
float signum(double a) {
	if (a > 0)
		return 1;
	else if (a < 0)
		return -1;
	else return 0;
}
void Extrusion::Update() {
	double extrusionfactor = this->Extrusionfactor;
	bool naivemode = this->Naivemode;
	int numberofsmoothing = this->Numberofsmoothing;
	bool fixmode = this->Fixmode;

									//Other parameters that can be changed : regionsizemodifier , padding 

	vtkSmartPointer<vtkPolyDataNormals> normalreader = vtkSmartPointer<vtkPolyDataNormals>::New();
	normalreader->SetInputData(this->Inputpd);
	normalreader->ComputeCellNormalsOn();
	normalreader->ComputePointNormalsOn();
	normalreader->AutoOrientNormalsOn();
	normalreader->Update();

	vtkSmartPointer<vtkPolyData> cachepd = vtkSmartPointer<vtkPolyData>::New();

	if (naivemode == 0) {

		vtkSmartPointer<vtkPolyData>linepd = this->Linepd;
		int numpt = linepd->GetNumberOfPoints();

		double **lineptlist;
		lineptlist = new double*[numpt];
		for (int i = 0; i < numpt; i++) {
			lineptlist[i] = new double[3];
			double *pointi = linepd->GetPoint(i);
			for (int j = 0; j < 3; j++) {
				lineptlist[i][j] = pointi[j];
			}
		}
		//Linear Regression Start;
		//Calculate mean of x, y and z;
		int sumx = 0, sumy = 0, sumz = 0;
		float meanx, meany, meanz;
		for (int i = 0; i < numpt; i++) {
			sumx += lineptlist[i][0];
			sumy += lineptlist[i][1];
			sumz += lineptlist[i][2];
		}
		meanx = sumx / (numpt*1.0);
		meany = sumy / (numpt*1.0);
		meanz = sumz / (numpt*1.0);
		//Calculate mean of y and z end;

		//Best fit line is x=x, y=a1*x+b1, z=a2*x+b2;

		float tempnumerator = 0, tempdenominator = 0;
		for (int i = 0; i < numpt; i++) {
			tempnumerator += (lineptlist[i][0] - meanx)*(lineptlist[i][1] - meany);
			tempdenominator += (lineptlist[i][0] - meanx)*(lineptlist[i][0] - meanx);
		}
		const float a1 = tempnumerator / tempdenominator;
		const float b1 = meany - a1*meanx;
		tempnumerator = 0; tempdenominator = 0;
		for (int i = 0; i < numpt; i++) {
			tempnumerator += (lineptlist[i][0] - meanx)*(lineptlist[i][2] - meanz);
			tempdenominator += (lineptlist[i][0] - meanx)*(lineptlist[i][0] - meanx);
		}
		const float a2 = tempnumerator / tempdenominator;
		const float b2 = meanz - a2*meanx;
		//So Best fit line is (0,b1,b2)+t*(1,a1,a2);

		//Find corresponding t of starting pt and end pt of drillcenterline, t1,t2 respectively;
		double tmp1, tmp2, padding = 0.1;
		tmp1 = ((linepd->GetPoint(0)[0]) + a1*(linepd->GetPoint(0)[1] - b1) + a2*(linepd->GetPoint(0)[2] - b2)) / (1 + a1*a1 + a2*a2);
		tmp2 = ((linepd->GetPoint(linepd->GetNumberOfPoints() - 1)[0]) + a1*(linepd->GetPoint(linepd->GetNumberOfPoints() - 1)[1] - b1) + a2*(linepd->GetPoint(linepd->GetNumberOfPoints() - 1)[2] - b2)) / (1 + a1*a1 + a2*a2);
		if (tmp1 > tmp2) {  //want t1<t2;
			double temp = tmp1;
			tmp1 = tmp2;
			tmp2 = temp;
		}
		const float t1 = tmp1 - (tmp2 - tmp1)*padding;  //Add padding;
		const float t2 = tmp2 + (tmp2 - tmp1)*padding;
		//Find corresponding t of starting pt and end pt of drillcenterline, t1,t2 respectively end;

		vtkSmartPointer<vtkPolyData> originalpd = normalreader->GetOutput();

		//Update pointlist, celllist;

		vtkSmartPointer<vtkPoints> pointlist = vtkSmartPointer<vtkPoints>::New();
		vtkSmartPointer<vtkCellArray> celllist = vtkSmartPointer<vtkCellArray>::New();
		for (int i = 0; i < originalpd->GetNumberOfPoints(); i++) {
			double point[3];
			originalpd->GetPoint(i, point);
			pointlist->InsertNextPoint(point[0], point[1], point[2]);
		}
		for (int i = 0; i < originalpd->GetNumberOfCells(); i++) {
			celllist->InsertNextCell(3);
			for (int j = 0; j < 3; j++) {
				celllist->InsertCellPoint(originalpd->GetCell(i)->GetPointId(j));
			}
		}
		//Update pointlist, celllist end;


		//Update (cell)normalarray, pointnormalarray, drillholeedgecellarray, drillholeedgepointarray;
		double regionsizemodifier = 0.5;
		vtkSmartPointer<vtkDoubleArray> normalarray = vtkSmartPointer<vtkDoubleArray>::New();
		vtkSmartPointer<vtkIntArray>drillholeedgecellarray = vtkSmartPointer<vtkIntArray>::New();
		normalarray->SetNumberOfComponents(3);
		normalarray->SetNumberOfTuples(originalpd->GetNumberOfCells());
		drillholeedgecellarray->SetNumberOfComponents(1);
		drillholeedgecellarray->SetNumberOfTuples(originalpd->GetNumberOfCells());

		for (int i = 0; i < originalpd->GetNumberOfCells(); i++) {
			vtkCell *celli = originalpd->GetCell(i);
			float sum1x = 0, sum1y = 0, sum1z = 0;
			float x0, y0, z0, t0;
			for (int j = 0; j < celli->GetNumberOfPoints(); j++) {
				double point[3];
				originalpd->GetPoint(celli->GetPointId(j), point);
				sum1x += point[0];
				sum1y += point[1];
				sum1z += point[2];
			}
			x0 = sum1x / celli->GetNumberOfPoints();
			y0 = sum1y / celli->GetNumberOfPoints();
			z0 = sum1z / celli->GetNumberOfPoints();
			t0 = (x0 + a1*(y0 - b1) + a2*(z0 - b2)) / (1 + a1*a1 + a2*a2);
			if (t0<t1 || t0>t2) {
				normalarray->SetTuple3(i, originalpd->GetCellData()->GetNormals()->GetComponent(i, 0), originalpd->GetCellData()->GetNormals()->GetComponent(i, 1), originalpd->GetCellData()->GetNormals()->GetComponent(i, 2));
				continue;
			}
			if ((t0 > t1 - regionsizemodifier*padding*(t2 - t1) && t0 < t1 + regionsizemodifier*padding*(t2 - t1)) || (t0 < t2 + regionsizemodifier*padding*(t2 - t1) && t0>t2 - regionsizemodifier*padding*(t2 - t1)))
				drillholeedgecellarray->SetTuple1(i, 1);
			else drillholeedgecellarray->SetTuple1(i, 0);

			float assignednormal[3];
			assignednormal[0] = x0 - t0;
			assignednormal[1] = y0 - a1*t0 - b1;
			assignednormal[2] = z0 - a2*t0 - b2;

			float norm = sqrt(assignednormal[0] * assignednormal[0] + assignednormal[1] * assignednormal[1] + assignednormal[2] * assignednormal[2]);
			assignednormal[0] = assignednormal[0] / norm;
			assignednormal[1] = assignednormal[1] / norm;
			assignednormal[2] = assignednormal[2] / norm;
			normalarray->SetTuple3(i, assignednormal[0], assignednormal[1], assignednormal[2]);
		}

		vtkSmartPointer<vtkIntArray> drillholeedgepointarray = vtkSmartPointer<vtkIntArray>::New();
		drillholeedgepointarray->SetNumberOfComponents(1);
		drillholeedgepointarray->SetNumberOfTuples(originalpd->GetNumberOfPoints());
		vtkSmartPointer<vtkDoubleArray>pointnormalarray = vtkSmartPointer<vtkDoubleArray>::New();
		pointnormalarray->SetNumberOfComponents(3);
		pointnormalarray->SetNumberOfTuples(originalpd->GetNumberOfPoints());
		for (int i = 0; i < originalpd->GetNumberOfPoints(); i++) {
			double point[3];
			originalpd->GetPoint(i, point);
			float x0 = point[0];
			float y0 = point[1];
			float z0 = point[2];
			float t0 = (x0 + a1*(y0 - b1) + a2*(z0 - b2)) / (1 + a1*a1 + a2*a2);
			if ((t0 > t1 - regionsizemodifier*padding*(t2 - t1) && t0<t1 + regionsizemodifier*padding*(t2 - t1)) || (t0 < t2 + regionsizemodifier*padding*(t2 - t1) && t0>t2 - regionsizemodifier*padding*(t2 - t1)))
				drillholeedgepointarray->SetTuple1(i, 1);
			else drillholeedgepointarray->SetTuple1(i, 0);

			if (t0<t1 || t0>t2) {
				pointnormalarray->SetTuple3(i, originalpd->GetPointData()->GetNormals()->GetComponent(i, 0), originalpd->GetPointData()->GetNormals()->GetComponent(i, 1), originalpd->GetPointData()->GetNormals()->GetComponent(i, 2));
				continue;
			}

			float assignednormal[3];
			assignednormal[0] = x0 - t0;
			assignednormal[1] = y0 - a1*t0 - b1;
			assignednormal[2] = z0 - a2*t0 - b2;

			float norm = sqrt(assignednormal[0] * assignednormal[0] + assignednormal[1] * assignednormal[1] + assignednormal[2] * assignednormal[2]);
			assignednormal[0] = assignednormal[0] / norm;
			assignednormal[1] = assignednormal[1] / norm;
			assignednormal[2] = assignednormal[2] / norm;
			pointnormalarray->SetTuple3(i, assignednormal[0], assignednormal[1], assignednormal[2]);
		}
		//Update (cell)normalarray, pointnormalarray, drillholeedgecellarray, drillholeedgepointarray end;


		//Smooth out normal (no warning when there is degenerated triangle);
		originalpd->BuildLinks();
		for (int n = 0; n<numberofsmoothing; n++) {
			vtkSmartPointer<vtkIdList> cellipointinegiborpointids = vtkSmartPointer<vtkIdList>::New();
			vtkSmartPointer<vtkIdList> pointinegiborcellids = vtkSmartPointer<vtkIdList>::New();
			vtkSmartPointer<vtkIdList> templist = vtkSmartPointer<vtkIdList>::New();
			vtkSmartPointer<vtkIdList> cellinegiborpointids = vtkSmartPointer<vtkIdList>::New();
			vtkSmartPointer<vtkDoubleArray>tempcellnormalllist = vtkSmartPointer<vtkDoubleArray>::New();
			tempcellnormalllist->SetNumberOfComponents(3);
			tempcellnormalllist->SetNumberOfTuples(originalpd->GetNumberOfCells());

			for (unsigned long int i = 0; i < originalpd->GetNumberOfCells(); i++) {
				if (drillholeedgecellarray->GetComponent(i, 0) == 0)
					continue;

				cellinegiborpointids->Reset();
				cellipointinegiborpointids->Reset();
				for (int j = 0; j < 3; j++) {
					templist->Reset();
					templist->InsertNextId(originalpd->GetCell(i)->GetPointId(j));
					originalpd->GetCellNeighbors(i, templist, cellipointinegiborpointids);
					for (int k = 0; k < cellipointinegiborpointids->GetNumberOfIds(); k++) {
						cellinegiborpointids->InsertUniqueId(cellipointinegiborpointids->GetId(k));
					}
				}
				if (cellinegiborpointids->GetNumberOfIds() == 0)
					continue;
				double sum2x = 0, sum2y = 0, sum2z = 0;
				for (int m = 0; m < cellinegiborpointids->GetNumberOfIds(); m++) {
					sum2x += normalarray->GetComponent(cellinegiborpointids->GetId(m), 0);
					sum2y += normalarray->GetComponent(cellinegiborpointids->GetId(m), 1);
					sum2z += normalarray->GetComponent(cellinegiborpointids->GetId(m), 2);
				}
				double norm2 = sqrt(sum2x*sum2x + sum2y*sum2y + sum2z*sum2z);
				if (norm2 == 0)
					continue;
				sum2x = sum2x / norm2;
				sum2y = sum2y / norm2;
				sum2z = sum2z / norm2;
				tempcellnormalllist->SetTuple3(i, sum2x, sum2y, sum2z);

			}

			for (int i = 0; i < originalpd->GetNumberOfCells(); i++) {
				if (drillholeedgecellarray->GetComponent(i, 0) == 0)
					continue;
				normalarray->SetTuple3(i, tempcellnormalllist->GetComponent(i, 0), tempcellnormalllist->GetComponent(i, 1), tempcellnormalllist->GetComponent(i, 2));

			}

			for (int i = 0; i < originalpd->GetNumberOfPoints(); i++) {
				if (drillholeedgepointarray->GetComponent(i, 0) == 0)
					continue;

				pointinegiborcellids->Reset();
				originalpd->GetPointCells(i, pointinegiborcellids);
				if (pointinegiborcellids->GetNumberOfIds() == 0)
					continue;

				double sum2x = 0, sum2y = 0, sum2z = 0;
				for (int j = 0; j < pointinegiborcellids->GetNumberOfIds(); j++) {
					sum2x += normalarray->GetComponent(pointinegiborcellids->GetId(j), 0);
					sum2y += normalarray->GetComponent(pointinegiborcellids->GetId(j), 1);
					sum2z += normalarray->GetComponent(pointinegiborcellids->GetId(j), 2);

				}
				double norm2 = sqrt(sum2x*sum2x + sum2y*sum2y + sum2z*sum2z);
				if (norm2 == 0)
					continue;
				sum2x = sum2x / norm2;
				sum2y = sum2y / norm2;
				sum2z = sum2z / norm2;
				pointnormalarray->SetTuple3(i, sum2x, sum2y, sum2z);
			}
		}

		//Smooth out normal (no warning when there is degenerated triangle) end;


		cachepd->SetPoints(pointlist);
		cachepd->SetStrips(celllist);
		cachepd->GetCellData()->SetNormals(normalarray);
		cachepd->GetPointData()->SetNormals(pointnormalarray);
	}
	else {
		cachepd = normalreader->GetOutput();
	}


	if (naivemode == 0 && fixmode == 1) {
		bool aggressivemode = 1;				// To control how aggressive in flattening a problematic local patch of the mesh. Recommend: on.

		bool silentmode = 0;						// To suppress output or not.
		bool meshqualitycontrol = 0;				// To control whether splitting triangles with "large" area.
		int max_num_fix = 600;						// Max number of fix will be performed on a mesh.
		double angular_change_tolerance = 50;		// Max angular change of each cell normal before/after the extrusion, setting it low can prevent local singularity formed by multiple cells but will hugely increase computational time.
													// Suppose a local singularity formed by n cell is to be prevented, then angular_change_tolerance<=(180-max_feature_angle)/n is required.
													// Normally take value (degree) between 0 to 90. 
		bool progressreport = 0;					// Set to report progress for every 10 % of cell completed.

		vtkSmartPointer<vtkPoints> pointlist = vtkSmartPointer<vtkPoints>::New();
		for (int i = 0; i < cachepd->GetNumberOfPoints(); i++) {
			pointlist->InsertNextPoint(cachepd->GetPoint(i)[0], cachepd->GetPoint(i)[1], cachepd->GetPoint(i)[2]);
		}

		vtkSmartPointer<vtkCellArray> celllist = vtkSmartPointer<vtkCellArray>::New();
		for (int i = 0; i < cachepd->GetNumberOfCells(); i++) {
			celllist->InsertNextCell(3);
			for (int j = 0; j < 3; j++) {
				celllist->InsertCellPoint(cachepd->GetCell(i)->GetPointId(j));
			}
		}

		vtkSmartPointer<vtkDoubleArray> smoothedcellnormal = vtkSmartPointer<vtkDoubleArray>::New();
		smoothedcellnormal->SetNumberOfComponents(3);
		smoothedcellnormal->SetNumberOfTuples(cachepd->GetNumberOfCells());
		for (int i = 0; i < cachepd->GetNumberOfCells(); i++) {
			smoothedcellnormal->SetTuple3(i, cachepd->GetCellData()->GetNormals()->GetComponent(i, 0), cachepd->GetCellData()->GetNormals()->GetComponent(i, 1), cachepd->GetCellData()->GetNormals()->GetComponent(i, 2));
		}

		vtkSmartPointer<vtkDoubleArray> smoothedpointnormal = vtkSmartPointer<vtkDoubleArray>::New();
		smoothedpointnormal->SetNumberOfComponents(3);
		smoothedpointnormal->SetNumberOfTuples(cachepd->GetNumberOfPoints());
		for (int i = 0; i < cachepd->GetNumberOfPoints(); i++) {
			smoothedpointnormal->SetTuple3(i, cachepd->GetPointData()->GetNormals()->GetComponent(i, 0), cachepd->GetPointData()->GetNormals()->GetComponent(i, 1), cachepd->GetPointData()->GetNormals()->GetComponent(i, 2));
		}
		double maxarea;
		if (meshqualitycontrol) {
			double sumarea = 0, sum2area = 0;
			for (int i = 0; i < cachepd->GetNumberOfCells(); i++) {
				double point1[3], point2[3], point3[3];
				int id1 = cachepd->GetCell(i)->GetPointId(0), id2 = cachepd->GetCell(i)->GetPointId(1), id3 = cachepd->GetCell(i)->GetPointId(2);
				cachepd->GetPoint(id1, point1);
				cachepd->GetPoint(id2, point2);
				cachepd->GetPoint(id3, point3);
				double area = vtkTriangle::TriangleArea(point1, point2, point3);
				sumarea += area;
				sum2area += area*area;
			}
			const double averagearea = sumarea / cachepd->GetNumberOfCells();
			const double sdarea = sqrt(1.0 / cachepd->GetNumberOfCells()*sum2area - averagearea*averagearea);
			double maxarea1 = 1.0*(averagearea + 5.0*sdarea), maxarea2 = 5.0*averagearea; //these parameters are subject to change
			maxarea = maxarea1;
			if (maxarea2 < maxarea1)
				maxarea = maxarea2;
			// Take minimum over these 2 parameter.
		}

		cachepd->Reset(); //This cachepd will be changed multiple times in the following WHILE loop

		bool extrusiongreenlight = 0;
		int count = -1;
		int progressreporter = 0;
		const long double PI = 3.14159265358979323846;
		double cos_of_tolerance = cos(angular_change_tolerance * PI / 180);

		while (extrusiongreenlight == 0) {
			count++;
			if (silentmode == 0)
				std::cout << count << " ";

			int marker = progressreporter*0.1*cachepd->GetNumberOfCells();

			cachepd = vtkSmartPointer<vtkPolyData>::New();
			cachepd->SetPoints(pointlist);
			cachepd->SetStrips(celllist);
			cachepd->GetCellData()->SetNormals(smoothedcellnormal);
			cachepd->GetPointData()->SetNormals(smoothedpointnormal);


			if (count == max_num_fix) {
				if (silentmode == 0)
					std::cout << "Program timeout!\n";
				break;
			}

			cachepd->BuildLinks();

			vtkSmartPointer<vtkDoubleArray> tempcellnormallist = vtkSmartPointer<vtkDoubleArray>::New();
			tempcellnormallist->SetNumberOfComponents(3);
			tempcellnormallist->SetNumberOfTuples(cachepd->GetNumberOfCells());
			vtkSmartPointer<vtkDoubleArray> temppointnormallist = vtkSmartPointer<vtkDoubleArray>::New();
			temppointnormallist->SetNumberOfComponents(3);
			temppointnormallist->SetNumberOfTuples(cachepd->GetNumberOfPoints());

			//Smooth 1 time(Mean filter);
			for (unsigned long int i = 0; i < cachepd->GetNumberOfCells(); i++) {
				vtkSmartPointer<vtkIdList> cellinegiborpointids = vtkSmartPointer<vtkIdList>::New();
				vtkSmartPointer<vtkIdList> cellipointinegiborpointids = vtkSmartPointer<vtkIdList>::New();

				for (int j = 0; j < 3; j++) {
					vtkSmartPointer<vtkIdList> templist = vtkSmartPointer<vtkIdList>::New();
					templist->InsertNextId(cachepd->GetCell(i)->GetPointId(j));
					cachepd->GetCellNeighbors(i, templist, cellipointinegiborpointids);
					for (int k = 0; k < cellipointinegiborpointids->GetNumberOfIds(); k++) {
						cellinegiborpointids->InsertUniqueId(cellipointinegiborpointids->GetId(k));
					}
				}
				if (cellinegiborpointids->GetNumberOfIds() == 0)
					continue;
				double sum2x = 0, sum2y = 0, sum2z = 0;
				for (int m = 0; m < cellinegiborpointids->GetNumberOfIds(); m++) {
					sum2x += cachepd->GetCellData()->GetNormals()->GetComponent(cellinegiborpointids->GetId(m), 0);
					sum2y += cachepd->GetCellData()->GetNormals()->GetComponent(cellinegiborpointids->GetId(m), 1);
					sum2z += cachepd->GetCellData()->GetNormals()->GetComponent(cellinegiborpointids->GetId(m), 2);
				}
				double norm2 = sqrt(sum2x*sum2x + sum2y*sum2y + sum2z*sum2z);
				if (norm2 == 0)
					continue;
				sum2x = sum2x / norm2;
				sum2y = sum2y / norm2;
				sum2z = sum2z / norm2;
				tempcellnormallist->SetTuple3(i, sum2x, sum2y, sum2z);
			}

			for (int i = 0; i < cachepd->GetNumberOfPoints(); i++) {
				vtkSmartPointer<vtkIdList> pointinegiborcellids = vtkSmartPointer<vtkIdList>::New();
				cachepd->GetPointCells(i, pointinegiborcellids);
				if (pointinegiborcellids->GetNumberOfIds() == 0)
					continue;

				double sum2x = 0, sum2y = 0, sum2z = 0;
				for (int j = 0; j < pointinegiborcellids->GetNumberOfIds(); j++) {
					sum2x += cachepd->GetCellData()->GetNormals()->GetComponent(pointinegiborcellids->GetId(j), 0);
					sum2y += cachepd->GetCellData()->GetNormals()->GetComponent(pointinegiborcellids->GetId(j), 1);
					sum2z += cachepd->GetCellData()->GetNormals()->GetComponent(pointinegiborcellids->GetId(j), 2);
				}
				double norm2 = sqrt(sum2x*sum2x + sum2y*sum2y + sum2z*sum2z);
				if (norm2 == 0)
					continue;
				sum2x = sum2x / norm2;
				sum2y = sum2y / norm2;
				sum2z = sum2z / norm2;
				temppointnormallist->SetTuple3(i, sum2x, sum2y, sum2z);
			}

			//Update smoothednormallist for cell and point;
			for (int i = 0; i < smoothedcellnormal->GetNumberOfTuples(); i++)
				smoothedcellnormal->SetTuple3(i, tempcellnormallist->GetComponent(i, 0), tempcellnormallist->GetComponent(i, 1), tempcellnormallist->GetComponent(i, 2));
			for (int i = 0; i < smoothedpointnormal->GetNumberOfTuples(); i++)
				smoothedpointnormal->SetTuple3(i, temppointnormallist->GetComponent(i, 0), temppointnormallist->GetComponent(i, 1), temppointnormallist->GetComponent(i, 2));

			//Performing extrusion test on each cell;
			for (int i = 0; i < cachepd->GetNumberOfCells(); i++) {
				double point1[3], point2[3], point3[3], normal1[3], normal2[3], normal3[3];
				for (int j = 0; j < 3; j++) {
					point1[j] = cachepd->GetPoint(cachepd->GetCell(i)->GetPointId(0))[j];
					point2[j] = cachepd->GetPoint(cachepd->GetCell(i)->GetPointId(1))[j];
					point3[j] = cachepd->GetPoint(cachepd->GetCell(i)->GetPointId(2))[j];
					normal1[j] = smoothedpointnormal->GetComponent(cachepd->GetCell(i)->GetPointId(0), j);
					normal2[j] = smoothedpointnormal->GetComponent(cachepd->GetCell(i)->GetPointId(1), j);
					normal3[j] = smoothedpointnormal->GetComponent(cachepd->GetCell(i)->GetPointId(2), j);
				}
				double v1[3], v2[3], n1[3], n2[3];
				for (int j = 0; j < 3; j++) {
					v1[j] = point2[j] - point1[j];
					v2[j] = point3[j] - point1[j];
					n1[j] = normal2[j] - normal1[j];
					n2[j] = normal3[j] - normal1[j];
				}
				double offsetted1[3], offsetted2[3];
				for (int j = 0; j < 3; j++) {
					offsetted1[j] = v1[j] + extrusionfactor*n1[j];
					offsetted2[j] = v2[j] + extrusionfactor*n2[j];
				}
				double cross1[3], cross2[3];
				vtkMath::Cross(v1, v2, cross1);
				vtkMath::Cross(offsetted1, offsetted2, cross2);
				double value = vtkMath::Dot(cross1, cross2);
				double norm1 = vtkMath::Norm(cross1);
				double norm2 = vtkMath::Norm(cross2);
				double thershold = cos_of_tolerance*norm1*norm2;

				//Check if the given extrusionfactor will cause trouble to cell i;
				if (value >= thershold) {
					if (i + 1 == cachepd->GetNumberOfCells())
						extrusiongreenlight = 1;
					continue;
				}

				else //Cell i must have problem then.
				{
					if (silentmode == 0)
						std::cout << " problem at cell " << i << "\n";

					if (i > marker && progressreport) {
						std::cout << progressreporter * 10 << " % completed.\n";
						progressreporter++;
					}

					//Calculate centroid;
					double centroid[3] = { 0,0,0 };
					for (int j = 0; j < 3; j++) {
						centroid[j] = point1[j] / 3 + point2[j] / 3 + point3[j] / 3;
					}

					//Detect if other cell (share an edge) or (share a point only) with cell i;
					vtkSmartPointer<vtkIntArray> numsharedpointwithcellilist = vtkSmartPointer<vtkIntArray>::New();
					numsharedpointwithcellilist->SetNumberOfComponents(1);
					numsharedpointwithcellilist->SetNumberOfTuples(cachepd->GetNumberOfCells());

					//initialize;
					for (int j = 0; j < numsharedpointwithcellilist->GetNumberOfTuples(); j++)
						numsharedpointwithcellilist->SetTuple1(j, 0);

					int id1 = cachepd->GetCell(i)->GetPointId(0);
					int id2 = cachepd->GetCell(i)->GetPointId(1);
					int id3 = cachepd->GetCell(i)->GetPointId(2);

					//Count number of share point of each cell j and the problematic cell i
					for (int j = 0; j < numsharedpointwithcellilist->GetNumberOfTuples(); j++) {
						for (int k = 0; k < 3; k++) {
							if (cachepd->GetCell(j)->GetPointId(k) == id1 || cachepd->GetCell(j)->GetPointId(k) == id2 || cachepd->GetCell(j)->GetPointId(k) == id3) {
								int originalvalue = numsharedpointwithcellilist->GetComponent(j, 0);
								numsharedpointwithcellilist->SetTuple1(j, originalvalue + 1);
							}
						}
					}


					//For agressivemode, we make the centroid(Newly constructed point) to be the centerpoint of all related point;
					if (aggressivemode) {
						int counter = 0;
						double sumx = 0, sumy = 0, sumz = 0;
						for (int j = 0; j < numsharedpointwithcellilist->GetNumberOfTuples(); j++) {
							if (numsharedpointwithcellilist->GetComponent(j, 0) != 1)
								continue;
							else {
								for (int k = 0; k < 3; k++) {
									int tempidk = cachepd->GetCell(j)->GetPointId(k);
									if (tempidk == id1 || tempidk == id2 || tempidk == id3)
										continue;
									else {
										double point[3];
										cachepd->GetPoint(tempidk, point);
										sumx += point[0];
										sumy += point[1];
										sumz += point[2];
										counter++;
									}
								}
							}
						}
						if (counter != 0) {
							centroid[0] = sumx / counter;
							centroid[1] = sumy / counter;
							centroid[2] = sumz / counter;
						}
						else {
							if (silentmode == 0)
								std::cout << "No neighboring cell for cell " << i << " ?\n";
						}

					}


					//Sort id1, id2, id3 so that id1<id2<id3;
					if (id1 > id2) {
						int tmp = id2;
						id2 = id1;
						id1 = tmp;
					}
					if (id2 > id3) {
						int tmp = id3;
						id3 = id2;
						id2 = tmp;
					}
					if (id1 > id2) {
						int tmp = id2;
						id2 = id1;
						id1 = tmp;
					}


					//To collapse(delete) cell i , we collapse the entire triangle into a point (say x0). So cell i and any triangle with a common edge will be deleted.
					//A new point (x0) will be sotred as the last point of the mesh. All triangle shared a single point with cell i will be directed to this new point x0.

					//Update temp point list
					vtkSmartPointer<vtkDoubleArray>temppointlist = vtkSmartPointer<vtkDoubleArray>::New();
					temppointlist->SetNumberOfComponents(3);
					for (int j = 0; j < cachepd->GetNumberOfPoints(); j++) {
						if (j != id1 && j != id2 && j != id3)
							temppointlist->InsertNextTuple3(cachepd->GetPoint(j)[0], cachepd->GetPoint(j)[1], cachepd->GetPoint(j)[2]);
					}
					temppointlist->InsertNextTuple3(centroid[0], centroid[1], centroid[2]);

					//Update temp cell list
					vtkSmartPointer<vtkIntArray>tempcellidlist = vtkSmartPointer<vtkIntArray>::New();
					tempcellidlist->SetNumberOfComponents(3);
					for (int j = 0; j < cachepd->GetNumberOfCells(); j++) {
						if (numsharedpointwithcellilist->GetComponent(j, 0) == 0) {
							//Note that pointid of each cell will be changed if we delete a cell and its three point.
							int firstid = cachepd->GetCell(j)->GetPointId(0), secondid = cachepd->GetCell(j)->GetPointId(1), thirdid = cachepd->GetCell(j)->GetPointId(2);
							if (firstid > id3)
								firstid--;
							if (firstid > id2)
								firstid--;
							if (firstid > id1)
								firstid--;
							if (secondid > id3)
								secondid--;
							if (secondid > id2)
								secondid--;
							if (secondid > id1)
								secondid--;
							if (thirdid > id3)
								thirdid--;
							if (thirdid > id2)
								thirdid--;
							if (thirdid > id1)
								thirdid--;
							tempcellidlist->InsertNextTuple3(firstid, secondid, thirdid);
						}

					}

					int centroidid = temppointlist->GetNumberOfTuples() - 1; //New point is the last point.

																			 //Redirect all affected triangle to the new point.
					for (int j = 0; j < numsharedpointwithcellilist->GetNumberOfTuples(); j++) {
						if (numsharedpointwithcellilist->GetComponent(j, 0) == 1) {
							int testsum = 0;
							if (cachepd->GetCell(j)->GetPointId(0) == id1 || cachepd->GetCell(j)->GetPointId(0) == id2 || cachepd->GetCell(j)->GetPointId(0) == id3) {
								int secondid = cachepd->GetCell(j)->GetPointId(1), thirdid = cachepd->GetCell(j)->GetPointId(2);
								if (secondid > id3)
									secondid--;
								if (secondid > id2)
									secondid--;
								if (secondid > id1)
									secondid--;
								if (thirdid > id3)
									thirdid--;
								if (thirdid > id2)
									thirdid--;
								if (thirdid > id1)
									thirdid--;
								tempcellidlist->InsertNextTuple3(centroidid, secondid, thirdid);
							}

							if (cachepd->GetCell(j)->GetPointId(1) == id1 || cachepd->GetCell(j)->GetPointId(1) == id2 || cachepd->GetCell(j)->GetPointId(1) == id3) {
								int secondid = cachepd->GetCell(j)->GetPointId(0), thirdid = cachepd->GetCell(j)->GetPointId(2);
								if (secondid > id3)
									secondid--;
								if (secondid > id2)
									secondid--;
								if (secondid > id1)
									secondid--;
								if (thirdid > id3)
									thirdid--;
								if (thirdid > id2)
									thirdid--;
								if (thirdid > id1)
									thirdid--;
								tempcellidlist->InsertNextTuple3(secondid, centroidid, thirdid);
							}

							if (cachepd->GetCell(j)->GetPointId(2) == id1 || cachepd->GetCell(j)->GetPointId(2) == id2 || cachepd->GetCell(j)->GetPointId(2) == id3) {
								int secondid = cachepd->GetCell(j)->GetPointId(0), thirdid = cachepd->GetCell(j)->GetPointId(1);
								if (secondid > id3)
									secondid--;
								if (secondid > id2)
									secondid--;
								if (secondid > id1)
									secondid--;
								if (thirdid > id3)
									thirdid--;
								if (thirdid > id2)
									thirdid--;
								if (thirdid > id1)
									thirdid--;
								tempcellidlist->InsertNextTuple3(secondid, thirdid, centroidid);
							}

						}

					}

					//Update pointlist;
					pointlist->Reset();
					pointlist->Initialize();
					for (int j = 0; j < temppointlist->GetNumberOfTuples(); j++) {
						double pointcache[3];
						pointcache[0] = temppointlist->GetComponent(j, 0);
						pointcache[1] = temppointlist->GetComponent(j, 1);
						pointcache[2] = temppointlist->GetComponent(j, 2);
						pointlist->InsertNextPoint(pointcache[0], pointcache[1], pointcache[2]);
					}

					//Update celllist;
					celllist->Reset();
					celllist->Initialize();
					for (int j = 0; j < tempcellidlist->GetNumberOfTuples(); j++) {
						celllist->InsertNextCell(3);
						for (int k = 0; k < 3; k++) {
							int pointcache = tempcellidlist->GetComponent(j, k);
							celllist->InsertCellPoint(pointcache);
						}
					}

					//Store new cell normal in cache;
					vtkSmartPointer<vtkDoubleArray> tempcellnormal = vtkSmartPointer<vtkDoubleArray>::New();
					tempcellnormal->SetNumberOfComponents(3);

					for (int j = 0; j < numsharedpointwithcellilist->GetNumberOfTuples(); j++) {
						if (numsharedpointwithcellilist->GetComponent(j, 0) == 0)//Copy unaltered cell normal first;
							tempcellnormal->InsertNextTuple3(smoothedcellnormal->GetComponent(j, 0), smoothedcellnormal->GetComponent(j, 1), smoothedcellnormal->GetComponent(j, 2));
					}

					for (int j = 0; j < numsharedpointwithcellilist->GetNumberOfTuples(); j++) {
						if (numsharedpointwithcellilist->GetComponent(j, 0) == 1) {
							tempcellnormal->InsertNextTuple3(smoothedcellnormal->GetComponent(j, 0), smoothedcellnormal->GetComponent(j, 1), smoothedcellnormal->GetComponent(j, 2));
						}
					}

					//Store new point normal in cache;
					vtkSmartPointer<vtkDoubleArray> temppointnormal = vtkSmartPointer<vtkDoubleArray>::New();
					temppointnormal->SetNumberOfComponents(3);

					for (int j = 0; j < pointlist->GetNumberOfPoints() - 1; j++) { //Copy unaltered point normal first;
						if (j < id1) {
							temppointnormal->InsertNextTuple3(smoothedpointnormal->GetComponent(j, 0), smoothedpointnormal->GetComponent(j, 1), smoothedpointnormal->GetComponent(j, 2));
							continue;
						}
						if (j >= id1 && j < id2) {
							temppointnormal->InsertNextTuple3(smoothedpointnormal->GetComponent(j + 1, 0), smoothedpointnormal->GetComponent(j + 1, 1), smoothedpointnormal->GetComponent(j + 1, 2));
							continue;
						}
						if (j >= id2 && j < id3) {
							temppointnormal->InsertNextTuple3(smoothedpointnormal->GetComponent(j + 2, 0), smoothedpointnormal->GetComponent(j + 2, 1), smoothedpointnormal->GetComponent(j + 2, 2));
							continue;
						}
						if (j >= id3) {
							temppointnormal->InsertNextTuple3(smoothedpointnormal->GetComponent(j + 3, 0), smoothedpointnormal->GetComponent(j + 3, 1), smoothedpointnormal->GetComponent(j + 3, 2));
							continue;
						}
					}
					//only the final added centroid left, its normal is defined to be original cell normal.
					temppointnormal->InsertNextTuple3(smoothedcellnormal->GetComponent(i, 0), smoothedcellnormal->GetComponent(i, 1), smoothedcellnormal->GetComponent(i, 2));


					//Update cell normal
					smoothedcellnormal->Reset();
					smoothedcellnormal->Initialize();
					smoothedcellnormal->SetNumberOfComponents(3);
					for (int j = 0; j < tempcellnormal->GetNumberOfTuples(); j++)
						smoothedcellnormal->InsertNextTuple3(tempcellnormal->GetComponent(j, 0), tempcellnormal->GetComponent(j, 1), tempcellnormal->GetComponent(j, 2));

					//Update point normal
					smoothedpointnormal->Reset();
					smoothedpointnormal->Initialize();
					smoothedpointnormal->SetNumberOfComponents(3);
					for (int j = 0; j < temppointnormal->GetNumberOfTuples(); j++)
						smoothedpointnormal->InsertNextTuple3(temppointnormal->GetComponent(j, 0), temppointnormal->GetComponent(j, 1), temppointnormal->GetComponent(j, 2));


					//Check for area;
					if (meshqualitycontrol) {
						// A triangle with "large" area wil be broken to 4 smaller triangle, formed center point of the three side of original triangle.
						bool areacheck = 1;
						while (areacheck) {
							int areatrouble = 0; int troublecellid = 0;

							for (int j = 0; j < celllist->GetNumberOfCells(); j++) {
								double t1[3], t2[3], t3[3], temptotalnumcell = celllist->GetNumberOfCells();
								int checkptid1 = celllist->GetData()->GetComponent(4 * j + 1, 0);
								int checkptid2 = celllist->GetData()->GetComponent(4 * j + 2, 0);
								int checkptid3 = celllist->GetData()->GetComponent(4 * j + 3, 0);
								pointlist->GetPoint(checkptid1, t1);
								pointlist->GetPoint(checkptid2, t2);
								pointlist->GetPoint(checkptid3, t3);
								double temparea = vtkTriangle::TriangleArea(t1, t2, t3);
								if (temparea > maxarea) {
									areatrouble = 1;
									troublecellid = j;
								}
							}

							if (areatrouble == 0) {
								areacheck = 0;
								break;
							}

							//Create new pd by storing in cache first;
							numsharedpointwithcellilist->Reset();
							numsharedpointwithcellilist->Initialize();
							numsharedpointwithcellilist->SetNumberOfComponents(1);
							for (int j = 0; j < celllist->GetNumberOfCells(); j++)
								numsharedpointwithcellilist->InsertNextTuple1(0);
							for (int k = 0; k < 3; k++) {
								int targetid = celllist->GetData()->GetComponent(4 * troublecellid + 1 + k, 0);
								for (int j = 0; j < numsharedpointwithcellilist->GetNumberOfTuples(); j++) {
									for (int m = 0; m < 3; m++) {
										if (celllist->GetData()->GetComponent(4 * j + m + 1, 0) == targetid) {
											int cachevalue = numsharedpointwithcellilist->GetComponent(j, 0);
											numsharedpointwithcellilist->SetTuple1(j, cachevalue + 1);
										}
									}
								}
							}

							temppointlist->Reset();
							temppointlist->Initialize();
							temppointlist->SetNumberOfComponents(3);
							tempcellidlist->Reset();
							tempcellidlist->Initialize();
							tempcellidlist->SetNumberOfComponents(3);
							tempcellnormal->Reset();
							tempcellnormal->Initialize();
							tempcellnormal->SetNumberOfComponents(3);
							temppointnormal->Reset();
							temppointnormal->Initialize();
							temppointnormal->SetNumberOfComponents(3);
							double t1[3], t2[3], t3[3];
							pointlist->GetPoint(celllist->GetData()->GetComponent(4 * troublecellid + 1, 0), t1);
							pointlist->GetPoint(celllist->GetData()->GetComponent(4 * troublecellid + 2, 0), t2);
							pointlist->GetPoint(celllist->GetData()->GetComponent(4 * troublecellid + 3, 0), t3);
							double tempcenter1[3], tempcenter2[3], tempcenter3[3];
							for (int m = 0; m < 3; m++) {
								tempcenter1[m] = t1[m] / 2 + t2[m] / 2;
								tempcenter2[m] = t2[m] / 2 + t3[m] / 2;
								tempcenter3[m] = t3[m] / 2 + t1[m] / 2;

							}
							for (int j = 0; j < pointlist->GetNumberOfPoints(); j++)
								temppointlist->InsertNextTuple3(pointlist->GetData()->GetComponent(j, 0), pointlist->GetData()->GetComponent(j, 1), pointlist->GetData()->GetComponent(j, 2));
							temppointlist->InsertNextTuple3(tempcenter1[0], tempcenter1[1], tempcenter1[2]);
							temppointlist->InsertNextTuple3(tempcenter2[0], tempcenter2[1], tempcenter2[2]);
							temppointlist->InsertNextTuple3(tempcenter3[0], tempcenter3[1], tempcenter3[2]);


							for (int j = 0; j < celllist->GetNumberOfCells(); j++) {
								if (numsharedpointwithcellilist->GetComponent(j, 0) <= 1) {
									int cache[3];
									for (int k = 0; k < 3; k++)
										cache[k] = celllist->GetData()->GetComponent(4 * j + k + 1, 0);
									tempcellidlist->InsertNextTuple3(cache[0], cache[1], cache[2]);
								}
							}
							int troublecellpt1id = celllist->GetData()->GetComponent(4 * troublecellid + 1, 0), troublecellpt2id = celllist->GetData()->GetComponent(4 * troublecellid + 2, 0), troublecellpt3id = celllist->GetData()->GetComponent(4 * troublecellid + 3, 0);
							int oldnumofpt = pointlist->GetNumberOfPoints();
							int count12 = 0, count23 = 0, count31 = 0, cell12id, cell23id, cell31id;
							for (int j = 0; j < celllist->GetNumberOfCells(); j++) {
								if (numsharedpointwithcellilist->GetComponent(j, 0) == 2) {
									int leftoverid;
									for (int k = 0; k < 3; k++) {
										int cacheidkofj = celllist->GetData()->GetComponent(4 * j + k + 1, 0);
										if ((cacheidkofj - troublecellpt1id)*(cacheidkofj - troublecellpt2id)*(cacheidkofj - troublecellpt3id) != 0) {
											leftoverid = cacheidkofj;
										}
									}
									int neighbortype; //>0 =>1-2, =0 =>2-3, <0 => 3-1 type;
									if (troublecellpt1id == celllist->GetData()->GetComponent(4 * j + 1, 0) || troublecellpt1id == celllist->GetData()->GetComponent(4 * j + 2, 0) || troublecellpt1id == celllist->GetData()->GetComponent(4 * j + 3, 0)) {
										if (troublecellpt2id == celllist->GetData()->GetComponent(4 * j + 1, 0) || troublecellpt2id == celllist->GetData()->GetComponent(4 * j + 2, 0) || troublecellpt2id == celllist->GetData()->GetComponent(4 * j + 3, 0)) {
											neighbortype = 1;
										}
										else neighbortype = -1;
									}
									else neighbortype = 0;

									if (neighbortype > 0) {
										tempcellidlist->InsertNextTuple3(troublecellpt1id, leftoverid, oldnumofpt);
										tempcellidlist->InsertNextTuple3(troublecellpt2id, oldnumofpt, leftoverid);
										count12 = count12 + 2;
										cell12id = j;
									}
									else if (neighbortype == 0) {
										tempcellidlist->InsertNextTuple3(troublecellpt2id, leftoverid, oldnumofpt + 1);
										tempcellidlist->InsertNextTuple3(troublecellpt3id, oldnumofpt + 1, leftoverid);
										count23 = count23 + 2;
										cell23id = j;
									}
									else {
										tempcellidlist->InsertNextTuple3(troublecellpt3id, leftoverid, oldnumofpt + 2);
										tempcellidlist->InsertNextTuple3(troublecellpt1id, oldnumofpt + 2, leftoverid);
										count31 = count31 + 2;
										cell31id = j;
									}
								}
							}
							if (count12 > 2 || count23 > 2 || count31 > 2) {
								if (silentmode == 0) {
									std::cout << "Problem arises in cell reconstruction due to the existence of non-manifold edge!!\n";
								}
							}

							tempcellidlist->InsertNextTuple3(troublecellpt1id, oldnumofpt, oldnumofpt + 2);
							tempcellidlist->InsertNextTuple3(troublecellpt2id, oldnumofpt + 1, oldnumofpt);
							tempcellidlist->InsertNextTuple3(troublecellpt3id, oldnumofpt + 2, oldnumofpt + 1);
							tempcellidlist->InsertNextTuple3(oldnumofpt, oldnumofpt + 1, oldnumofpt + 2);


							for (int j = 0; j < smoothedcellnormal->GetNumberOfTuples(); j++) {
								if (numsharedpointwithcellilist->GetComponent(j, 0) <= 1)
									tempcellnormal->InsertNextTuple3(smoothedcellnormal->GetComponent(j, 0), smoothedcellnormal->GetComponent(j, 1), smoothedcellnormal->GetComponent(j, 2));
							}
							for (int j = 0; j < count12; j++)
								tempcellnormal->InsertNextTuple3(smoothedcellnormal->GetComponent(cell12id, 0), smoothedcellnormal->GetComponent(cell12id, 1), smoothedcellnormal->GetComponent(cell12id, 2));
							for (int j = 0; j < count23; j++)
								tempcellnormal->InsertNextTuple3(smoothedcellnormal->GetComponent(cell23id, 0), smoothedcellnormal->GetComponent(cell23id, 1), smoothedcellnormal->GetComponent(cell23id, 2));
							for (int j = 0; j < count31; j++)
								tempcellnormal->InsertNextTuple3(smoothedcellnormal->GetComponent(cell31id, 0), smoothedcellnormal->GetComponent(cell31id, 1), smoothedcellnormal->GetComponent(cell31id, 2));
							for (int j = 0; j < 4; j++)
								tempcellnormal->InsertNextTuple3(smoothedcellnormal->GetComponent(troublecellid, 0), smoothedcellnormal->GetComponent(troublecellid, 1), smoothedcellnormal->GetComponent(troublecellid, 2));

							for (int j = 0; j < smoothedpointnormal->GetNumberOfTuples(); j++)
								temppointnormal->InsertNextTuple3(smoothedpointnormal->GetComponent(j, 0), smoothedpointnormal->GetComponent(j, 1), smoothedpointnormal->GetComponent(j, 2));
							double normal1[3], normal2[3], normal3[3];;
							for (int j = 0; j < 3; j++) {
								normal1[j] = smoothedpointnormal->GetComponent(troublecellpt1id, j) / 2 + smoothedpointnormal->GetComponent(troublecellpt2id, j) / 2;
								normal2[j] = smoothedpointnormal->GetComponent(troublecellpt3id, j) / 2 + smoothedpointnormal->GetComponent(troublecellpt2id, j) / 2;
								normal3[j] = smoothedpointnormal->GetComponent(troublecellpt1id, j) / 2 + smoothedpointnormal->GetComponent(troublecellpt3id, j) / 2;
							}
							double tempnorm;
							tempnorm = sqrt(normal1[0] * normal1[0] + normal1[1] * normal1[1] + normal1[2] * normal1[2]);
							if (tempnorm == 0 || silentmode == 0)
								std::cerr << "ERROR for normal";
							for (int j = 0; j < 3; j++)
								normal1[j] = normal1[j] / tempnorm;
							tempnorm = sqrt(normal2[0] * normal2[0] + normal2[1] * normal2[1] + normal2[2] * normal2[2]);
							if (tempnorm == 0 || silentmode == 0)
								std::cerr << "ERROR for normal";
							for (int j = 0; j < 3; j++)
								normal2[j] = normal2[j] / tempnorm;
							tempnorm = sqrt(normal3[0] * normal3[0] + normal3[1] * normal3[1] + normal3[2] * normal3[2]);
							if (tempnorm == 0 || silentmode == 0)
								std::cerr << "ERROR for normal";
							for (int j = 0; j < 3; j++)
								normal3[j] = normal3[j] / tempnorm;
							temppointnormal->InsertNextTuple3(normal1[0], normal1[1], normal1[2]);
							temppointnormal->InsertNextTuple3(normal2[0], normal2[1], normal2[2]);
							temppointnormal->InsertNextTuple3(normal3[0], normal3[1], normal3[2]);


							//Update the data of cachepd;
							pointlist->Reset();
							pointlist->Initialize();
							for (int j = 0; j < temppointlist->GetNumberOfTuples(); j++)
								pointlist->InsertNextPoint(temppointlist->GetComponent(j, 0), temppointlist->GetComponent(j, 1), temppointlist->GetComponent(j, 2));
							celllist->Reset();
							celllist->Initialize();
							for (int j = 0; j < tempcellidlist->GetNumberOfTuples(); j++) {
								celllist->InsertNextCell(3);
								for (int k = 0; k < 3; k++)
									celllist->InsertCellPoint(tempcellidlist->GetComponent(j, k));
							}
							smoothedcellnormal->Reset();
							smoothedcellnormal->Initialize();
							for (int j = 0; j < tempcellnormal->GetNumberOfTuples(); j++)
								smoothedcellnormal->InsertNextTuple3(tempcellnormal->GetComponent(j, 0), tempcellnormal->GetComponent(j, 1), tempcellnormal->GetComponent(j, 2));
							smoothedpointnormal->Reset();
							smoothedpointnormal->Initialize();
							for (int j = 0; j < temppointnormal->GetNumberOfTuples(); j++)
								smoothedpointnormal->InsertNextTuple3(temppointnormal->GetComponent(j, 0), temppointnormal->GetComponent(j, 1), temppointnormal->GetComponent(j, 2));

							//To check if any degenerated triangle in the mesh.
							for (int j = 0; j < celllist->GetNumberOfCells(); j++) {
								int pid1 = celllist->GetData()->GetComponent(4 * j + 1, 0);
								int pid2 = celllist->GetData()->GetComponent(4 * j + 2, 0);
								int pid3 = celllist->GetData()->GetComponent(4 * j + 3, 0);
								if ((pid1 - pid2)*(pid2 - pid3)*(pid3 - pid1) == 0 || silentmode == 0) {
									std::cout << "Degenerated triangle detected. ";
									tempcellidlist->Reset();
									tempcellidlist->Initialize();
									tempcellidlist->SetNumberOfComponents(3);
									tempcellnormal->Reset();
									tempcellnormal->Initialize();
									tempcellnormal->SetNumberOfComponents(3);
									for (int k = 0; k < celllist->GetNumberOfCells(); k++) {
										if (k != j)
											tempcellidlist->InsertNextTuple3(celllist->GetData()->GetComponent(4 * k + 1, 0), celllist->GetData()->GetComponent(4 * k + 2, 0), celllist->GetData()->GetComponent(4 * k + 3, 0));
									}
									for (int k = 0; k < smoothedcellnormal->GetNumberOfTuples(); k++) {
										if (k != j)
											tempcellnormal->InsertNextTuple3(smoothedcellnormal->GetComponent(k, 0), smoothedcellnormal->GetComponent(k, 1), smoothedcellnormal->GetComponent(k, 2));
									}
									celllist->Reset();
									celllist->Initialize();
									for (int j = 0; j < tempcellidlist->GetNumberOfTuples(); j++) {
										celllist->InsertNextCell(3);
										for (int k = 0; k < 3; k++)
											celllist->InsertCellPoint(tempcellidlist->GetComponent(j, k));
									}
									smoothedcellnormal->Reset();
									smoothedcellnormal->Initialize();
									for (int j = 0; j < tempcellnormal->GetNumberOfTuples(); j++)
										smoothedcellnormal->InsertNextTuple3(tempcellnormal->GetComponent(j, 0), tempcellnormal->GetComponent(j, 1), tempcellnormal->GetComponent(j, 2));
									if (silentmode == 0)
										std::cout << "  Tango down!\n";
								}
							}
							if (silentmode == 0)
								std::cout << ".";
						}//while areacheck end;
					}//meshqualitycontrol mode end;

					if (silentmode == 0)
						std::cout << "\n";
					break;  //To go through another complete check of the mesh.
				}//Else end

			} //for each cell i

		}//While end;


	}


	vtkSmartPointer<vtkLinearExtrusionFilter> extrusion = vtkSmartPointer<vtkLinearExtrusionFilter>::New();
	extrusion->SetInputData(cachepd);
	extrusion->CappingOn();
	extrusion->SetExtrusionTypeToNormalExtrusion();
	extrusion->SetScaleFactor(extrusionfactor);   
	extrusion->Update();

	this->Outputpd = vtkSmartPointer<vtkPolyData>::New();
	this->Outputpd = extrusion->GetOutput();


}