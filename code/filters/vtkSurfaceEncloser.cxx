/*
Author:		Wong, Matthew Lun
Date:		2nd, Feb, 2016
Occupation:	Chinese University of Hong Kong,
			Department of Imaging and Inteventional Radiology,
			Junior Research Assistant


This program is implemented to build a surface between two lines with different
number of points and distrubution.


Wong Matthew Lun
Copyright (C) 2016

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "vtkSurfaceEncloser.h"


#if VTK_MAJOR_VERSION < 6
#define SetInputData SetInput
#define AddInputData AddInput
#endif

SurfaceEncloser::SurfaceEncloser()
{
	LOOP_FLAG = false;
	m_edge1 = vtkSmartPointer<vtkPolyData>::New();
	m_edge2 = vtkSmartPointer<vtkPolyData>::New();
	m_shiftedEdge2 = vtkSmartPointer<vtkPolyData>::New();
	m_loopDirection = (double*)malloc(sizeof(double) * 3);
	m_loopDirection[0] = 0;
	m_loopDirection[1] = 0;
	m_loopDirection[2] = 1;
}


SurfaceEncloser::~SurfaceEncloser()
{
	free(m_loopDirection);
}

vtkPolyData * SurfaceEncloser::GetOutput()
{
	return m_outputPD;
}

vtkPolyData * SurfaceEncloser::GetShiftedEdge()
{
	return m_shiftedEdge2;
}

void SurfaceEncloser::SetInput(vtkPolyData * edge1, vtkPolyData * edge2)
{
	m_edge1 = edge1;
	m_edge2 = edge2;
}

void SurfaceEncloser::SetEdgesAreLoops(bool val)
{
	LOOP_FLAG = val;
}

// Return 0 where there are no error. 
int SurfaceEncloser::Update()
{
	// Reconstruct the polydata to order the id first
	this->_RestructurePD(m_edge1, m_referenceCrossProduct);

	//Find the normal of loop m_edge1 as m_referenceCrossProduct
	vtkSmartPointer<vtkCenterOfMass> comFilter = vtkSmartPointer<vtkCenterOfMass>::New();
	comFilter->SetInputData(m_edge1);
	comFilter->Update();
	memcpy(m_referenceCrossProduct, this->_FindNormal(m_edge1, comFilter->GetCenter()), sizeof(double)*3);

	// Use m_edge1's loop normal as reference normal of m_edge2
	this->_RestructurePD(m_edge2, m_referenceCrossProduct);

	

	m_outputPD = vtkPolyData::New();
	m_appendedInput = vtkPolyData::New();
	vtkSmartPointer<vtkAppendPolyData> adder = vtkSmartPointer<vtkAppendPolyData>::New();
	adder->AddInputData(m_edge1);
	adder->AddInputData(m_edge2);
	adder->Update();

	m_outputPD->DeepCopy(adder->GetOutput());
	m_outputPD->BuildLinks();

	m_appendedInput->DeepCopy(adder->GetOutput());
	m_appendedInput->BuildLinks();

	return this->_CloseSurface();
}
void SurfaceEncloser::_FormCells(vtkIdType origin, vtkIdList *idlist)
{
	vtkIdType pts[3];
	for (int i = 0; i < idlist->GetNumberOfIds() - 1; i++)
	{
		// tackle normal direction issues
		if (origin < m_edge1->GetNumberOfPoints()) {
			pts[2] = origin;
			pts[1] = idlist->GetId(i);
			pts[0] = idlist->GetId(i + 1);
		}
		else {
			pts[0] = origin;
			pts[1] = idlist->GetId(i);
			pts[2] = idlist->GetId(i + 1);
		}
		m_outputPD->InsertNextLinkedCell(VTK_TRIANGLE, 3, pts);
	}
}


// Align the center of mass of m_edge1 and m_edge2 and produce m_shiftedEdge2 (leaving m_edge1 unchanged)
// This should be improved by aligning the normal of the surface formed from the line to make this more 
// general, but thats sort of like doing a no-scale rigid registration.
int SurfaceEncloser::_AlignLoops()
{
	// Find centroid of points
	double *com1 = (double*)malloc(sizeof(double) * 3);
	double *com2 = (double*)malloc(sizeof(double) * 3);
	double *direction = (double*)malloc(sizeof(double) * 3);
	vtkSmartPointer<vtkCenterOfMass> comFilter = vtkSmartPointer<vtkCenterOfMass>::New();
	comFilter->SetInputData(m_edge1);
	comFilter->Update();
	memcpy(com1, comFilter->GetCenter(), sizeof(double)*3);

	comFilter->RemoveAllInputs();
	comFilter->SetInputData(m_edge2);
	comFilter->Update();
	memcpy(com2, comFilter->GetCenter(), sizeof(double)*3);

	// Calculate the direction and magnitude for translation
	double dist;
	vtkMath::Subtract(com1, com2, direction);
	dist = vtkMath::Norm(direction);
	vtkMath::Normalize(direction);
	vtkMath::MultiplyScalar(direction, dist);

	// Calculate angle between two normal vectors
	double* normEdge1 = this->_FindNormal(m_edge1, com1);
	double* normEdge2 = this->_FindNormal(m_edge2, com2);
	double angle = vtkMath::DegreesFromRadians(acos(vtkMath::Dot(normEdge1, normEdge2))); // in degree
	double* rotationalAxis = (double*)malloc(sizeof(double) * 3);
	if (angle != 0) {
		vtkMath::Cross(normEdge2, normEdge1, rotationalAxis);
	}
	else {
		rotationalAxis[0] = 0;
		rotationalAxis[1] = 0;
		rotationalAxis[2] = 1;
	}

	// rotation center is com2
	vtkSmartPointer<vtkTransform> shiftTransform = vtkSmartPointer<vtkTransform>::New();
	shiftTransform->PostMultiply();
	shiftTransform->Translate(-com2[0], -com2[1], -com2[2]);
	shiftTransform->RotateWXYZ(angle, rotationalAxis);
	shiftTransform->Translate(com2);
	shiftTransform->Translate(direction);
	shiftTransform->Update();

	vtkSmartPointer<vtkTransformPolyDataFilter> transformFilter = vtkSmartPointer<vtkTransformPolyDataFilter>::New();
	transformFilter->SetTransform(shiftTransform);
	transformFilter->SetInputData(m_edge2);
	transformFilter->Update();
	m_shiftedEdge2->DeepCopy(transformFilter->GetOutput());

	
	free(normEdge1);
	free(normEdge2);
	free(rotationalAxis);
	free(direction);
	free(com1);
	free(com2);
	return 0;
}

int SurfaceEncloser::_CloseSurface()
{
	// Shift edge2 so that the loops' center of mass are aligned
	this->_AlignLoops();

	// Prepare KDTrees
	vtkSmartPointer<vtkKdTree> kdTreeEdge1 = vtkSmartPointer<vtkKdTree>::New();
	vtkSmartPointer<vtkKdTree> kdTreeEdge2 = vtkSmartPointer<vtkKdTree>::New();
	kdTreeEdge1->BuildLocatorFromPoints(m_edge1);
	kdTreeEdge2->BuildLocatorFromPoints(m_shiftedEdge2);
	
	double dist;
	int endpointEdge1 = 0, endpointEdge2 = 0;
	m_edge1PtsInCells = vtkIdList::New();
	m_edge2PtsInCells = vtkIdList::New();

	vtkIdType edge1Cursor = 0, edge2Cursor;
	edge2Cursor = kdTreeEdge2->FindClosestPoint(m_edge1->GetPoint(edge1Cursor), dist);
	m_edge1PtsInCells->InsertUniqueId(edge1Cursor);
	m_edge1PtsInCells->InsertUniqueId(edge1Cursor + 1);
	m_edge2PtsInCells->InsertUniqueId(edge2Cursor + m_edge1->GetNumberOfPoints());

	endpointEdge1 += edge1Cursor;
	endpointEdge2 += edge2Cursor;

	// Manually push last cell
	vtkIdType triangle1[3], triangle2[3];
	triangle1[0] = 0;
	triangle1[1] = edge2Cursor + m_edge1->GetNumberOfPoints();
	triangle1[2] = m_edge1->GetNumberOfPoints() - 1;
	triangle2[0] = edge2Cursor - 1 + m_edge1->GetNumberOfPoints();
	triangle2[1] = edge2Cursor + m_edge1->GetNumberOfPoints();
	triangle2[2] = m_edge1->GetNumberOfPoints() - 1;

	if (triangle2[0] < m_edge1->GetNumberOfPoints()) {
		triangle2[0] = m_appendedInput->GetNumberOfPoints() - 1;
	}
	m_outputPD->InsertNextLinkedCell(VTK_TRIANGLE, 3, triangle1);
	m_outputPD->InsertNextLinkedCell(VTK_TRIANGLE, 3, triangle2);

	// Manually push first cell
	vtkIdType pts[3];
	pts[0] = edge1Cursor;
	pts[1] = edge2Cursor + m_edge1->GetNumberOfPoints();
	pts[2] = edge1Cursor + 1;
	m_outputPD->InsertNextLinkedCell(VTK_TRIANGLE, 3, pts);
	edge1Cursor += 1;



	// while not all points has at least a cell
	int l_counter = 0;
	bool MovingCursor1 = false;
	bool Edge1Finished = false;
	bool Edge2Finished = false;
	while (m_edge1PtsInCells->GetNumberOfIds() + m_edge2PtsInCells->GetNumberOfIds() < m_appendedInput->GetNumberOfPoints()) {
		l_counter += 1;
		double dist;
		vtkIdType l_closestPoint;
		vtkIdType l_nextCell[3];
		vtkIdList *l_tmp = vtkIdList::New();
		// Alternatively move cursors
		if (!MovingCursor1 && !Edge2Finished) {
			// moving cursor 2
			
			// Obtain the closest point (CP) to edge1Cursor in m_edge2
			l_closestPoint = kdTreeEdge2->FindClosestPoint(m_appendedInput->GetPoint(edge1Cursor), dist);
			//cout << "Pairing edge1Cursor " << edge1Cursor << " with edge2 " << edge2Cursor << " to " <<  l_closestPoint << endl;
			// If the CP is same as edge2Cursor, give a self kick to preven endless self looping
			if (l_closestPoint == edge2Cursor) {
				l_closestPoint += 1;
			}
			else if (l_closestPoint < edge2Cursor) {
				// A trick to deal with backward's polylines and undesired neighbour configuration
				l_closestPoint = (edge2Cursor + 1);
			} 
	
			l_closestPoint = l_closestPoint % m_edge2->GetNumberOfPoints(); 

			// Obtain neighboring set from current edge2Cursor to the CP
			if (this->_FindNeighborSet(edge2Cursor, l_closestPoint, l_tmp, m_edge2->GetNumberOfPoints(), m_edge1->GetNumberOfPoints(), endpointEdge2)) {
				std::cerr << "Error! Error while finding neighbor set at interval pt ID: " << edge2Cursor << " to " << l_closestPoint << " for edge2." << endl;
				return 1;
			}
			this->_FormCells(edge1Cursor, l_tmp);
			edge2Cursor = l_closestPoint;
			for (int i = 0; i < l_tmp->GetNumberOfIds(); i++)
			{
				m_edge2PtsInCells->InsertUniqueId(l_tmp->GetId(i));
			}
			if (edge2Cursor == endpointEdge2) {
				Edge2Finished = true;
			}
		}
		else if (MovingCursor1 && !Edge1Finished){
			// moving cursor 1
			// Obtain the closest point (CP) to edge2Cursor in m_edge1
			l_closestPoint = kdTreeEdge1->FindClosestPoint(m_shiftedEdge2->GetPoint(edge2Cursor), dist);
			//cout << "Pairing edge2Cursor " << edge2Cursor << " with edge1 " << edge1Cursor << " to " << l_closestPoint << endl;
			 
			// If the CP is same as edge2Cursor, give a self kick
			if (l_closestPoint == edge1Cursor && l_closestPoint != m_edge1->GetNumberOfPoints() - 1) {
				l_closestPoint += 1;
			}
			else if (l_closestPoint < edge1Cursor) {
				// A trick to deal with backward's polylines and undesired neighbour configuration
				l_closestPoint = edge1Cursor + 1;
			}
			l_closestPoint = l_closestPoint % m_edge1->GetNumberOfPoints();

			// Obtain neighboring set from current edge2Cursor to the CP
			if (this->_FindNeighborSet(edge1Cursor, l_closestPoint, l_tmp, m_edge1->GetNumberOfPoints(), 0, endpointEdge1)) {
				std::cerr << "Error! Error while finding neighbor set at interval pt ID: " << edge2Cursor << " to " << l_closestPoint << " for edge1." << endl;
				return 1;
			}
			this->_FormCells(edge2Cursor + m_edge1->GetNumberOfPoints(), l_tmp);
			edge1Cursor = l_closestPoint;
			for (int i = 0; i < l_tmp->GetNumberOfIds(); i++)
			{
				m_edge1PtsInCells->InsertUniqueId(l_tmp->GetId(i));
			}
			if (edge1Cursor == endpointEdge1) {
				Edge1Finished = true;
			}

		}
		
		// Switch active cursor
		if (MovingCursor1) {
			MovingCursor1 = false;
		}
		else {
			MovingCursor1 = true;
		}
		l_tmp->Delete();

		if (l_counter > (m_edge1->GetNumberOfPoints() + m_edge2->GetNumberOfPoints() * 2)) {
			return 1;
		}
	}


	return 0;
}

int SurfaceEncloser::_FindNeighborSet(vtkIdType start, vtkIdType target, vtkIdList * outSet, int modulus, int offset, vtkIdType endPoint)
{
	vtkIdType l_nextPoint;
	vtkIdType l_loopedCell = -1;
	vtkIdList *output = vtkIdList::New();
	vtkIdList *tmp = vtkIdList::New();
	output->InsertNextId(start + offset);
	//cout << start +offset<< " ";
	while (start != target)
	{
		start += 1;
		// condition detects end point
		if (start >= modulus) {
			start = start%(modulus);
			// Search two current list to see if the point is already used
			vtkSortDataArray::Sort(m_edge1PtsInCells);
			vtkSortDataArray::Sort(m_edge2PtsInCells);
			if (std::binary_search<vtkIdType*, vtkIdType>(
					m_edge1PtsInCells->GetPointer(0),
					m_edge1PtsInCells->GetPointer(m_edge1PtsInCells->GetNumberOfIds() - 1),
					start + offset) 
				||
				std::binary_search<vtkIdType*, vtkIdType>(
					m_edge2PtsInCells->GetPointer(0),
					m_edge2PtsInCells->GetPointer(m_edge2PtsInCells->GetNumberOfIds() - 1),
					start + offset)) 
			{
				continue;
			}
			else {
				// if not, push the point into the output list for triangle generation
				output->InsertNextId(start + offset);
			};
			if (start%(modulus) == endPoint) {
				// break if end point is reached
				break;
			}
		}
		else {
			// Search two current list to see if the point is already used
			vtkSortDataArray::Sort(m_edge1PtsInCells);
			vtkSortDataArray::Sort(m_edge2PtsInCells);
			if (std::binary_search<vtkIdType*, vtkIdType>(
				m_edge1PtsInCells->GetPointer(0),
				m_edge1PtsInCells->GetPointer(m_edge1PtsInCells->GetNumberOfIds() - 1),
				start + offset)
				||
				std::binary_search<vtkIdType*, vtkIdType>(
					m_edge2PtsInCells->GetPointer(0),
					m_edge2PtsInCells->GetPointer(m_edge2PtsInCells->GetNumberOfIds() - 1),
					start + offset)) 
			{
				continue;
			}
			else {
				// if not, push the point into the output list for triangle generation
				output->InsertNextId(start + offset);
			}
		}
		//cout << start + offset << " ";
		
		// Last defense to prevent flooding machine's memory
		if (output->GetNumberOfIds() > 1000) {
			// break if there are too much points in the list, which is probably due to some error
			std::cerr << "Warning! From " << start << " to " << target << " overflowed." << endl;
			return 1;
		}
		
	}
	//cout << endl;
	outSet->DeepCopy(output);
	output->Delete();
	tmp->Delete();
	return 0;
}



double SurfaceEncloser::_CalculateDistant(vtkIdList *list)
{
	double dist = 0;
	double *pt1 = m_outputPD->GetPoint(0), *pt2;
	for (int i = 0; i < list->GetNumberOfIds()-1; i++)
	{
		double* l_tmpVect;
		pt2 = m_outputPD->GetPoint(i + 1);
		vtkMath::Subtract(pt2, pt1, l_tmpVect);
		dist += vtkMath::Norm(l_tmpVect);
	}
	return dist;
}

// Get a list of the cell id
void SurfaceEncloser::_GetCellList(vtkSmartPointer<vtkPolyData> inPD, vtkIdList* outList)
{
	int numOfList = inPD->GetNumberOfCells();
	int l_looper = 0;
	while (outList->GetNumberOfIds() < numOfList) {
		if (inPD->GetCell(l_looper)->GetNumberOfPoints() > 0) {
			outList->InsertNextId(l_looper);
		}
		l_looper += 1;
	}
}

void SurfaceEncloser::_VectorSub(double* v1, double* v2, double* v3) {
	double *tv3 = (double*)malloc(sizeof(double) * 3);
	
	tv3[0] = v1[0] - v2[0];
	tv3[1] = v1[1] - v2[1];
	tv3[2] = v1[2] - v2[2];
	//cout << "v1: " << v1[0] << "," << v1[1] << "," << v1[2] << endl;
	//cout << "v2: " << v2[0] << "," << v2[1] << "," << v2[2] << endl;
	memcpy(v3, tv3, sizeof(double) * 3);
	//cout << "v3: " << v3[0] << "," << v3[1] << "," << v3[2] << endl;
	free(tv3);
}

// Find the point cloud normal with respect to com. Assume Id is ordered
double * SurfaceEncloser::_FindNormal(vtkSmartPointer<vtkPolyData> inPD, double* com)
{
	double* outNormal = (double*)malloc(sizeof(double) * 3);
	outNormal[0] = 0;
	outNormal[1] = 0;
	outNormal[2] = 0;

	for (int i = 0; i < inPD->GetNumberOfPoints(); i++)
	{
		double* crossProduct = (double*)malloc(sizeof(double) * 3);
		double vect1[3], vect2[3];
		vtkMath::Subtract(inPD->GetPoint(i), com, vect1);
		vtkMath::Subtract(inPD->GetPoint((i+1)%inPD->GetNumberOfPoints()), com, vect2);
		vtkMath::Cross(vect2, vect1, crossProduct);
		vtkMath::Add(outNormal, crossProduct, outNormal);
		
		free(crossProduct);
	}
	vtkMath::Normalize(outNormal);
	return outNormal;
}

void SurfaceEncloser::_GetNeighborPoint(vtkSmartPointer<vtkPolyData> inPD, vtkIdType ptId, vtkIdList* outIdlist)
{
	vtkIdList* cellIdList = vtkIdList::New();
	vtkIdList* ptsIdList = vtkIdList::New();

	std::vector<vtkIdType> tmpOutIdsHolder;

	inPD->GetPointCells(ptId, cellIdList);
	for (int i = 0; i < cellIdList->GetNumberOfIds(); i++)
	{
		inPD->GetCellPoints(cellIdList->GetId(i), ptsIdList);
		for (int j = 0; j < ptsIdList->GetNumberOfIds(); j++)
		{
			if (ptsIdList->GetId(j) != ptId) {
				tmpOutIdsHolder.push_back(ptId);
			}
		}
	}

	vtkIdList* tmpOutIdList = vtkIdList::New();
	for (int k = 0; k < tmpOutIdsHolder.size(); ++k)
	{
		tmpOutIdList->InsertNextId(tmpOutIdsHolder.at(k));
	}

	outIdlist->DeepCopy(tmpOutIdList);

	cellIdList->Delete();
	ptsIdList->Delete();
	tmpOutIdList->Delete();
}

// Restructure lines only so that adjacent point has incremental vtkID
void SurfaceEncloser::_RestructurePD(vtkSmartPointer<vtkPolyData> inPD, double referenceDirection[3])
{
	vtkSmartPointer<vtkPolyData> tmpHolder = vtkSmartPointer<vtkPolyData>::New();
	vtkSmartPointer<vtkPoints> tmpPts = vtkSmartPointer<vtkPoints>::New();
	vtkSmartPointer<vtkCellArray> tmpCellArr = vtkSmartPointer<vtkCellArray>::New();

	if (inPD->GetNumberOfPoints() > 2 && inPD->GetNumberOfCells() == 1)
		return;

	// Clean the line before reconstruction to remove duplicated points and cells, which mess with the program
	vtkSmartPointer<vtkCleanPolyData> cleaner = vtkSmartPointer<vtkCleanPolyData>::New();
	cleaner->SetInputData(inPD);
	cleaner->PointMergingOn();
	cleaner->ConvertLinesToPointsOn();
	cleaner->ConvertPolysToLinesOn();
	cleaner->Update();
	inPD->DeepCopy(cleaner->GetOutput());
	inPD->BuildLinks(); // build link for pushing cells

	//vtkXMLPolyDataWriter* writer = vtkXMLPolyDataWriter::New();
	//writer->SetFileName("../data/preRecon.vtp");
	//writer->SetInputData(inPD);
	//writer->Update();
	//writer->Write();

	int l_nextCell;
	int l_nextPt;
	int l_looper = 0;
	int startID = 0;
	int l_prevID = -1;
	// Start from a point that has cell
	vtkIdList* startPointFinder = vtkIdList::New();
	inPD->GetCellPoints(3, startPointFinder);
	int x = startPointFinder->GetId(0);
	int y = startPointFinder->GetId(1);
	l_looper = startPointFinder->GetId(1);
	startID = l_looper;
	tmpPts->InsertNextPoint(inPD->GetPoint(l_looper));


	while (tmpPts->GetNumberOfPoints() < inPD->GetNumberOfPoints()) {
		vtkIdList* neighborPtID = vtkIdList::New();
		vtkIdList* neighborCellID = vtkIdList::New();
		// First point deside direction
		if (tmpPts->GetNumberOfPoints() == 1) {
			// Get CenterOfMass
			vtkSmartPointer<vtkCenterOfMass> comfilter = vtkSmartPointer<vtkCenterOfMass>::New();
			comfilter->SetInputData(inPD);
			comfilter->Update();
			double* com = comfilter->GetCenter();

			// Get neighbor cells
			inPD->GetPointCells(l_looper, neighborCellID);
			//cout << "Number of cell for pt " << l_looper << ": " << neighborCellID->GetNumberOfIds() << " "; //DEBUG
			double *vectorLooper = (double*)malloc(sizeof(double) * 3);
			double *vector1 = (double*)malloc(sizeof(double) * 3);
			double *vector2 = (double*)malloc(sizeof(double) * 3);
			vtkIdType neighborPtId1;
			vtkIdType neighborPtId2;

			inPD->GetCellPoints(neighborCellID->GetId(0), neighborPtID);
			if (neighborPtID->GetId(0) != l_looper) {
				neighborPtId1 = neighborPtID->GetId(0);
			}
			else {
				neighborPtId1 = neighborPtID->GetId(1);
			}
			inPD->GetCellPoints(neighborCellID->GetId(1), neighborPtID);
			if (neighborPtID->GetId(0) != l_looper) {
				neighborPtId2 = neighborPtID->GetId(0);
			}
			else {
				neighborPtId2 = neighborPtID->GetId(1);
			}

			memcpy(vectorLooper, inPD->GetPoint(l_looper), sizeof(double) * 3);
			memcpy(vector1, inPD->GetPoint(neighborPtId1), sizeof(double) * 3);
			memcpy(vector2, inPD->GetPoint(neighborPtId2), sizeof(double) * 3);

			vtkMath::Subtract(vectorLooper, com, vectorLooper);
			vtkMath::Subtract(vector1, com, vector1);
			vtkMath::Subtract(vector2, com, vector2);
			vtkMath::Normalize(vector1);
			vtkMath::Normalize(vector2);

			vtkMath::Cross(vector1, vectorLooper, vector1);
			vtkMath::Cross(vector2, vectorLooper, vector2);

			double d1 = vtkMath::Dot(vector1, referenceDirection);
			double d2 = vtkMath::Dot(vector2, referenceDirection);
			
			if (d1 >= d2) {
				// if d1 follows direction
				l_prevID = neighborCellID->GetId(1);
			}
			else if (d1 <  d2) {
				// if d2 follows direction
				l_prevID = neighborCellID->GetId(0);
			}
			else if (d1*d2 > 0) {
				// both same sign
				std::cerr << "Both neighbor has same dot product with direction. Potential error of loop order direction" << endl;
				l_prevID = neighborCellID->GetId(0);
			}
			else {
				l_prevID = neighborCellID->GetId(0);
			}

			free(vectorLooper);
			free(vector1);
			free(vector2);
		}

		// Get neighbor cells
		inPD->GetPointCells(l_looper, neighborCellID);
		//cout << "Number of cell for pt " << l_looper << ": " << neighborCellID->GetNumberOfIds() << " " << endl; //DEBUG
		// if it only has one cell
		if (neighborCellID->GetNumberOfIds() == 1) {
			// attemps to connect back to the loop
			std::cerr << "Warning! Your input is not a loop, attempting to connect gap at ptID="<< l_looper << "." << endl;
			
			// Get other point of the cell
			vtkIdType l_prevPtID;
			inPD->GetCellPoints(l_looper, neighborPtID);
			if (neighborPtID->GetId(0) != l_looper) {
				l_prevPtID = neighborPtID->GetId(0);
			} 
			else {
				l_prevPtID = neighborPtID->GetId(1);
			}

			vtkSmartPointer<vtkKdTree> l_kd = vtkSmartPointer<vtkKdTree>::New();
			l_kd->BuildLocatorFromPoints(inPD);
			l_kd->FindClosestNPoints(3, inPD->GetPoint(l_looper), neighborPtID);
			// Get points of neighbor cells
			vtkIdType l_tempLink[2];
			l_tempLink[0] = l_looper;
			for (int i = 0; i < 3; i++)
			{
				if (neighborPtID->GetId(i) != l_looper && neighborPtID->GetId(i) != l_prevPtID) {
					l_looper = neighborPtID->GetId(i);
					tmpPts->InsertNextPoint(inPD->GetPoint(l_looper));
				}
			}
			
			l_tempLink[1] = l_looper;
			l_prevID = inPD->InsertNextLinkedCell(VTK_LINE, 2, l_tempLink);
		}
		else {
			if (neighborCellID->GetId(0) != l_prevID) {
				l_nextCell = neighborCellID->GetId(0);
				l_prevID = l_nextCell;
			}
			else {
				l_nextCell = neighborCellID->GetId(1);
				l_prevID = l_nextCell;
			}
			// Get points of neighbor cells
			inPD->GetCellPoints(l_nextCell, neighborPtID);
			//cout << "Number of pt for cell " << l_nextCell << ": " << neighborPtID->GetNumberOfIds() << "\n"; //DEBUG
			if (neighborPtID->GetId(0) != l_looper) {
				l_looper = neighborPtID->GetId(0);
				tmpPts->InsertNextPoint(inPD->GetPoint(l_looper));
			}
			else
			{
				l_looper = neighborPtID->GetId(1);
				tmpPts->InsertNextPoint(inPD->GetPoint(l_looper));
			}
		}
		
		// Push the line into the new polydata
		vtkSmartPointer<vtkLine> l_line = vtkSmartPointer<vtkLine>::New();
		l_line->GetPointIds()->SetId(0, tmpPts->GetNumberOfPoints() - 2);
		l_line->GetPointIds()->SetId(1, tmpPts->GetNumberOfPoints() - 1);
		tmpCellArr->InsertNextCell(l_line);

		neighborPtID->Delete();
		neighborCellID->Delete();

		if (l_looper == startID) {
			// Break if reaching the end
			break;
		}
	}
	vtkSmartPointer<vtkLine> tmpline = vtkSmartPointer<vtkLine>::New();
	tmpline->GetPointIds()->SetId(0, tmpPts->GetNumberOfPoints() - 1);
	tmpline->GetPointIds()->SetId(1, 0);
	tmpCellArr->InsertNextCell(tmpline);

	tmpHolder->SetPoints(tmpPts);
	tmpHolder->SetLines(tmpCellArr);

	// Copy result to inPD
	inPD->DeepCopy(tmpHolder);
}

