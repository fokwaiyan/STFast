#ifndef __NORMALOFFSET_H_INCLUDED
#define _NORMALOFFSET_H_INCLUDED

#include <vtkSTLReader.h>
#include <vtkPolyData.h>
#include <vtkPolyDataNormals.h>
#include <vtkPoints.h>
#include <vtkPointData.h>
#include <vtkCellArray.h>
#include <vtkCellData.h>

#include <vtkDoubleArray.h>
#include <vtkIntArray.h>
#include <vtkTriangle.h>
#include <vtkMath.h>

#include <vtkSTLWriter.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkOBJExporter.h>


class NormalOffset {
public:
	void SetInputData(vtkPolyData*);
	void SetOffsetFactor(double);
	void SetNumberOfMeanFilter(int);
	void SetNaiveMode(bool);
	void SetAggressiveMode(bool);
	void SetSilentMode(bool);
	void SetMeshQualityControl(bool);
	void SetMaxNumberOfIteration(int);
	void SetAngleTolerance(double);
	void SetProgressReportMode(bool);
	void Update();
	vtkPolyData* GetOutPut();

private:
	double Extrusionfactor = 2;					// extrusionfactor = x means an offset of x mm will be performed, and can be used in an extrusion task.
	int Num_mean_filter = 20;					// Number of mean filter applied to the normal of the mesh before performing fix or extrusion.
	bool Naivemode = 0;							// To turn on/off repairing of the mesh. Recommend: off.
	bool Aggressivemode = 1;					// To control how aggressive in flattening a problematic local patch of the mesh. Recommend: on.
	bool Silentmode = 1;						// To suppress output or not.
	bool Meshqualitycontrol = 0;				// To control whether splitting triangles with "large" area.
	int Max_num_fix = 50;						// Max number of fix will be performed on a mesh. Original = 600
	double Angular_change_tolerance = 50;		// Max angular change of each cell normal before/after the extrusion, setting it low can prevent local singularity formed by multiple cells but will hugely increase computational time.
												// Suppose a local singularity formed by n cell is to be prevented, then angular_change_tolerance<=(180-max_feature_angle)/n is required.
												// Normally take value (degree) between 0 to 90. 
	bool Progressreport = 0;					// Set to report progress for every 10 % of cell completed. Not accurate.
	
	vtkPolyData* Inputpd;
	vtkSmartPointer<vtkPolyData> Outputpd;
};

#endif