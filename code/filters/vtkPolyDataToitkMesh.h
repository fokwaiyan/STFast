#ifndef __vtkPolyDataToitkMesh_h__
#define __vtkPolyDataToitkMesh_h__

#include "vtkPoints.h"
#include "vtkCellArray.h"
#include "vtkPolyData.h"
#include "itkDefaultDynamicMeshTraits.h"
#include "itkMesh.h"
#include "itkTriangleCell.h"
#include "AbstractFilter.h"

class vtkPolyDataToitkMesh : AbstractFilter
{
	Q_OBJECT
 public:
	 //Constructor/Destructor
	vtkPolyDataToitkMesh(QObject* parent = NULL);
	~vtkPolyDataToitkMesh();


	typedef itk::DefaultDynamicMeshTraits<float, 3, 3, float, float> TriangleMeshTraits;
	typedef itk::Mesh<float,3> TriangleMeshType;

	//Setter
	void SetInput( vtkPolyData * polydata);

	//Getter
	TriangleMeshType::Pointer GetOutput();
  
public slots:
	virtual int Update();

private:
	vtkPolyData*				 m_input;
	TriangleMeshType::Pointer  m_output;
};

#endif
