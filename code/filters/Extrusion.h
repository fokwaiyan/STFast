#ifndef __NORMALEXTRUSION_H_INCLUDED
#define _NORMALEXTRUSION_H_INCLUDED

#include <vtkSmartPointer.h>
#include <vtkPolyData.h>


class Extrusion {
public:
	void SetInputData(vtkPolyData*);
	void SetInputCenterLine(vtkPolyData*);
	void SetExtrusionFactor(double);
	void SetNaiveMode(bool);
	void SetNumberOfSmooth(int);
	void SetFixMode(bool);
	void Update();
	vtkPolyData* GetOutput();

private:
	double Extrusionfactor;			//extrusionfactor=x => thickness of result = x mm
	bool Naivemode = 0;				//naivemode can only be used on "really good" mesh,require drillholecenterline.vtp if it is turned off.
	int Numberofsmoothing = 50;		//This,  number of mean filter applied to cell normal, should be increased if the resulting mesh seems not smooth enough.
	bool Fixmode = 0;				//More carefully extrude the mesh. Only use it when the output has self intersection.Recommend : Off.
	
	vtkPolyData* Linepd;
	vtkPolyData* Inputpd;
	vtkSmartPointer<vtkPolyData> Outputpd;

};
float signum(double);



#endif