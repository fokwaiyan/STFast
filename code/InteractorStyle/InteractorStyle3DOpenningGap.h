#ifndef INTERACTOR_STYLE_OPENNING_GAP_H
#define INTERACTOR_STYLE_OPENNING_GAP_H

#include "vtkActor.h"
#include "vtkObjectFactory.h"
#include "vtkInteractorStyleTrackballCamera.h"
#include "vtkOrientedGlyphContourRepresentation.h"
#include "vtkContourWidget.h"
#include "vtkSmartPointer.h"
#include "vtkTubeFilter.h"


#include "vtkIdTypeArray.h"
#include "vtkSphereSource.h"
#include "vtkPolyDataMapper.h"
#include "vtkActor.h"
#include "vtkIdTypeArray.h"

#include <vtkPlaneWidget.h>

using namespace std;

class MyContourWidget;

class InteractorStyle3DOpenningGap : public vtkInteractorStyleTrackballCamera
#ifdef ABSTRACT_INTERACTOR_3D_H
	, public AbstractInteractor3D // inherit this if the .h file is included
#endif
{
public:
	static InteractorStyle3DOpenningGap* New();
	vtkTypeMacro(InteractorStyle3DOpenningGap, vtkInteractorStyleTrackballCamera);

	/* Defines which polydata to clip */
	vtkSetMacro(TargetActor, vtkActor*);
	vtkGetMacro(TargetActor, vtkActor*);

	/*	Defines whether the larger segment after cutting
	*	is preserved or deleted. If true, the larger piece
	*	is deleted. Default to be false	
	*/
	vtkSetMacro(DeleteLargerSegment, bool);
	vtkGetMacro(DeleteLargerSegment, bool);
	vtkBooleanMacro(DeleteLargerSegment, bool);

	/*	Defines the number of sides of the tube used to clip
	*	the polydata.
	*/
	vtkSetMacro(ClipResolution, int);
	vtkGetMacro(ClipResolution, int);

	/*  Defines the maximum region to be eaten away */
	vtkSetMacro(ClipTolerence, double);
	vtkGetMacro(ClipTolerence, double);


	int GetWidgetState();
	void SetEnableWidget(int);

	virtual void OnLeftButtonDown() override;

	virtual void OnLeftButtonUp() override;

	virtual void OnRightButtonDown() override;

	virtual void OnRightButtonUp() override;

	virtual void SetPlaneWidget(vtkSmartPointer<vtkPlaneWidget> planeWidget);

protected:
	InteractorStyle3DOpenningGap();
	~InteractorStyle3DOpenningGap();
		
	virtual void OnKeyPress();

	virtual void ToggleWidget();
	virtual void ToggleTubeWidget();
	virtual void FindClosestPoint();
	virtual void DropSeedEnabled();

	virtual void GetExtrusionVector();
	virtual void ExtrudeAlongVector();
	virtual void ClearWidgetRep();
	virtual void BuildRidges();
	virtual void SaveFile();

private:
	void Initialize();
	void ClipAlongContourLine();
	void ClipAlongContourLine(int n);
	void PushCells();
	void GetExtrudeLine();
	void ExtrudeCellByPolyData();
	void ExtrudeCellByLine();
	virtual void MarkCellInsidePolygon(vtkSmartPointer<vtkPolyData> inPD, vtkSmartPointer<vtkPolyData> clipperPD, vtkIdList *outList);
	virtual void GetCellLocation(vtkSmartPointer<vtkPolyData> inPD, vtkCell* inCell, double *outPt);
	//virtual void PushCellsAlongVector(vtkSmartPointer<vtkPolyData> inPD, vtkSmartPointer<vtkIdList> cellId, vtkSmartPointer<vtkPolyData> outPD);
	virtual void PushCellsAlongVector(vtkSmartPointer<vtkPolyData> inPD, vtkSmartPointer<vtkPolyData> outPD);

	// Objects to be set
	vtkActor*		TargetActor;

	// Parametersfind
	bool	DeleteLargerSegment;
	bool    DropSeedState;
	int		ClipResolution;
	double	ClipTolerence;
	

	// Self use objects
	vtkSmartPointer<vtkPolyData>							m_original;
	vtkSmartPointer<vtkPolyData>							m_output;
	vtkSmartPointer<vtkContourWidget>						m_contourWidget;
	vtkSmartPointer<vtkOrientedGlyphContourRepresentation>	m_contourRep;
	vtkSmartPointer<vtkActor>								m_tubeActor;
	vtkSmartPointer<vtkTubeFilter>							m_tubeFilter;
	vtkSmartPointer<vtkSphereSource>						m_sphere;
	vtkSmartPointer<vtkPolyDataMapper>						m_sphMapper;
	vtkSmartPointer<vtkActor>								m_sphactor;
	vtkSmartPointer<vtkIdTypeArray>							m_ids;
	
	//for extrusion
	double*													m_extrusionVector ;
	vtkSmartPointer<vtkPlaneWidget>							m_planeWidget;

	//ridge extrusion
	vtkSmartPointer<vtkPolyData>							m_ridges;
	vtkSmartPointer<vtkPolyData>							m_bottomline;
	vtkSmartPointer<vtkActor>								m_ridgeActor;	
	int														m_wholeScaleFactor;
	int														m_ridgeScaleFactor;
};

#endif // !INTERACTOR_STYLE_FREE_CLIPPER_H
