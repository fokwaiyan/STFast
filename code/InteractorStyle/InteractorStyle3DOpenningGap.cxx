#include "vtkPolygonalSurfaceContourLineInterpolator.h"
#include "vtkBezierContourLineInterpolator.h"
#include "vtkPolygonalSurfacePointPlacer.h"
#include "vtkProperty.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkPolyDataMapper.h"
#include "vtkRenderer.h"
#include "vtkRendererCollection.h"
#include "vtkRenderWindow.h"
#include "vtkWidgetEventTranslator.h"

#include "InteractorStyle3DOpenningGap.h"
#include "vtkTubeFilter.h"
#include "vtkCleanPolyData.h"
#include "vtkCommand.h"
#include "vtkImplicitPolyDataDistance.h"
#include "vtkClipPolyData.h"


#include "vtkIdList.h"
#include "vtkXMLDataSetWriter.h"
#include "vtkPoints.h"
#include <vtkFeatureEdges.h>
#include "vtkPolyDataConnectivityFilter.h"
#include "vtkPolyDataMapper.h"
#include "CutAlongPolyLineFilter.h"
#include "CutAlongPolyLineFilter2.h"
#include "vtkKdTree.h"
#include "vtkActor.h"

#include "vtkCellPicker.h"
#include "vtkSphereSource.h"
#include "vtkSelectionNode.h"
#include "vtkSelection.h"
#include "vtkGeometryFilter.h"
#include "vtkExtractSelection.h"
#include "vtkUnstructuredGrid.h"
#include "vtkSTLWriter.h"

//extrusion
#include <vtkLinearExtrusionFilter.h>
#include <vtkPolyDataNormals.h>
#include <vtkAppendPolyData.h>
#include <vtkIntersectionPolyDataFilter.h>
#include <vtkRibbonFilter.h>
#include <vtkTriangleFilter.h>
#include <vtkSelectEnclosedPoints.h>
#include <vtkMath.h>
#include <vtkCell.h>
#include <vtkPointData.h>
#include <vtkFillHolesFilter.h>
#include <vtkXMLPolyDataWriter.h>
#include <vtkTriangle.h>

#include <vtkPolygon.h>
#include <vtkCellArray.h>
#include <vtkThreshold.h>
#include <vtkPolyLine.h>
#include <vtkSurfaceEncloser.h>
#include <vtkPolyLineSortIDFilter.h>

vtkStandardNewMacro(InteractorStyle3DOpenningGap);

void InteractorStyle3DOpenningGap::OnKeyPress()
{
	std::string key = this->Interactor->GetKeySym();
	std::cout << key <<endl;
	
	if (key == "Escape") {
	}
	
	else if (key == "a")	{
		//this->DropSeedEnabled();
		return;
	}
	else if (key == "Return") {
		this->ClipAlongContourLine();
	}

	// Undo one cut
	else if (key == "z") {
		this->TargetActor->GetMapper()->GetInput()->DeepCopy(this->m_original);
	}
	else if (key == "D") {
		this->ToggleWidget();
	}
	else if (key == "t") {
		this->ToggleTubeWidget();
	}

	else if (key == "space") {
		this->FindClosestPoint();
	}
	else if (key == "v") {
		this->GetExtrusionVector();
	}
	else if (key == "B") {
		if (!m_extrusionVector) {
		}
		else{
			std::cout << "Extrusion vector:" << m_extrusionVector[0] << "," << m_extrusionVector[1] << "," << m_extrusionVector[2] << endl;
			this->ExtrudeAlongVector();
		}
	}
	else if (key == "p"){
		if (this->m_planeWidget->GetEnabled()){
			this->m_planeWidget->EnabledOff();
		}
		else
			this->m_planeWidget->EnabledOn();
	}
	else if (key == "C")
	{
		this->ClearWidgetRep();
	}
	else if (key == "R")
	{
		this->PushCells();
		//this->ClipAlongContourLine(1);
	}
	else if (key == "A")
	{
		//this->BuildRidges();
	}
	else if (key == "S")
	{
		this->SaveFile();
	}
	Superclass::OnKeyPress();
}

void InteractorStyle3DOpenningGap::ToggleWidget()
{
	if (this->m_contourWidget == NULL || this->m_contourRep == NULL)
		this->Initialize();

	vtkDebugMacro(<< "Toggleing Widget! Widget state: " << this->m_contourWidget->GetEnabled());

	if (this->m_contourWidget->GetEnabled())
	{
		//this->TargetActor->GetProperty()->SetOpacity(1);
		this->m_contourWidget->EnabledOff();
	}
	else
	{
		//this->TargetActor->GetProperty()->SetOpacity(0.8);
		this->m_contourWidget->EnabledOn();
	}
}

void InteractorStyle3DOpenningGap::ToggleTubeWidget()
{
	if (this->CurrentRenderer->HasViewProp(this->m_tubeActor))
	{
		this->CurrentRenderer->RemoveActor(this->m_tubeActor);
	}
	else {
		if (this->m_contourRep->GetContourRepresentationAsPolyData()->GetNumberOfPoints() == 0)
			return;

		vtkSmartPointer<vtkCleanPolyData> cleaner = vtkSmartPointer<vtkCleanPolyData>::New();
		cleaner->SetInputData(this->m_contourRep->GetContourRepresentationAsPolyData());
		cleaner->SetTolerance(0.01);
		cleaner->Update();

		this->m_tubeFilter->SetInputConnection(cleaner->GetOutputPort());
		//this->m_tubeFilter->SetRadius(this->ClipTolerence);
		this->m_tubeFilter->SetRadius(this->ClipResolution/2.0);
		this->m_tubeFilter->SetNumberOfSides(8);
		this->m_tubeFilter->Update();

		vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
		mapper->SetInputConnection(this->m_tubeFilter->GetOutputPort());
		mapper->Update();

		this->m_tubeActor->SetMapper(mapper);
		this->m_tubeActor->GetProperty()->SetColor(1, 1, 0);
		this->m_tubeActor->GetProperty()->SetOpacity(0.8);
		this->CurrentRenderer->AddActor(this->m_tubeActor);
	}
	this->Interactor->Render();
}

InteractorStyle3DOpenningGap::InteractorStyle3DOpenningGap()
{
	this->ClipResolution = 4;
	this->ClipTolerence = 0.5;
	this->DeleteLargerSegment = false;
	this->DropSeedState = false;
	this->m_contourWidget = NULL;
	this->m_contourRep = NULL;
	this->TargetActor = NULL;
	this->m_tubeFilter = NULL;
	this->m_original = vtkSmartPointer<vtkPolyData>::New();
	this->m_tubeActor = vtkSmartPointer<vtkActor>::New();
	this->m_output = vtkSmartPointer<vtkPolyData>::New();
	this->m_sphere = vtkSmartPointer<vtkSphereSource>::New();
	this->m_sphMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
	this->m_sphactor = vtkSmartPointer<vtkActor>::New();
	this->m_ids = vtkSmartPointer<vtkIdTypeArray>::New();

	this->m_extrusionVector = NULL;
	this->m_ridges = vtkSmartPointer<vtkPolyData>::New();
	this->m_bottomline = vtkSmartPointer<vtkPolyData>::New();
	this->m_ridgeActor = vtkSmartPointer<vtkActor>::New();
	this->m_ridgeScaleFactor = 5;
	this->m_wholeScaleFactor = 5;

}

InteractorStyle3DOpenningGap::~InteractorStyle3DOpenningGap()
{
	if (this->CurrentRenderer->HasViewProp(this->m_tubeActor))
		this->CurrentRenderer->RemoveActor(this->m_tubeActor);

	if (this->m_contourWidget != NULL)
	{
		this->m_contourWidget->EnabledOff();
		this->m_contourRep->Delete();
	}
	if (this->CurrentRenderer->HasViewProp(this->m_sphactor))
		this->CurrentRenderer->RemoveActor(this->m_sphactor);

}

int InteractorStyle3DOpenningGap::GetWidgetState()
{
	return this->m_contourWidget->GetEnabled();
}

void InteractorStyle3DOpenningGap::SetEnableWidget(int enable)
{
	if (this->m_contourWidget == NULL)
		return;

	if (enable)
	{
		this->m_contourWidget->EnabledOn();
	}
	else {
		this->m_contourWidget->EnabledOff();
	}
	this->Interactor->Render();
}

void InteractorStyle3DOpenningGap::OnLeftButtonDown()
{
	if (DropSeedState)
	{
		int* pos = this->Interactor->GetEventPosition();

		vtkSmartPointer<vtkCellPicker> picker = vtkSmartPointer<vtkCellPicker>::New();
		picker->SetTolerance(0.005);

		picker->Pick(pos[0], pos[1], 0, this->CurrentRenderer);
		double* worldPosition = picker->GetPickPosition();
		std::cout << "Cell id is: " << picker->GetCellId() << std::endl;

		// Forward events
		Superclass::OnLeftButtonUp();

		if (picker->GetCellId() != -1)
		{
			if (m_sphactor != NULL)
				this->CurrentRenderer->RemoveActor(m_sphactor);

			m_sphere->SetCenter(worldPosition);
			m_sphere->SetRadius(10.0);
			m_sphere->SetPhiResolution(100);
			m_sphere->SetThetaResolution(100);

			m_sphMapper->SetInputData(m_sphere->GetOutput());
			m_sphactor->SetMapper(m_sphMapper);
			m_sphactor->GetProperty()->SetColor(1, 0, 0);
			
			m_ids->SetNumberOfComponents(1);
			m_ids->InsertNextValue(picker->GetCellId());

			this->CurrentRenderer->AddActor(m_sphactor);
			this->CurrentRenderer->ResetCamera();
		}
	}
	else
		Superclass::OnLeftButtonDown();

}

void InteractorStyle3DOpenningGap::OnLeftButtonUp()
{
	Superclass::OnLeftButtonUp();
}

void InteractorStyle3DOpenningGap::OnRightButtonDown()
{
	Superclass::OnRightButtonDown();
	if (this->m_contourWidget != NULL)
	{
		this->Interactor->Render();
	}
	this->m_contourWidget->SetWidgetState(vtkContourWidget::Manipulate);
}

void InteractorStyle3DOpenningGap::OnRightButtonUp()
{
	Superclass::OnRightButtonUp();
}

void InteractorStyle3DOpenningGap::SetPlaneWidget(vtkSmartPointer<vtkPlaneWidget> planeWidget)
{
	m_planeWidget = planeWidget;
}

void InteractorStyle3DOpenningGap::Initialize()
{
	// Create objects needed
	if (this->m_contourWidget == NULL)
	{
		this->m_contourWidget = vtkSmartPointer<vtkContourWidget>::New();
	}
	if (this->m_contourRep == NULL)
		this->m_contourRep = vtkSmartPointer<vtkOrientedGlyphContourRepresentation>::New();
	if (this->m_tubeFilter == NULL)
		this->m_tubeFilter = vtkSmartPointer<vtkTubeFilter>::New();

	// Error check
	if (this->TargetActor == NULL)
	{
		vtkWarningMacro(<< "No TargetActor specified!");
		return;
	}

	vtkSmartPointer<vtkPolygonalSurfacePointPlacer> pointPlacer =
		vtkSmartPointer<vtkPolygonalSurfacePointPlacer>::New();
	pointPlacer->AddProp(this->TargetActor);

	this->m_contourRep->SetLineInterpolator(vtkSmartPointer<vtkBezierContourLineInterpolator>::New());
	this->m_contourRep->SetHandleSize(3.0);
	this->m_contourRep->GetLinesProperty()->SetColor(1, 0, 0);
	this->m_contourRep->GetLinesProperty()->SetLineWidth(5.0);
	this->m_contourRep->SetPointPlacer(pointPlacer);

	this->m_contourWidget->SetInteractor(this->Interactor);
	this->m_contourWidget->SetRepresentation(this->m_contourRep);
	this->m_contourWidget->GetEventTranslator()->RemoveTranslation(vtkCommand::RightButtonPressEvent); // Remove last point adding
}

void InteractorStyle3DOpenningGap::ClipAlongContourLine()
{
	if (this->TargetActor == NULL)
		return;

	// copy input to original
	vtkPolyData* input = vtkPolyData::SafeDownCast(this->TargetActor->GetMapper()->GetInput());
	this->m_original->DeepCopy(input);

	vtkSmartPointer<vtkCleanPolyData> cleaner = vtkSmartPointer<vtkCleanPolyData>::New();
	cleaner->SetInputData(this->m_contourRep->GetContourRepresentationAsPolyData());
	cleaner->SetTolerance(0.01);
	cleaner->Update();

	vtkSmartPointer<vtkTubeFilter> tubefilter = vtkSmartPointer<vtkTubeFilter>::New();
	tubefilter->SetInputData(cleaner->GetOutput());
	//tubefilter->SetRadius(this->ClipTolerence);
	tubefilter->SetRadius(this->ClipResolution / 2.);
	tubefilter->SetCapping(0);
	tubefilter->SetNumberOfSides(8);
	tubefilter->Update();

	vtkSmartPointer<vtkImplicitPolyDataDistance> clipFunc = vtkSmartPointer<vtkImplicitPolyDataDistance>::New();
	clipFunc->SetInput(tubefilter->GetOutput());

	vtkSmartPointer<vtkClipPolyData> clipper = vtkSmartPointer<vtkClipPolyData>::New();
	clipper->SetInputData(input);
	clipper->SetClipFunction(clipFunc);
	clipper->Update();
	
	std::cout << "Before conn: " << endl;
	clipper->GetOutput()->Print(std::cout);

	vtkSmartPointer<vtkPolyDataConnectivityFilter> conn = vtkSmartPointer<vtkPolyDataConnectivityFilter>::New();
	conn->SetInputData(clipper->GetOutput());
	conn->SetExtractionModeToSpecifiedRegions();
	conn->AddSpecifiedRegion(1);
	conn->Update();
	
	// Check result before copying it to input
	if (conn->GetOutput() == NULL)
		return;
	if (conn->GetOutput()->GetNumberOfPoints() == 0)
		return;
	std::cout << "After conn: " << endl;
	conn->GetOutput()->Print(std::cout);

	input->DeepCopy(conn->GetOutput());
	m_original->DeepCopy(conn->GetOutput());

	vtkSmartPointer<vtkXMLPolyDataWriter> writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
	writer->SetInputData(input);
	writer->SetFileName("D:/STFast/Test/ContourSurfaceExtract/datas/first-plate.vtp");
	writer->Write();
	writer->Update();
}

void InteractorStyle3DOpenningGap::ClipAlongContourLine(int n)
{
	if (this->TargetActor == NULL)
		return;

	// copy input to original
	vtkPolyData* input = vtkPolyData::SafeDownCast(this->TargetActor->GetMapper()->GetInput());
	//m_ridges->DeepCopy(input);

	vtkSmartPointer<vtkCleanPolyData> cleaner = vtkSmartPointer<vtkCleanPolyData>::New();
	cleaner->SetInputData(this->m_contourRep->GetContourRepresentationAsPolyData());
	cleaner->SetTolerance(0.01);
	cleaner->Update();

	vtkSmartPointer<vtkTubeFilter> tubefilter = vtkSmartPointer<vtkTubeFilter>::New();
	tubefilter->SetInputData(cleaner->GetOutput());
	//tubefilter->SetRadius(this->ClipTolerence);
	tubefilter->SetRadius(this->ClipResolution / 2.);
	tubefilter->SetCapping(0);
	tubefilter->SetNumberOfSides(8);
	tubefilter->Update();

	vtkSmartPointer<vtkImplicitPolyDataDistance> clipFunc = vtkSmartPointer<vtkImplicitPolyDataDistance>::New();
	clipFunc->SetInput(tubefilter->GetOutput());

	vtkSmartPointer<vtkClipPolyData> clipper = vtkSmartPointer<vtkClipPolyData>::New();
	clipper->SetInputData(input);
	clipper->SetClipFunction(clipFunc);
	clipper->Update();

	std::cout << "Before conn: " << endl;
	clipper->GetOutput()->Print(std::cout);
	//inside - specific region:1 
	vtkSmartPointer<vtkPolyDataConnectivityFilter> conn = vtkSmartPointer<vtkPolyDataConnectivityFilter>::New();
	conn->SetInputData(clipper->GetOutput());
	conn->SetExtractionModeToSpecifiedRegions();
	conn->AddSpecifiedRegion(1);
	conn->Update();

	vtkIdList* idList;
	vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();

	// Check result before copying it to input
	std::cout << "After conn: " << conn->GetNumberOfExtractedRegions()<< endl;

	//outside - specific region:2
	vtkSmartPointer<vtkPolyDataConnectivityFilter> conn2 = vtkSmartPointer<vtkPolyDataConnectivityFilter>::New();
	conn2->SetInputData(clipper->GetOutput());
	conn2->SetExtractionModeToSpecifiedRegions();
	conn2->AddSpecifiedRegion(0);
	conn2->Update();

	m_ridges->DeepCopy(conn->GetOutput());
	m_output->DeepCopy(conn2->GetOutput());

	vtkSmartPointer<vtkPolyDataMapper> ridgeMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
	ridgeMapper->SetInputData(conn->GetOutput());
	m_ridgeActor->SetMapper(ridgeMapper);
	m_ridgeActor->GetProperty()->SetColor(0, 1, 1);
	this->CurrentRenderer->AddActor(m_ridgeActor);

	vtkSmartPointer<vtkXMLPolyDataWriter> ridgeswriter = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
	ridgeswriter->SetInputData(conn->GetOutput());
	ridgeswriter->SetFileName("D:/STFast/Test/ContourSurfaceExtract/datas/ridges-conn.vtp");
	ridgeswriter->Write();
	ridgeswriter->Update();
	vtkSmartPointer<vtkXMLPolyDataWriter> outside = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
	outside->SetInputData(conn2->GetOutput());
	outside->SetFileName("D:/STFast/Test/ContourSurfaceExtract/datas/outside-conn.vtp");
	outside->Write();
	outside->Update();
	
}

void InteractorStyle3DOpenningGap::PushCells()
{
	if (!this->TargetActor){
		return;
	}
	vtkPolyData* input = vtkPolyData::SafeDownCast(this->TargetActor->GetMapper()->GetInput());
	std::cout << "points of input:" << input->GetNumberOfPoints() << endl;
	std::cout << "Points of contours: "<< m_contourRep->GetContourRepresentationAsPolyData()->GetNumberOfPoints()<<endl;
	
	vtkSmartPointer<vtkXMLPolyDataWriter> writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
	writer->SetInputData(m_contourRep->GetContourRepresentationAsPolyData());
	writer->SetFileName("D:/STFast/Test/ContourSurfaceExtract/datas/contourRep.vtp");
	writer->Write();
	writer->Update();

	this->ClipAlongContourLine(1);
	this->GetExtrudeLine();
	this->ExtrudeCellByPolyData();
	//this->ExtrudeCellByLine();
	//this->defineIntersectPolygon();

	//if (!this->m_ridgeActor) {
	//	return;
	//}
	//vtkPolyData* input2 = vtkPolyData::SafeDownCast(this->m_ridgeActor->GetMapper()->GetInput());
	//input2->Print(std::cout);

	////find intersection as polydata
	//vtkSmartPointer<vtkRibbonFilter> ribbon = vtkSmartPointer<vtkRibbonFilter> ::New();
	//ribbon->SetInputData(m_contourRep->GetContourRepresentationAsPolyData());
	//ribbon->SetDefaultNormal(m_extrusionVector);
	//ribbon->SetWidth(2);
	//ribbon->Update();
	//vtkSmartPointer<vtkTriangleFilter> triangle = vtkSmartPointer<vtkTriangleFilter>::New();
	//triangle->SetInputData(ribbon->GetOutput());
	//triangle->Update();

	//vtkSmartPointer<vtkIntersectionPolyDataFilter> intersect = vtkSmartPointer<vtkIntersectionPolyDataFilter>::New();
	//intersect->SetInputData(0, input);
	//intersect->SetInputData(1, input2);
	//intersect->GetSplitFirstOutput();
	//intersect->Update();

	//vtkSmartPointer<vtkCleanPolyData> intersectClean = vtkSmartPointer<vtkCleanPolyData>::New();
	//intersectClean->SetInputData(intersect->GetOutput());
	//intersectClean->Update();
	//std::cout << "Intersect: " << intersectClean->GetOutput()->GetNumberOfCells() << endl;

	//vtkSmartPointer<vtkFillHolesFilter> holefiller = vtkSmartPointer<vtkFillHolesFilter>::New();
	//holefiller->SetInputData(ribbon->GetOutput());
	//holefiller->SetHoleSize(10000.0);
	//holefiller->Update();

	//vtkSmartPointer<vtkXMLPolyDataWriter> writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
	//writer->SetInputData(m_ridges);
	//writer->SetFileName("D:/STFast/Project/data/ridges.vtp");
	//writer->Write();
	//writer->Update();
	
	////extrude the points of polydata by the extrusion vector
	//vtkSmartPointer<vtkIdList> outlist = vtkSmartPointer<vtkIdList>::New();
	//this->MarkCellInsidePolygon(input, m_ridges, outlist);
	//outlist->Print(std::cout);

	//create closed surface for the two polydata
	//vtkSmartPointer<vtkPolyData> output = vtkSmartPointer<vtkPolyData>::New();
	//this->PushCellsAlongVector(input, outlist, output);
	//if (m_ridges->GetNumberOfCells() ==0) {
	//	return;
	//}
	//vtkSmartPointer<vtkPolyData> outPD = vtkSmartPointer<vtkPolyData>::New();
	//this->PushCellsAlongVector(m_ridges, outPD);
	////
	//vtkSmartPointer<vtkAppendPolyData> app = vtkSmartPointer<vtkAppendPolyData>::New();
	//app->SetInputData(input);
	//app->SetInputData(m_ridges);
	//app->Update();
	//
	//vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
	//mapper->SetInputData(m_ridges);
	//this->m_ridgeActor->SetMapper(mapper);
	//m_ridgeActor->GetProperty()->SetColor(0, 1, 1);
	//this->CurrentRenderer->AddActor(m_ridgeActor);

}
void InteractorStyle3DOpenningGap::GetExtrudeLine()
{
	//get feature edge
	vtkSmartPointer<vtkFeatureEdges> featureEdge = vtkSmartPointer<vtkFeatureEdges>::New();
	featureEdge->SetInputData(m_output);
	featureEdge->SetBoundaryEdges(true);
	featureEdge->Update();
	
	//createTubefilter with m_contourRep and then clipinsde out
	vtkSmartPointer<vtkTubeFilter> tubefilter = vtkSmartPointer<vtkTubeFilter>::New();
	tubefilter->SetInputData(m_contourRep->GetContourRepresentationAsPolyData());
	tubefilter->SetRadius(this->ClipResolution);
	tubefilter->SetCapping(0);
	tubefilter->SetNumberOfSides(8);
	tubefilter->Update();

	vtkSmartPointer<vtkImplicitPolyDataDistance> clipFunc = vtkSmartPointer<vtkImplicitPolyDataDistance>::New();
	clipFunc->SetInput(tubefilter->GetOutput());

	vtkSmartPointer<vtkClipPolyData> clipper = vtkSmartPointer<vtkClipPolyData>::New();
	clipper->SetInputData(featureEdge->GetOutput());
	clipper->SetInsideOut(true);
	clipper->SetClipFunction(clipFunc);
	clipper->Update();

	vtkPolyLineSortDIFilter* sorter = vtkPolyLineSortDIFilter::New();
	vtkSmartPointer<vtkPolyData> sortedPD = vtkSmartPointer<vtkPolyData>::New();
	std::vector<vtkPolyData*> sortedPd;
		//sort the IDs in polydata 
		sorter->SetInputData(clipper->GetOutput());
		sorter->Update();
		sortedPD->DeepCopy(sorter->GetOutput());
		
	//m_ridges->DeepCopy(sortedPD);
	m_bottomline->DeepCopy(clipper->GetOutput());

	vtkSmartPointer<vtkXMLPolyDataWriter> writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
	writer->SetInputData(clipper->GetOutput());
	writer->SetFileName("D:/STFast/Test/ContourSurfaceExtract/datas/newpolydata.vtp");
	writer->Write();
	writer->Update();

}

void InteractorStyle3DOpenningGap::ExtrudeCellByPolyData()
{
	if (!TargetActor) {
		return;
	}

	vtkPolyData* input = vtkPolyData::SafeDownCast(this->TargetActor->GetMapper()->GetInput());

	vtkSmartPointer<vtkPolyData> polydata = vtkSmartPointer<vtkPolyData>::New();
	polydata->DeepCopy(m_ridges);
	
	vtkSmartPointer<vtkXMLPolyDataWriter> writer1 = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
	writer1->SetInputData(m_ridges);
	writer1->SetFileName("D:/STFast/Test/ContourSurfaceExtract/datas/m_ridges.vtp");
	writer1->Write();
	writer1->Update();

	vtkIdList* idList;
	vtkIdList* newIdList = NULL;
	vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
	vtkSmartPointer<vtkCellArray> triangles = vtkSmartPointer<vtkCellArray>::New();
	//loop for points, if point has neighbouring cells, then start getting from this pointId
	//and putting the consecutive points into the new list
	vtkIdType startSeed;

  for (int i = 0 ; i< polydata->GetNumberOfPoints(); i++)
  {
	  vtkSmartPointer<vtkIdList> cellIdList = vtkSmartPointer<vtkIdList>::New();
	  polydata->GetPointCells(i, cellIdList);
	  if (cellIdList->GetNumberOfIds()> 1){
		  //state the start point for push points from onwards
		  startSeed = i;
		  std::cout << "Start seed: " << startSeed << endl;
		  break;
	  }		  
  }

  //new list and then push 
  double m_extrusionVector[3];
  m_extrusionVector[0] = -0.819568;
  m_extrusionVector[1] = 0.414478;
  m_extrusionVector[2] = -0.395621;
  double m_ridgeScaleFactor = 5.0;

  for (int i = startSeed; i < polydata->GetNumberOfPoints(); i++)
  {
	  std::cout << "Push: " << i << endl;
	  double* newPoints;
	  newPoints = polydata->GetPoint(i);
	  std::cout << "New points ";
	  for (int j = 0; j < 3; j++) {
		  newPoints[j] = newPoints[j] + m_extrusionVector[j] * m_ridgeScaleFactor;
		  std::cout << j << ": " << newPoints[j] << " ";
	  }
	  std::cout << endl;
	  points->InsertNextPoint(newPoints);
	  //points->InsertNextPoint(polydata->GetPoint(i));		//this is for no pushing
  }

  for (int i = 0; i < polydata->GetNumberOfCells(); i++)
  {
	  vtkCell* cell = polydata->GetCell(i);
	  int ptId = 0;
	  idList = cell->GetPointIds();

	  std::cout << "id list: ";
	  vtkSmartPointer<vtkTriangle> tri = vtkSmartPointer<vtkTriangle>::New();
	  for (int j = 0; j < idList->GetNumberOfIds(); j++) {
		  vtkIdType newId;
		  newId = idList->GetId(j) - startSeed;
		  std::cout << "newID: " << newId << " ";
		  tri->GetPointIds()->SetId(j, newId);
	  }
	  triangles->InsertNextCell(tri);
	  std::cout << endl;
  }
  vtkSmartPointer<vtkPolyData> zero = vtkSmartPointer<vtkPolyData>::New();
  zero->SetPoints(points);
  zero->SetPolys(triangles);
  m_ridges->DeepCopy(zero);
  std::cout << "no of new Points: " << zero->GetNumberOfPoints() << endl;
  std::cout << "no. of new Cells: " << zero->GetNumberOfCells() << endl;

  vtkSmartPointer<vtkFeatureEdges> zeroPolyline = vtkSmartPointer<vtkFeatureEdges>::New();
  zeroPolyline->SetInputData(zero);
  zeroPolyline->SetBoundaryEdges(true);
  zeroPolyline->SetManifoldEdges(false);
  zeroPolyline->SetNonManifoldEdges(false);
  zeroPolyline->Update();

  vtkSmartPointer<vtkCleanPolyData> cleaner = vtkSmartPointer<vtkCleanPolyData>::New();
  cleaner->SetInputData(zero);
  cleaner->Update();

  vtkSmartPointer<vtkPolyData> face = vtkSmartPointer<vtkPolyData>::New();
  //surface encloser the two layers
  SurfaceEncloser* surfaceEncloser = new SurfaceEncloser;
  surfaceEncloser->SetInput(zeroPolyline->GetOutput(), m_bottomline);
  //surfaceEncloser->SetEdgesAreLoops(true);
  surfaceEncloser->Update();
  face->DeepCopy(surfaceEncloser->GetOutput());

  vtkSmartPointer<vtkAppendPolyData> app = vtkSmartPointer<vtkAppendPolyData>::New();
  app->AddInputData(face);
  app->AddInputData(m_output);
  app->AddInputData(zero);
  app->Update();

  vtkSmartPointer<vtkXMLPolyDataWriter> writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
  writer->SetInputData(face);
  writer->SetFileName("D:/STFast/Test/ContourSurfaceExtract/datas/face.vtp");
  writer->Write();
  writer->Update();

  input->DeepCopy(app->GetOutput());
}

void InteractorStyle3DOpenningGap::ExtrudeCellByLine()
{
	if (!TargetActor) {
		return;
	}

	vtkPolyData* input = vtkPolyData::SafeDownCast(this->TargetActor->GetMapper()->GetInput());

	vtkSmartPointer<vtkPolyData> polyline = vtkSmartPointer<vtkPolyData>::New();
	//polyline->DeepCopy(m_contourRep->GetContourRepresentationAsPolyData());
	polyline->DeepCopy(m_ridges);

	vtkSmartPointer<vtkPolygon> polygon = vtkSmartPointer<vtkPolygon>::New();
	vtkSmartPointer<vtkPolyLine> line = vtkSmartPointer<vtkPolyLine>::New();
	vtkSmartPointer<vtkCellArray> cellArray = vtkSmartPointer<vtkCellArray>::New();
	vtkSmartPointer<vtkPoints> newPoints = vtkSmartPointer<vtkPoints>::New();
	for (int i = 0; i < polyline->GetNumberOfPoints(); i++) {
		std::cout << "Push: " << i << endl;
		double* newPoint;
		newPoint = polyline->GetPoint(i);
		std::cout << "New points ";
		for (int j = 0; j < 3; j++) {
			newPoint[j] = newPoint[j] + m_extrusionVector[j] * m_ridgeScaleFactor;
			std::cout << j << ": " << newPoint[j] << " ";
		}
		std::cout << endl;
		newPoints->InsertNextPoint(newPoint);
		line->GetPointIds()->InsertNextId(newPoints->GetNumberOfPoints() - 1);
		//polygon->GetPointIds()->InsertNextId(newPoints->GetNumberOfPoints() - 1);
	}
	/*cellArray->InsertNextCell(polygon);*/
	cellArray->InsertNextCell(line);
	vtkSmartPointer<vtkPolyData> newPolyData = vtkSmartPointer<vtkPolyData>::New();
	newPolyData->SetPoints(newPoints);
	newPolyData->SetPolys(cellArray);


	vtkSmartPointer<vtkPolyData> face = vtkSmartPointer<vtkPolyData>::New();
	//surface encloser the two layers
	SurfaceEncloser* surfaceEncloser = new SurfaceEncloser;
	surfaceEncloser->SetInput(polyline, newPolyData);
	surfaceEncloser->SetEdgesAreLoops(true);
	surfaceEncloser->Update();
	face->DeepCopy(surfaceEncloser->GetOutput());

	vtkSmartPointer<vtkTriangleFilter> triagnle = vtkSmartPointer<vtkTriangleFilter>::New();
	triagnle->SetInputData(face);
	triagnle->Update();

	vtkSmartPointer<vtkAppendPolyData> app = vtkSmartPointer<vtkAppendPolyData>::New();
	app->AddInputData(triagnle->GetOutput());
	app->AddInputData(m_output);
	app->Update();

	vtkSmartPointer<vtkCleanPolyData> cleaner = vtkSmartPointer<vtkCleanPolyData>::New();
	cleaner->SetInputData(app->GetOutput());
	cleaner->Update();

	m_output->DeepCopy(cleaner->GetOutput());
	input->DeepCopy(app->GetOutput());

	vtkSmartPointer<vtkXMLPolyDataWriter> writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
	writer->SetInputData(face);
	writer->SetFileName("D:/STFast/Test/ContourSurfaceExtract/datas/face.vtp");
	writer->Write();
	writer->Update();

	vtkSmartPointer<vtkXMLPolyDataWriter> writer2 = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
	writer2->SetInputData(cleaner->GetOutput());
	writer2->SetFileName("D:/STFast/Test/ContourSurfaceExtract/datas/extruded.vtp");
	writer2->Write();
	writer2->Update();


}

void InteractorStyle3DOpenningGap::MarkCellInsidePolygon(vtkSmartPointer<vtkPolyData> inPD, vtkSmartPointer<vtkPolyData> clipperPD, vtkIdList *outList)
{
	vtkIdList* tmpList = vtkIdList::New();
	vtkSmartPointer<vtkSelectEnclosedPoints> selector = vtkSmartPointer<vtkSelectEnclosedPoints>::New();
	selector->SetInputData(inPD);
	selector->SetSurfaceData(clipperPD);
	selector->Update();

	vtkSmartPointer<vtkXMLPolyDataWriter> inWriter = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
	inWriter->SetInputData(inPD);
	inWriter->SetFileName("D:/STFast/Test/ContourSurfaceExtract/datas/inPD.vtp");
	inWriter->Write();
	inWriter->Update();

	vtkSmartPointer<vtkXMLPolyDataWriter> clipWriter = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
	clipWriter->SetInputData(clipperPD);
	clipWriter->SetFileName("D:/STFast/Test/ContourSurfaceExtract/datas/clipPD.vtp");
	clipWriter->Write();
	clipWriter->Update();

	vtkSmartPointer<vtkImplicitPolyDataDistance> ipdd = vtkSmartPointer<vtkImplicitPolyDataDistance>::New();
	ipdd->SetInput(clipperPD);
	ipdd->SetTolerance(0);

	// Can be optimize to locate possible cells first
	for (int i = 0; i < inPD->GetNumberOfCells(); i++)
	{
		vtkCell* l_cell = inPD->GetCell(i);
		double *l_cellCoord = (double*)malloc(sizeof(double) * 3);
		this->GetCellLocation(inPD, l_cell, l_cellCoord);
		double l_inpolygon = ipdd->EvaluateFunction(l_cellCoord);
		if (l_inpolygon <= 0 && l_cell->GetNumberOfPoints() >= 3) {
			tmpList->InsertUniqueId(i);
			//cout << "Marking " << i << endl;
		}
		std::free(l_cellCoord);
	}
	outList->DeepCopy(tmpList);
	std::cout << outList->GetNumberOfIds() << endl;
	for (int i=0; i< outList->GetNumberOfIds(); i++)
	{
		std::cout << outList->GetId(i) << " ";
	}

}

void InteractorStyle3DOpenningGap::GetCellLocation(vtkSmartPointer<vtkPolyData> inPD, vtkCell* inCell, double *outPt)
{
		double outtmp[3] = { 0,0,0 };
		int l_looper = 0;
		for (int i = 0; i < inCell->GetNumberOfPoints(); i++) {
			outtmp[0] += inPD->GetPoint(inCell->GetPointId(i))[0];
			outtmp[1] += inPD->GetPoint(inCell->GetPointId(i))[1];
			outtmp[2] += inPD->GetPoint(inCell->GetPointId(i))[2];
		}
		vtkMath::MultiplyScalar(outtmp, 1. / inCell->GetNumberOfPoints());
		memcpy(outPt, outtmp, sizeof(double) * 3);
}


void InteractorStyle3DOpenningGap::PushCellsAlongVector(vtkSmartPointer<vtkPolyData> inPD, vtkSmartPointer<vtkPolyData> outPD)
{	
	vtkSmartPointer<vtkPolyData> tempIn = vtkSmartPointer<vtkPolyData>::New();
	tempIn->DeepCopy(inPD);
	
	vtkSmartPointer<vtkXMLPolyDataWriter> inWriter = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
	inWriter->SetInputData(tempIn);
	inWriter->SetFileName("D:/STFast/Test/ContourSurfaceExtract/datas/toPush.vtp");
	inWriter->Write();
	inWriter->Update();

	std::cout << tempIn->GetNumberOfCells() << endl;
	for (int i =0; i< tempIn->GetNumberOfCells(); i++) {
		double* x = NULL;
		x = tempIn->GetPoint(i);
		//std::cout << "Before Push: " << x[0] << "," << x[1] << "," << x[2] << endl;
		for (vtkIdType j = 0; j < 3; j++){
			x[j] = x[j] + this->m_extrusionVector[j]* m_ridgeScaleFactor;		
		}
	 	
		//std::cout << "After Push: " << x[0] << "," << x[1] << "," << x[2] << endl;
	}
	vtkSmartPointer<vtkXMLPolyDataWriter> writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
	writer->SetInputData(inPD);
	writer->SetFileName("D:/STFast/Test/ContourSurfaceExtract/datas/pushed.vtp");
	writer->Write();
	writer->Update();
	
}

void InteractorStyle3DOpenningGap::DropSeedEnabled()
{
	if(!DropSeedState)
		DropSeedState = true;
	else if (DropSeedState == true)
		DropSeedState = false;

}

void InteractorStyle3DOpenningGap::GetExtrusionVector()
{
	m_extrusionVector = m_planeWidget->GetNormal();
	std::cout << "Extrusion vector:" << m_extrusionVector[0] << "," << m_extrusionVector[1] << "," << m_extrusionVector[2] << endl;
}

void InteractorStyle3DOpenningGap::ExtrudeAlongVector()
{
	if (this->TargetActor == NULL)
		return;

	// copy input to original
	vtkPolyData* input = vtkPolyData::SafeDownCast(this->TargetActor->GetMapper()->GetInput());
	this->m_original->DeepCopy(input);

	vtkSmartPointer<vtkLinearExtrusionFilter> extrude = vtkSmartPointer<vtkLinearExtrusionFilter>::New();
		extrude->SetInputData(input);
		extrude->SetExtrusionTypeToVectorExtrusion();
		extrude->SetVector(m_extrusionVector);		//getnormal 
		extrude->SetScaleFactor(5);
		extrude->Update();

		vtkSmartPointer<vtkPolyDataNormals> pdNormal = vtkSmartPointer<vtkPolyDataNormals>::New();
		pdNormal->SetInputData(extrude->GetOutput());
		pdNormal->Update();

		input->DeepCopy(pdNormal->GetOutput());

		vtkSmartPointer<vtkSTLWriter> writer = vtkSmartPointer<vtkSTLWriter>::New();
		writer->SetFileName("D:/STFast/Test/ContourSurfaceExtract/datas/plate-extrude.stl");
		writer->SetInputConnection(pdNormal->GetOutputPort());
		writer->Write();
}

void InteractorStyle3DOpenningGap::ClearWidgetRep()
{
	if (m_contourRep != NULL)
	{
		m_contourRep = NULL;
		m_contourWidget = NULL;
		this->Initialize();
	}
}

void InteractorStyle3DOpenningGap::BuildRidges()
{
	if (this->m_ridgeActor == NULL)
		return;
	vtkPolyData* ridgePD = vtkPolyData::SafeDownCast(this->m_ridgeActor->GetMapper()->GetInput());
	
	if (this->TargetActor == NULL)
		return;
	vtkPolyData* input = vtkPolyData::SafeDownCast(this->TargetActor->GetMapper()->GetInput());

	//vtkSmartPointer<vtkLinearExtrusionFilter> extrude = vtkSmartPointer<vtkLinearExtrusionFilter>::New();
	//extrude->SetInputData(ridgePD);
	//extrude->SetExtrusionTypeToVectorExtrusion();
	//extrude->SetVector(m_extrusionVector);		//getnormal 
	//extrude->SetScaleFactor(5);
	//extrude->Update();

	//vtkSmartPointer<vtkPolyDataNormals> pdNormal = vtkSmartPointer<vtkPolyDataNormals>::New();
	//pdNormal->SetInputData(extrude->GetOutput());
	//pdNormal->Update();
	//
	//ridgePD->DeepCopy(pdNormal->GetOutput());

	//vtkSmartPointer<vtkAppendPolyData> app = vtkSmartPointer<vtkAppendPolyData>::New();
	//app->AddInputData(ridgePD);
	//app->AddInputData(m_output);
	//app->Update();
	//
	//input->DeepCopy(app->GetOutput());
	//m_original->DeepCopy(input);
}

void InteractorStyle3DOpenningGap::SaveFile()
{
	if (!TargetActor) {
		return;
	}

	vtkPolyData* input = vtkPolyData::SafeDownCast(this->TargetActor->GetMapper()->GetInput());
	vtkSmartPointer<vtkPolyDataNormals> pdNormal = vtkSmartPointer<vtkPolyDataNormals>::New();
	pdNormal->SetInputData(input);
	pdNormal->Update();

	vtkSmartPointer<vtkSTLWriter> writer = vtkSmartPointer<vtkSTLWriter>::New();
	writer->SetFileName("D:/STFast/Test/ContourSurfaceExtract/datas/plate-app.stl");
	writer->SetInputData(pdNormal->GetOutput());
	writer->Write();
	writer->Update();


}

void InteractorStyle3DOpenningGap::FindClosestPoint()
{
	// list the cells out
	vtkSmartPointer<vtkPolyData> contourPD = vtkSmartPointer<vtkPolyData> ::New();
	contourPD = m_contourRep->GetContourRepresentationAsPolyData();
	int n = contourPD->GetNumberOfPoints();
	vtkPoints* points = contourPD->GetPoints();

	double start[3], end[3];
	memcpy(start, points->GetPoint(0), sizeof(double) * 3);
	memcpy(end, points->GetPoint(n - 1), sizeof(double) * 3);
	std::cout << "First Points: " << start[0] << start[1] << start[2];
	std::cout << "Last Points: " << end[0] << end[1] << end[2];

	for (int i = 0; i < n; i++)
	{
		std::cout << "Point " << i << ": " << points->GetPoint(i)[0] << " " << points->GetPoint(i)[1] << " " << points->GetPoint(i)[2] << endl;
	}
	vtkPolyData* input = vtkPolyData::SafeDownCast(this->TargetActor->GetMapper()->GetInput());
	this->m_original->DeepCopy(input);

	vtkSmartPointer<vtkFeatureEdges> featureEdges = vtkSmartPointer<vtkFeatureEdges>::New();
	featureEdges->SetInputData(m_original);			//surface
	featureEdges->BoundaryEdgesOn();
	featureEdges->FeatureEdgesOff();
	featureEdges->ManifoldEdgesOff();
	featureEdges->NonManifoldEdgesOff();
	featureEdges->Update();

	vtkSmartPointer<vtkPolyDataConnectivityFilter> connFilter = vtkSmartPointer<vtkPolyDataConnectivityFilter>::New();
	connFilter->SetInputData(featureEdges->GetOutput());
	connFilter->ColorRegionsOn();
	connFilter->SetExtractionModeToClosestPointRegion();
	connFilter->SetClosestPoint(start);
	connFilter->Update();

	//start point
	vtkSmartPointer<vtkPolyData> conpd = vtkSmartPointer<vtkPolyData>::New();
	conpd->DeepCopy(connFilter->GetOutput());
	vtkSmartPointer<vtkKdTree> kdTree = vtkSmartPointer<vtkKdTree>::New();
	kdTree->BuildLocatorFromPoints(conpd);
	double NOTUSED;
	double closestSPoint[3];
	int closestPointId = kdTree->FindClosestPoint(start, NOTUSED);
	memcpy(closestSPoint, conpd->GetPoint(closestPointId), sizeof(double) * 3);

	//end point
	connFilter->SetClosestPoint(end);
	connFilter->Update();
	conpd->DeepCopy(connFilter->GetOutput());
	kdTree->BuildLocatorFromPoints(conpd);
	double closestEPoint[3];
	int closestEPointId = kdTree->FindClosestPoint(end, NOTUSED);
	memcpy(closestEPoint, conpd->GetPoint(closestEPointId), sizeof(double) * 3);

	points->InsertPoint(0, closestSPoint);
	points->InsertNextPoint(closestEPoint);
	
	contourPD->SetPoints(points);
	m_contourWidget->Initialize(contourPD);

	//std::cout << "Closest Start Points: " << closestSPoint[0] << " " << closestSPoint[1] << " " << closestSPoint[2] << endl;
	//std::cout << "Closest End Points: " << closestEPoint[0]<<" " << closestEPoint[1] << " " << closestEPoint[2]  << endl;

	//std::cout << n << endl;
	//std::cout << contourPD->GetNumberOfPoints() << endl;
	for (int i = 0; i < contourPD->GetNumberOfPoints(); i++)
	{
		std::cout << "Point " << i << ": " << points->GetPoint(i)[0] << " " << points->GetPoint(i)[1] << " " << points->GetPoint(i)[2] << endl;
	}


}

